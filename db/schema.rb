# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_31_153919) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bans", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "banning_user", null: false
    t.datetime "ban_end_time"
    t.boolean "permanent_ban", default: false, null: false
    t.boolean "ban_reversal", default: false, null: false
    t.text "internal_reason", null: false
    t.string "external_reason", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "banned_user", null: false
    t.index ["user_id"], name: "index_bans_on_user_id"
  end

  create_table "chat_log_edits", force: :cascade do |t|
    t.bigint "chat_log_id"
    t.bigint "chat_log_edited"
    t.text "internal_justification"
    t.string "external_justification"
    t.bigint "editor_id"
    t.bigint "edited_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chat_log_id"], name: "index_chat_log_edits_on_chat_log_id"
    t.index ["editor_id"], name: "index_chat_log_edits_on_editor_id"
  end

  create_table "chat_logs", force: :cascade do |t|
    t.string "channel", default: ""
    t.string "sender", default: ""
    t.text "message", default: ""
    t.bigint "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "conversations", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "target_id", null: false
    t.bigint "creator_id"
    t.bigint "created_by", null: false
    t.string "subject", null: false
    t.datetime "read_by_user"
    t.datetime "read_by_admins"
    t.boolean "locked", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_id"], name: "index_conversations_on_creator_id"
    t.index ["user_id"], name: "index_conversations_on_user_id"
  end

  create_table "credentials", force: :cascade do |t|
    t.string "external_id"
    t.string "public_key"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "nickname"
    t.index ["user_id"], name: "index_credentials_on_user_id"
  end

  create_table "eulas", force: :cascade do |t|
    t.bigint "user_id"
    t.string "note"
    t.text "eula_text", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "publisher", null: false
    t.index ["user_id"], name: "index_eulas_on_user_id"
  end

  create_table "messages", force: :cascade do |t|
    t.bigint "conversation_id"
    t.bigint "sender_id"
    t.bigint "sent_by", null: false
    t.boolean "sent_anonymously", default: false, null: false
    t.text "body", null: false
    t.datetime "edited_at"
    t.boolean "user_visible", default: true, null: false
    t.bigint "deleter_id"
    t.bigint "deleted_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "body_plaintext", default: "", null: false
    t.index ["conversation_id"], name: "index_messages_on_conversation_id"
    t.index ["deleter_id"], name: "index_messages_on_deleter_id"
    t.index ["sender_id"], name: "index_messages_on_sender_id"
  end

  create_table "reports", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "reporter", null: false
    t.string "report_reason", null: false
    t.boolean "resolved", default: false, null: false
    t.string "resolving_action"
    t.datetime "resolved_at"
    t.bigint "resolver"
    t.bigint "msg_id", null: false
    t.string "msg_channel", null: false
    t.string "msg_sender", null: false
    t.text "msg_text", null: false
    t.bigint "msg_date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_reports_on_user_id"
  end

  create_table "role_permissions", force: :cascade do |t|
    t.jsonb "permissions", null: false
    t.bigint "role_id"
    t.bigint "creator_id"
    t.bigint "created_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_id"], name: "index_role_permissions_on_creator_id"
    t.index ["role_id"], name: "index_role_permissions_on_role_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.string "description", default: "", null: false
    t.boolean "enabled", default: true, null: false
    t.bigint "creator_id"
    t.bigint "created_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_id"], name: "index_roles_on_creator_id"
  end

  create_table "searches", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "searcher", null: false
    t.string "query_type", null: false
    t.bigint "results", null: false
    t.jsonb "query_notes"
    t.decimal "execution_time", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "url", default: "", null: false
    t.jsonb "search_params", default: {}, null: false
    t.index ["user_id"], name: "index_searches_on_user_id"
  end

  create_table "user_oauths", force: :cascade do |t|
    t.string "provider", null: false
    t.string "token", null: false
    t.string "email"
    t.string "uid", null: false
    t.string "username"
    t.datetime "expires_at"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "domain"
    t.index ["domain", "provider", "uid"], name: "index_user_oauths_on_domain_and_provider_and_uid", unique: true
    t.index ["domain", "provider", "user_id"], name: "index_user_oauths_on_domain_and_provider_and_user_id", unique: true
  end

  create_table "user_preferences", force: :cascade do |t|
    t.bigint "user_id"
    t.string "tz_offset", default: "+00:00", null: false
    t.boolean "use_24hr_time", default: true, null: false
    t.string "date_format", default: "%Y-%m-%d", null: false
    t.string "default_sort_col", default: "date", null: false
    t.string "default_sort_order", default: "desc", null: false
    t.integer "search_results", default: 10, null: false
    t.string "theme", default: "Light (Default)", null: false
    t.boolean "notify_pwchange_pri", default: true, null: false
    t.boolean "notify_pwchange_bkp", default: false, null: false
    t.boolean "notify_pwreset_pri", default: true, null: false
    t.boolean "notify_pwreset_bkp", default: false, null: false
    t.boolean "notify_sso_pri", default: true, null: false
    t.boolean "notify_sso_bkp", default: false, null: false
    t.boolean "notify_2fa_pri", default: true, null: false
    t.boolean "notify_2fa_bkp", default: false, null: false
    t.boolean "notify_admin_pri", default: true, null: false
    t.boolean "notify_admin_bkp", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_preferences_on_user_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.string "email", default: "", null: false
    t.string "backup_email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "eula_accepted_at"
    t.boolean "confirmable", default: true, null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "role", default: "user", null: false
    t.string "current_challenge"
    t.string "credential"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "encrypted_otp_secret"
    t.string "encrypted_otp_secret_iv"
    t.string "encrypted_otp_secret_salt"
    t.integer "consumed_timestep"
    t.boolean "otp_required_for_login", default: false, null: false
    t.datetime "otp_verification_timeout"
    t.datetime "otp_enabled_at"
    t.string "otp_backup_codes", array: true
    t.datetime "otp_backup_codes_enabled_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "bans", "users"
  add_foreign_key "chat_log_edits", "chat_logs"
  add_foreign_key "chat_log_edits", "users", column: "editor_id"
  add_foreign_key "conversations", "users"
  add_foreign_key "conversations", "users", column: "creator_id", on_delete: :nullify
  add_foreign_key "credentials", "users"
  add_foreign_key "eulas", "users"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "users", column: "deleter_id", on_delete: :nullify
  add_foreign_key "messages", "users", column: "sender_id", on_delete: :nullify
  add_foreign_key "reports", "users"
  add_foreign_key "role_permissions", "roles"
  add_foreign_key "role_permissions", "users", column: "creator_id"
  add_foreign_key "roles", "users", column: "creator_id"
  add_foreign_key "searches", "users"
  add_foreign_key "user_oauths", "users"
  add_foreign_key "user_preferences", "users"
end

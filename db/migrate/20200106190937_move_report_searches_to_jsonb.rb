class MoveReportSearchesToJsonb < ActiveRecord::Migration[5.2]
  def self.up
    puts "Migrating searches where query_type is one of: report_browse_user, report_query_user"
    execute("UPDATE searches" \
            "  SET search_params = jsonb_build_object(" \
            "    'searched_type', searched_type," \
            "    'channel', channel," \
            "    'sender', sender," \
            "    'message_query', advanced_query," \
            "    'reason_query', report_reason_query," \
            "    'base_id', base_id," \
            "    'start_time', start_time," \
            "    'end_time', end_time," \
            "    'per_page', per_page," \
            "    'page', page," \
            "    'highlight', message_highlight," \
            "    'sort_column', sort_column," \
            "    'sort_direction', sort_direction," \
            "    'custom_per_page', custom_per_page" \
            "  )" \
            "  WHERE query_type IN ('report_browse_user', 'report_query_user');")
    puts "Migrated searches where query_type is one of: report_browse_user, report_query_user"

    puts "Migrating searches where query_type is one of: report_browse, report_query"
    execute("UPDATE searches" \
            "  SET search_params = jsonb_build_object(" \
            "    'searched_type', searched_type," \
            "    'channel', channel," \
            "    'sender', sender," \
            "    'message_query', advanced_query," \
            "    'reason_query', report_reason_query," \
            "    'reporter', searched_id," \
            "    'resolver', searched_id2," \
            "    'base_id', base_id," \
            "    'start_time', start_time," \
            "    'end_time', end_time," \
            "    'per_page', per_page," \
            "    'page', page," \
            "    'highlight', message_highlight," \
            "    'sort_column', sort_column," \
            "    'sort_direction', sort_direction," \
            "    'custom_per_page', custom_per_page" \
            "  )" \
            "  WHERE query_type IN ('report_browse', 'report_query');")
    puts "Migrated searches where query_type is one of: report_browse, report_query"
  end

  def self.down
    puts "Defining cast function"
    execute("CREATE OR REPLACE FUNCTION cast_bigint_default(text, bigint) RETURNS bigint AS $$" \
            "  BEGIN" \
            "    RETURN CAST($1 AS bigint);" \
            "  EXCEPTION" \
            "    WHEN invalid_text_representation THEN" \
            "      RAISE NOTICE 'WARNING: default cast value used for %, data has likely been lost!%', $1, '\n';" \
            "      RETURN $2;" \
            "  END;" \
            " $$ LANGUAGE PLPGSQL IMMUTABLE;")
    puts "Cast function defined"
    # See previous migration (move_chat_log_searches_to_jsonb) for more info on
    # this function.

    puts "Migrating back searches where query_type is one of: report_browse_user, report_query_user"
    execute("UPDATE searches" \
            "  SET" \
            "    searched_type = search_params::jsonb->'searched_type'," \
            "    channel = search_params::jsonb->'channel'," \
            "    sender = search_params::jsonb->'sender'," \
            "    advanced_query = search_params::jsonb->'message_query'," \
            "    report_reason_query = search_params::jsonb->'reason_query'," \
            "    base_id = (search_params::jsonb->>'base_id')::integer," \
            "    start_time = cast_bigint_default(search_params::jsonb->>'start_time', 0)," \
            "    end_time = cast_bigint_default(search_params::jsonb->>'end_time', 0)," \
            "    per_page = (search_params::jsonb->>'per_page')::integer," \
            "    page = (search_params::jsonb->>'page')::integer," \
            "    message_highlight = (search_params::jsonb->>'highlight')::integer," \
            "    sort_column = search_params::jsonb->'sort_column'," \
            "    sort_direction = search_params::jsonb->'sort_direction'," \
            "    custom_per_page = (search_params::jsonb->>'custom_per_page')::boolean" \
            "  WHERE query_type IN ('report_browse_user', 'report_query_user');")
    puts "Migrated back searches where query_type is one of: report_browse_user, report_query_user"

    puts "Migrating back searches where query_type is one of: report_browse, report_query"
    execute("UPDATE searches" \
            "  SET" \
            "    searched_type = search_params::jsonb->'searched_type'," \
            "    channel = search_params::jsonb->'channel'," \
            "    sender = search_params::jsonb->'sender'," \
            "    advanced_query = search_params::jsonb->'message_query'," \
            "    report_reason_query = search_params::jsonb->'reason_query'," \
            "    searched_id = cast_bigint_default(search_params::jsonb->>'reporter', 0)," \
            "    searched_id2 = cast_bigint_default(search_params::jsonb->>'resolver', 0)," \
            "    base_id = (search_params::jsonb->>'base_id')::integer," \
            "    start_time = cast_bigint_default(search_params::jsonb->>'start_time', 0)," \
            "    end_time = cast_bigint_default(search_params::jsonb->>'end_time', 0)," \
            "    per_page = (search_params::jsonb->>'per_page')::integer," \
            "    page = (search_params::jsonb->>'page')::integer," \
            "    message_highlight = (search_params::jsonb->>'highlight')::integer," \
            "    sort_column = search_params::jsonb->'sort_column'," \
            "    sort_direction = search_params::jsonb->'sort_direction'," \
            "    custom_per_page = (search_params::jsonb->>'custom_per_page')::boolean" \
            "  WHERE query_type IN ('report_browse', 'report_query');")
    puts "Migrated back searches where query_type is one of: report_browse, report_query"

    puts "Removing cast function"
    execute("DROP FUNCTION IF EXISTS cast_bigint_default;")
    puts "Cast function removed"
  end
end

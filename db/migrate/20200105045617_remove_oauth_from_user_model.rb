class RemoveOauthFromUserModel < ActiveRecord::Migration[5.2]
  def change
    # Remove Discord Oauth columns
    remove_column :users, :ds_oauth_used, :boolean, default: false, null: false
    remove_column :users, :ds_oauth_token, :string
    remove_column :users, :ds_email, :string
    remove_column :users, :ds_uid, :string
    remove_column :users, :ds_username, :string

    # Remove Facebook Oauth columns
    remove_column :users, :fb_oauth_used, :boolean, default: false, null: false
    remove_column :users, :fb_oauth_token, :string
    remove_column :users, :fb_oauth_expires_at, :datetime
    remove_column :users, :fb_email, :string
    remove_column :users, :fb_uid, :string
    remove_column :users, :fb_username, :string

    # Remove GitHub Oauth columns
    remove_column :users, :gh_oauth_used, :boolean, default: false, null: false
    remove_column :users, :gh_oauth_token, :string
    remove_column :users, :gh_email, :string
    remove_column :users, :gh_uid, :string
    remove_column :users, :gh_username, :string

    # Remove GitLab Oauth columns
    remove_column :users, :gl_oauth_used, :boolean, default: false, null: false
    remove_column :users, :gl_oauth_token, :string
    remove_column :users, :gl_email, :string
    remove_column :users, :gl_uid, :string
    remove_column :users, :gl_username, :string

    # Remove Google Oauth columns
    remove_column :users, :goog_oauth_used, :boolean, default: false, null: false
    remove_column :users, :goog_oauth_token, :string
    remove_column :users, :goog_oauth_expires_at, :datetime
    remove_column :users, :goog_email, :string
    remove_column :users, :goog_uid, :string

    # Remove Twitch Oauth columns
    remove_column :users, :jtv_oauth_used, :boolean, default: false, null: false
    remove_column :users, :jtv_oauth_token, :string
    remove_column :users, :jtv_email, :string
    remove_column :users, :jtv_uid, :string
    remove_column :users, :jtv_username, :string

    # Remove Twitter Oauth columns
    remove_column :users, :tw_oauth_used, :boolean, default: false, null: false
    remove_column :users, :tw_oauth_token, :string
    remove_column :users, :tw_email, :string
    remove_column :users, :tw_uid, :string
    remove_column :users, :tw_username, :string
  end
end

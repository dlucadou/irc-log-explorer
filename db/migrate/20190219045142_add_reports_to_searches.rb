class AddReportsToSearches < ActiveRecord::Migration[5.2]
  def change
    add_column :searches, :report_reason_query, :text, null: true
    add_column :searches, :searched_id2, :string, null: true
  end
end

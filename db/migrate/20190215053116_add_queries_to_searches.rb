class AddQueriesToSearches < ActiveRecord::Migration[5.2]
  def change
    add_column :searches, :url, :text, null: false, default: ''
    add_column :searches, :exec_time_min, :decimal, null: true
    add_column :searches, :exec_time_max, :decimal, null: true
    add_column :searches, :searched_id, :string, null: true
    add_column :searches, :searched_type, :string, null: true
    add_column :searches, :searched_errors, :string, null: true
    add_column :searches, :searched_scope, :string, null: true
  end
end

class CreateChatLogEdits < ActiveRecord::Migration[5.2]
  def change
    create_table :chat_log_edits do |t|
      t.references :chat_log,           foreign_key: true
      t.integer :chat_log_edited,       null: true, default: nil
      t.text :internal_justification
      t.string :external_justification
      t.references :editor,             index: true
      t.integer :edited_by,             null: true, default: nil

      t.timestamps
    end
    add_foreign_key :chat_log_edits, :users, column: :editor_id
  end
end

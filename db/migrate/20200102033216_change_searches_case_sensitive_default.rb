class ChangeSearchesCaseSensitiveDefault < ActiveRecord::Migration[5.2]
  def change
    Search.where(case_sensitive: nil).update_all(case_sensitive: false)
    change_column :searches, :case_sensitive, :boolean, null: false, default: false
  end
end

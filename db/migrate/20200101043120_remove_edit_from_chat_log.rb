class RemoveEditFromChatLog < ActiveRecord::Migration[5.2]
  def change
    remove_column :chat_logs, :date_last_edited
    remove_column :chat_logs, :last_edit_reason
  end
end

class MoveSearchSearchesToJsonb < ActiveRecord::Migration[5.2]
  def self.up
    puts "Migrating searches where query_type is one of: search_browse, search_query"
    execute("UPDATE searches" \
            "  SET search_params = jsonb_build_object(" \
            "    'searched_type', searched_type," \
            "    'searched_scope', searched_scope," \
            "    'searched_errors', searched_errors," \
            "    'searched_id', searched_id," \
            "    'exec_time_min', exec_time_min," \
            "    'exec_time_max', exec_time_max," \
            "    'start_time', start_time," \
            "    'end_time', end_time," \
            "    'per_page', per_page," \
            "    'page', page," \
            "    'sort_column', sort_column," \
            "    'sort_direction', sort_direction," \
            "    'custom_per_page', custom_per_page" \
            "  )" \
            "  WHERE query_type IN ('search_browse', 'search_query');")
    puts "Migrated searches where query_type is one of: search_browse, search_query"
  end

  def self.down
    puts "Defining cast function"
    execute("CREATE OR REPLACE FUNCTION cast_bigint_default(text, bigint) RETURNS bigint AS $$" \
            "  BEGIN" \
            "    RETURN CAST($1 AS bigint);" \
            "  EXCEPTION" \
            "    WHEN invalid_text_representation THEN" \
            "      RAISE NOTICE 'WARNING: default cast value used for %, data has likely been lost!%', $1, '\n';" \
            "      RETURN $2;" \
            "  END;" \
            " $$ LANGUAGE PLPGSQL IMMUTABLE;")
    puts "Cast function defined"
    # See previous migration (move_chat_log_searches_to_jsonb) for more info on
    # this function.

    puts "Migrating searches where query_type is one of: search_browse, search_query"
    execute("UPDATE searches" \
            "  SET" \
            "    searched_type = search_params::jsonb->'searched_type'," \
            "    searched_scope = search_params::jsonb->'searched_scope'," \
            "    searched_errors = search_params::jsonb->'searched_errors'," \
            "    searched_id = cast_bigint_default(search_params::jsonb->>'reporter', 0)," \
            "    start_time = cast_bigint_default(search_params::jsonb->>'start_time', 0)," \
            "    end_time = cast_bigint_default(search_params::jsonb->>'end_time', 0)," \
            "    per_page = (search_params::jsonb->>'per_page')::integer," \
            "    page = (search_params::jsonb->>'page')::integer," \
            "    sort_column = search_params::jsonb->'sort_column'," \
            "    sort_direction = search_params::jsonb->'sort_direction'," \
            "    custom_per_page = (search_params::jsonb->>'custom_per_page')::boolean" \
            "  WHERE query_type IN ('search_browse', 'search_query');")
    puts "Migrated searches where query_type is one of: search_browse, search_query"

    puts "Removing cast function"
    execute("DROP FUNCTION IF EXISTS cast_bigint_default;")
    puts "Cast function removed"
  end
end

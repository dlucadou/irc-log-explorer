# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      ## Database authenticatable
      t.string :name,                null: false, default: ""
      t.string :email,               null: false, default: ""
      t.string :backup_email,        null: false, default: ""
      t.string :encrypted_password,  null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.inet     :current_sign_in_ip
      t.inet     :last_sign_in_ip
      
      ## EULA
      t.datetime :eula_accepted_at, null: true, default: nil
        # nil = never accepted a EULA
        # When they do accept a EULA, it's easy to determine if they have
        # accepted any updated EULAs by just doing a datetime comparison to
        # the current datetime.

      ## Confirmable
      t.boolean  :confirmable,   default: true, null: false
        # False = SSO-only account, true = normal account or SSO-only account
        # converted to normal account
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at
      
      ## Omniauthable
      # Discord
      t.boolean :ds_oauth_used,      null: false, default: false
      t.string :ds_oauth_token,      null: true, default: nil
      t.string :ds_email,            null: true, default: nil
      t.string :ds_uid,              null: true, default: nil
      t.string :ds_username,         null: true, default: nil
      
      # Facebook
      t.boolean :fb_oauth_used,      null: false, default: false
      t.string :fb_oauth_token,      null: true, default: nil
      t.datetime :fb_oauth_expires_at, null: true, default: nil
      t.string :fb_email,            null: true, default: nil
      t.string :fb_uid,              null: true, default: nil
      t.string :fb_username,         null: true, default: nil
      
      # GitHub
      t.boolean :gh_oauth_used,      null: false, default: false
      t.string :gh_oauth_token,      null: true, default: nil
      t.string :gh_email,            null: true, default: nil
      t.string :gh_uid,              null: true, default: nil
      t.string :gh_username,         null: true, default: nil
      
      # GitLab
      t.boolean :gl_oauth_used,      null: false, default: false
      t.string :gl_oauth_token,      null: true, default: nil
      t.string :gl_email,            null: true, default: nil
      t.string :gl_uid,              null: true, default: nil
      t.string :gl_username,         null: true, default: nil
      
      # Google
      t.boolean :goog_oauth_used,    null: false, default: false
      t.string :goog_oauth_token,    null: true, default: nil
      t.datetime :goog_oauth_expires_at, null: true, default: nil
      t.string :goog_email,          null: true, default: nil
      t.string :goog_uid,            null: true, default: nil
      
      # Twitch
      t.boolean :jtv_oauth_used,     null: false, default: false
      t.string :jtv_oauth_token,     null: true, default: nil
      t.string :jtv_email,           null: true, default: nil
      t.string :jtv_uid,             null: true, default: nil
      t.string :jtv_username,        null: true, default: nil
      
      # Twitter
      t.boolean :tw_oauth_used,      null: false, default: false
      t.string :tw_oauth_token,      null: true, default: nil
      t.string :tw_email,            null: true, default: nil
      t.string :tw_uid,              null: true, default: nil
      t.string :tw_username,         null: true, default: nil
      
      ## Permissions and Bans
      t.string :role,                null: false, default: "user"
        # Values: user, admin. Potential future roles: superuser, moderator.
      t.datetime :banned_until,      null: true, default: nil 
      t.boolean :permanently_banned, null: false, default: false

      ## For WebAuthn U2F
      t.string :current_challenge,   null: true
      t.string :credential,          null: true
      
      ## Unix timestamp for signup date
      t.integer :created_at,         limit: 8, null: false


      t.timestamps null: false
    end

    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
    # add_index :users, :confirmation_token,   unique: true
    # add_index :users, :unlock_token,         unique: true
  end
end

class CreateSearches < ActiveRecord::Migration[5.2]
  def change
    create_table :searches do |t|
      t.belongs_to  :user, index: true, foreign_key: true
      t.integer :searcher, null: false
      t.string :query_type, null: false
        # Expected types: [:basic, :advanced, :context]
      t.integer :results, null: false
      t.jsonb :query_notes, null: true, default: nil
        # The intent of this field is to store search notes (i.e. errors), and
        # I chose to use jsonb over text or string because I like being able
        # to add additional attributes, and potentially filter by searches
        # with errors, warnings, etc.
      t.decimal :execution_time, null: false
      # Query values
      t.text :basic_query, null: true, default: nil
      t.text :advanced_query, null: true, default: nil
      t.string :channel, null: true, default: nil
      t.string :sender, null: true, default: nil
      t.boolean :case_sensitive, null: true, default: nil
      t.integer :start_id, null: true, default: nil
      t.integer :base_id, null: true, default: nil
      t.integer :end_id, null: true, default: nil
      t.bigint :start_time, null: true, default: nil
      t.bigint :end_time, null: true, default: nil
      # View options
      t.string :sort_column, null: true
      t.string :sort_direction, null: true
      t.integer :message_highlight, null: true
      t.integer :page, null: true
      t.integer :per_page, null: true
      t.boolean :custom_per_page, null: false, default: false
      
      t.timestamps
    end
  end
end

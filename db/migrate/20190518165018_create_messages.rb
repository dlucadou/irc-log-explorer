class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.belongs_to :conversation,     index: true, foreign_key: true
      t.references :sender,           index: true
        # User who sent this message
      t.integer    :sent_by,          null: false
        # ID of the user who sent it (incase account is deleted)
      t.boolean    :sent_anonymously, null: false, default: false
        # If the user can see who sent the message

      t.text       :body,             null: false
      t.datetime   :edited_at,        null: true, default: nil
        # Only set when the user edits message, updated_at is changed
        # when the message is hidden, deleted, etc.
      t.boolean    :user_visible,     null: false, default: true

      t.references :deleter,          index: true
      t.integer    :deleted_by,       null: true, default: nil
        # ID of user who deleted it (incase account is deleted)

      t.timestamps
    end
    add_foreign_key :messages, :users, column: :sender_id, on_delete: :nullify
    add_foreign_key :messages, :users, column: :deleter_id, on_delete: :nullify
  end
end

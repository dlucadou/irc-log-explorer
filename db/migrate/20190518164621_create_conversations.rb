class CreateConversations < ActiveRecord::Migration[5.2]
  def change
    create_table :conversations do |t|
      t.belongs_to  :user,             index: true, foreign_key: true
        # User conversation is sent to or sent by, not an admin who sent or replied to it
      t.integer     :target_id,        null: false
        # ID of the user the conversation is sent to (in case of account deletion)
      t.references  :creator,          index: true
      t.integer     :created_by,       null: false

      t.string      :subject,          null: false

      t.datetime    :read_by_user,     null: true, default: nil
      t.datetime    :read_by_admins,   null: true, default: nil

      t.boolean     :locked,           null: false, default: false

      t.timestamps
    end
    add_foreign_key :conversations, :users, column: :creator_id, on_delete: :nullify
  end
end

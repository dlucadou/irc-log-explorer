class AddBanFieldsToUsers < ActiveRecord::Migration[5.2]
  def change
    # Add email fields for Oauth providers
    add_column :users, :ban_reason_internal, :text, null: true
    add_column :users, :ban_reason_external, :string, null: true
  end
end

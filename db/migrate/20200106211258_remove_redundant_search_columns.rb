class RemoveRedundantSearchColumns < ActiveRecord::Migration[5.2]
  def change
    # Remove columns that are now stored in the search_params jsonb column
    remove_column :searches, :basic_query, :text
    remove_column :searches, :advanced_query, :text
    remove_column :searches, :channel, :string
    remove_column :searches, :sender, :string
    remove_column :searches, :case_sensitive, :boolean, default: false, null: false
    remove_column :searches, :start_id, :integer
    remove_column :searches, :base_id, :integer
    remove_column :searches, :end_id, :integer
    remove_column :searches, :start_time, :bigint
    remove_column :searches, :end_time, :bigint
    remove_column :searches, :sort_column, :string
    remove_column :searches, :sort_direction, :string
    remove_column :searches, :message_highlight, :integer
    remove_column :searches, :page, :integer
    remove_column :searches, :per_page, :integer
    remove_column :searches, :custom_per_page, :boolean, default: false, null: false
    remove_column :searches, :exec_time_min, :decimal
    remove_column :searches, :exec_time_max, :decimal
    remove_column :searches, :searched_id, :string
    remove_column :searches, :searched_type, :string
    remove_column :searches, :searched_errors, :string
    remove_column :searches, :searched_scope, :string
    remove_column :searches, :report_reason_query, :text
    remove_column :searches, :searched_id2, :string
  end
end

class MigrateOauthToUserOauthTable < ActiveRecord::Migration[5.2]
  def self.up
    puts "Migrating Discord Oauth users"
    execute("INSERT INTO user_oauths(provider, token, email, uid, username, user_id, created_at, updated_at)" \
            "  SELECT 'discord', users.ds_oauth_token, users.ds_email, users.ds_uid, users.ds_username, users.id, now(), now()" \
            "  FROM users" \
            "  WHERE users.ds_oauth_used IS TRUE;")
    puts "Discord Oauth users migrated successfully"

    puts "Migrating Facebook Oauth users"
    execute("INSERT INTO user_oauths(provider, token, email, uid, username, expires_at, user_id, created_at, updated_at)" \
            "  SELECT 'facebook', users.fb_oauth_token, users.fb_email, users.fb_uid, users.fb_username, users.fb_oauth_expires_at, users.id, now(), now()" \
            "  FROM users" \
            "  WHERE users.fb_oauth_used IS TRUE;")
    puts "Facebook Oauth users migrated successfully"

    puts "Migrating GitHub Oauth users"
    execute("INSERT INTO user_oauths(provider, token, email, uid, username, user_id, created_at, updated_at)" \
            "  SELECT 'github', users.gh_oauth_token, users.gh_email, users.gh_uid, users.gh_username, users.id, now(), now()" \
            "  FROM users" \
            "  WHERE users.gh_oauth_used IS TRUE;")
    puts "GitHub Oauth users migrated successfully"

    puts "Migrating GitLab Oauth users"
    execute("INSERT INTO user_oauths(provider, token, email, uid, username, user_id, created_at, updated_at)" \
            "  SELECT 'gitlab', users.gl_oauth_token, users.gl_email, users.gl_uid, users.gl_username, users.id, now(), now()" \
            "  FROM users" \
            "  WHERE users.gl_oauth_used IS TRUE;")
    puts "GitLab Oauth users migrated successfully"

    puts "Migrating Google Oauth users"
    execute("INSERT INTO user_oauths(provider, token, email, uid, expires_at, user_id, created_at, updated_at)" \
            "  SELECT 'google_oauth2', users.goog_oauth_token, users.goog_email, users.goog_uid, users.goog_oauth_expires_at, users.id, now(), now()" \
            "  FROM users" \
            "  WHERE users.goog_oauth_used IS TRUE;")
    puts "Google Oauth users migrated successfully"

    puts "Migrating Twitch Oauth users"
    execute("INSERT INTO user_oauths(provider, token, email, uid, username, user_id, created_at, updated_at)" \
            "  SELECT 'twitch', users.jtv_oauth_token, users.jtv_email, users.jtv_uid, users.jtv_username, users.id, now(), now()" \
            "  FROM users" \
            "  WHERE users.jtv_oauth_used IS TRUE;")
    puts "Twitch Oauth users migrated successfully"

    puts "Migrating Twitter Oauth users"
    execute("INSERT INTO user_oauths(provider, token, email, uid, username, user_id, created_at, updated_at)" \
            "  SELECT 'twitter', users.tw_oauth_token, users.tw_email, users.tw_uid, users.tw_username, users.id, now(), now()" \
            "  FROM users" \
            "  WHERE users.tw_oauth_used IS TRUE;")
    puts "Twitter Oauth users migrated successfully"
  end

  def self.down
    puts "De-migrating Discord Oauth users"
    execute("UPDATE users AS u" \
            "  SET ds_oauth_used = true, ds_oauth_token = uo.token," \
            "    ds_email = uo.email, ds_uid = uo.uid," \
            "    ds_username = uo.username" \
            "  FROM user_oauths AS uo" \
            "  WHERE u.id = uo.user_id AND uo.provider = 'discord';")
    puts "Discord Oauth users de-migrated successfully"

    puts "De-migrating Facebook Oauth users"
    execute("UPDATE users AS u" \
            "  SET fb_oauth_used = true, fb_oauth_token = uo.token," \
            "    fb_oauth_expires_at = uo.expires_at, fb_email = uo.email," \
            "    fb_uid = uo.uid, fb_username = uo.username" \
            "  FROM user_oauths AS uo" \
            "  WHERE u.id = uo.user_id AND uo.provider = 'facebook';")
    puts "Facebook Oauth users de-migrated successfully"

    puts "De-migrating GitHub Oauth users"
    execute("UPDATE users AS u" \
            "  SET gh_oauth_used = true, gh_oauth_token = uo.token," \
            "    gh_email = uo.email, gh_uid = uo.uid," \
            "    gh_username = uo.username" \
            "  FROM user_oauths AS uo" \
            "  WHERE u.id = uo.user_id AND uo.provider = 'github';")
    puts "GitHub Oauth users de-migrated successfully"

    puts "De-migrating GitLab Oauth users"
    execute("UPDATE users AS u" \
            "  SET gl_oauth_used = true, gl_oauth_token = uo.token," \
            "    gl_email = uo.email, gl_uid = uo.uid," \
            "    gl_username = uo.username" \
            "  FROM user_oauths AS uo" \
            "  WHERE u.id = uo.user_id AND uo.provider = 'gitlab';")
    puts "GitLab Oauth users de-migrated successfully"

    puts "De-migrating Google Oauth users"
    execute("UPDATE users AS u" \
            "  SET goog_oauth_used = true, goog_oauth_token = uo.token," \
            "    goog_oauth_expires_at = uo.expires_at, goog_email = uo.email," \
            "    goog_uid = uo.uid" \
            "  FROM user_oauths AS uo" \
            "  WHERE u.id = uo.user_id AND uo.provider = 'google_oauth2';")
    puts "Google Oauth users de-migrated successfully"

    puts "De-migrating Twitch Oauth users"
    execute("UPDATE users AS u" \
            "  SET jtv_oauth_used = true, jtv_oauth_token = uo.token," \
            "    jtv_email = uo.email, jtv_uid = uo.uid," \
            "    jtv_username = uo.username" \
            "  FROM user_oauths AS uo" \
            "  WHERE u.id = uo.user_id AND uo.provider = 'twitch';")
    puts "Twitch Oauth users de-migrated successfully"

    puts "De-migrating Twitter Oauth users"
    execute("UPDATE users AS u" \
            "  SET tw_oauth_used = true, tw_oauth_token = uo.token," \
            "    tw_email = uo.email, tw_uid = uo.uid," \
            "    tw_username = uo.username" \
            "  FROM user_oauths AS uo" \
            "  WHERE u.id = uo.user_id AND uo.provider = 'twitter';")
    puts "Twitter Oauth users de-migrated successfully"
  end
end

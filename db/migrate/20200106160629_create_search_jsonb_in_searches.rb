class CreateSearchJsonbInSearches < ActiveRecord::Migration[5.2]
  def change
    add_column :searches, :search_params, :jsonb, default: {}
    change_column_null :searches, :search_params, false
  end
end

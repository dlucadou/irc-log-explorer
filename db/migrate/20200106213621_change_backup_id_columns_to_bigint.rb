class ChangeBackupIdColumnsToBigint < ActiveRecord::Migration[5.2]
  def change
    # I have several backup columns for IDs that are nullified on delete. This converts them to BigInt, since all IDs use BigInt in the DB.

    # Bans table
    change_column :bans, :user_id, :integer, limit: 8
    change_column :bans, :banning_user, :integer, limit: 8
    change_column :bans, :banned_user, :integer, limit: 8

    # ChatLogEdits table
    change_column :chat_log_edits, :chat_log_edited, :integer, limit: 8
    change_column :chat_log_edits, :edited_by, :integer, limit: 8

    # Conversations table
    change_column :conversations, :target_id, :integer, limit: 8
    change_column :conversations, :created_by, :integer, limit: 8

    # Messages table
    change_column :messages, :sent_by, :integer, limit: 8
    change_column :messages, :deleted_by, :integer, limit: 8

    # Reports table
    change_column :reports, :reporter, :integer, limit: 8
    change_column :reports, :resolver, :integer, limit: 8
    change_column :reports, :msg_id, :integer, limit: 8 # Refers to ChatLogs ID, which is BigInt

    # RolePermissions table
    change_column :role_permissions, :created_by, :integer, limit: 8

    # Roles table
    change_column :roles, :created_by, :integer, limit: 8

    # Searches table
    change_column :searches, :searcher, :integer, limit: 8
    change_column :searches, :results, :integer, limit: 8 # Can be up to the max ChatLog, Search, or Report ID, which is BigInt
  end
end

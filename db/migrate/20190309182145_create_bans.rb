class CreateBans < ActiveRecord::Migration[5.2]
  def change
    create_table :bans do |t|
      t.belongs_to :user,          index: true, foreign_key: true
      t.integer :user_id,          null: false
      t.integer :banning_user,     null: false
      t.datetime :ban_end_time,    null: true, default: nil
        # nil indicates either a permanent ban or a ban reversal
      t.boolean :permanent_ban,    null: false, default: false
      t.boolean :ban_reversal,     null: false, default: false
      t.text :internal_reason,     null: false
      t.string :external_reason,   null: false

      t.timestamps
    end
  end
end

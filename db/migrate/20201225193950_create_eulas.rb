class CreateEulas < ActiveRecord::Migration[6.0]
  def change
    create_table :eulas do |t|
      t.belongs_to  :user, foreign_key: true
      t.string      :note, null: true
      t.text        :eula_text, null: false

      t.timestamps
    end
  end
end

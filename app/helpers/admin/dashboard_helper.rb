module Admin::DashboardHelper
  include ApplicationHelper

  # Reports page helper methods

  def self.dash_report_statuses
    ReportsHelper.report_statuses
  end

  # Searches page helper methods

  def self.dash_search_types
    {
      'All': 'all',
      'Advanced': 'advanced',
      'Basic': 'basic',
      'Browse': 'browse',
      'Context': 'context',
      'Reports (Browse)': 'report_browse',
      'Reports (Query)': 'report_query',
      'Searches (Browse)': 'search_browse',
      'Searches (Query)': 'search_query',
      'User Reports (Browse)': 'report_browse_user',
      'User Reports (Query)': 'report_query_user'
    }
  end

  def self.dash_search_errors
    {
      'All': 'all',
      '1 or more': 'true',
      'None': 'false'
    }
  end

  def self.dash_search_scopes
    {
      'All': 'all',
      'All except Admin Searches': 'all_no-admin-search',
      'Admin': 'admin',
      'User': 'user'
    }
  end

  # Sorting
  def dash_search_sort_link(column, title = nil)
    # The title parameter is used if the column name isn't what I want to use
    title ||= column.titleize
    direction = column == dash_search_sort_column && sort_direction == "asc" ? "desc" : "asc"
    icon = sort_direction == "asc" ? "fas fa-caret-up" : "fas fa-caret-down"
    icon = column == dash_search_sort_column ? icon : ""
    link_to "#{title} <span class='#{icon}'></span>".html_safe, params.permit(:column, :direction, :query_type, :execution_time_min, :execution_time_max, :user_id, :query_notes, :start_time, :end_time, :query_scope, :reason, :advanced_query, :resolver_id, :page, :highlight, :per_page, :utf8).merge(:column => column, :direction => direction)
  end

  def dash_search_sortable_columns
    #{'date': 'created_at', 'errors': 'errors', 'execution_time': 'execution_time', 'id': 'id', 'type': 'query_type', 'user_id': 'user_id'}
    ["created_at", "execution_time", "query_type", "user_id"]
  end

  def dash_search_sort_column
    user_column = Current.user.user_preference.default_sort_col
    user_column = "created_at" if ["channel", "sender", "date"].index(user_column)
    dash_search_sortable_columns.include?(params[:column]) ? params[:column] : user_column
      # Only uses user preferences default column sort when user has
      # not clicked on any other columns
  end
end

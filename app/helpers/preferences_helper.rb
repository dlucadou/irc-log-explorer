module PreferencesHelper
  def self.tz_offset_regex
    /(^[+-]([01]\d|[2][0123]):[012345]\d$)|(^00:00$)/
    # Matches any offset from -23:59 to +23:59, including 00:00.
    # This is the range of offsets allowed by the Time class, not the list
    # of time zones in use.
    # I'm not going to validate if an offset is an actual timezone because
    # then I would have to keep track of time zone changes and that is not
    # a very enjoyable task.
  end

  def tz_offset_regex
    PreferencesHelper.tz_offset_regex
  end

  def self.tz_utc_regex
    /^[+-]?00:00$/
    # Used to match an offset of GMT, regardless of how the user specified it
  end

  def tz_utc_regex
    PreferencesHelper.tz_utc_regex
  end

  def self.valid_date_formats
    {'YYYY-MM-DD': '%Y-%m-%d', 'YYYY-DD-MM': '%Y-%d-%m', 'MM-DD-YYYY': '%m-%d-%Y', 'DD-MM-YYYY': '%d-%m-%Y', 'MM-YYYY-DD': '%m-%Y-%d', 'DD-YYYY-MM': '%d-%Y-%m'}
  end

  def valid_date_formats
    PreferencesHelper.valid_date_formats
  end

  def valid_booleans
    ['true', 'false']
  end

  def valid_checkboxes
    [true, false]
  end

  def self.valid_sort_columns
    {'Channel': 'channel', 'Sender': 'sender', 'Time': 'date'}
  end

  def valid_sort_columns
    PreferencesHelper.valid_sort_columns
  end

  def valid_sort_orders
    ['asc', 'desc']
  end

  def valid_themes
    ['Dark', 'Light (Default)', 'Midnight (Dark)', 'Minty (Light)', 'Solarized (Dark)', 'Ubuntu (Light)']
  end

  # "self." methods can be accessed by calling PreferencesHelper.method
  # without using "include PreferencesHelper"
  # https://stackoverflow.com/a/5130344
  def self.user_settable_prefs(user: Current.user)
    {
      :notify_pwchange_pri => true,
      :notify_pwchange_bkp => user.backup_email.length > 0,
      :notify_pwreset_pri => false,
      :notify_pwreset_bkp => user.backup_email.length > 0,
      :notify_sso_pri => true,
      :notify_sso_bkp => user.backup_email.length > 0,
      :notify_2fa_pri => false,
      :notify_2fa_bkp => user.backup_email.length > 0,
      :notify_admin_pri => false,
      :notify_admin_bkp => user.backup_email.length > 0
    }
  end

  # Retain compatibility for files that have used "include PreferencesHelper"
  def user_settable_prefs(user: Current.user)
    PreferencesHelper.user_settable_prefs(user: user)
  end

  def self.valid_per_page(per_page)
    per_page.to_i.to_s == per_page.to_s && per_page.to_i >= PreferencesHelper.min_per_page && per_page.to_i <= PreferencesHelper.max_per_page
    # TODO: centralize usage of this across the application
  end

  def self.default_per_page
    Rails.configuration.application[:users][:preferences][:default_per_page]
  end

  def self.min_per_page
    Rails.configuration.application[:users][:preferences][:min_per_page]
  end

  def self.max_per_page
    Rails.configuration.application[:users][:preferences][:max_per_page]
  end
end

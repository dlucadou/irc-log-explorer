require 'redcarpet/render_strip' # For markdown_to_plaintext
require 'uri'

module ApplicationHelper
  def flash_class(level)
    case level.to_sym
      when :info then "alert alert-info"
      when :notice then "alert alert-warning"
      when :success then "alert alert-success"
      when :error then "alert alert-danger"
      when :alert then "alert alert-danger"
      else "alert alert-dark"
    end
  end

  def per_page(use_prefs: false)
    if !params[:per_page].nil? && PreferencesHelper.valid_per_page(params[:per_page]) && !use_prefs
      per_page = params[:per_page].to_i
    elsif !Current.user.user_preference.search_results.nil?
      per_page = Current.user.user_preference.search_results.to_i
    else
      per_page = Rails.configuration.application[:users][:preferences][:default_per_page]
    end
  end

  def self.banned_user_permissions
    # This method generates the list of controllers a user is allowed to access while banned.
    # It is a stop-gap until I get the permissions system in place.
    # TODO: once I have a proper permissions system, eliminate this.
    [
      PagesController.controller_name,
      ConversationsController.controller_name,
      MessagesController.controller_name,
      SessionsController.controller_name
    ]
  end

  def banned_user_permissions
    ApplicationHelper.banned_user_permissions
  end

  # Formatting
  def self.markdown(text)
    render_options = { filter_html: true }
      # Filter HTML - remove inlined HTML tags
    unless ApplicationHelper.image_embeds_allowed
      render_options[:no_images] = true
      # No images - disables embedding images - instead, they'll be displayed
      # as "![Image](http://localhost:80/images/image.jpg)" (that is, plain
      # Markdown).
    end
    markdown_options = { autolink: true, fenced_code_blocks: true, highlight: true, space_after_headers: true, strikethrough: true, superscript: true, tables: true, underline: true }
      # Autolink - automatically make non-MD links into <a>'s, e.g.
      # "https://google.com/" will become a link without doing this:
      # "[https://google.com/](https://google.com/)"
      # Fenced code blocks - creates blocks for ```this syntax```; that is, it
      # does a monospaced font and respects whitepsace.
      # Highlight - enables ==highlighting==
      # Space after headers - you have to have a space after # to make headers
      # (i.e. #this won't be huge). You can still escape "\# like this" to
      # avoid giant headers.
      # Strikethrough - enables ~~strikesthroughs~~
      # Superscript - enables this^syntax, also^(this syntax) and^this ^syntax
      # Tables - self explanatory
      # Underline - enables _underlines_ (does not enable ++this syntax++)
      # Fancy quotes (using "quote: true") are not enabled because it uses HTML
      # <q> tags, so the ""'s are not selectable.
    Redcarpet::Markdown.new(RedcarpetHelper::BootstrapRenderer.new(render_options), markdown_options).render(text).html_safe
  end

  def markdown(text)
    ApplicationHelper.markdown(text)
  end

  def self.markdown_to_plaintext(text)
    # Takes a string with Markdown-formatted text and returns it as plaintext
    # for easier searching
    Redcarpet::Markdown.new(Redcarpet::Render::StripDown).render(text)
  end

  def markdown_to_plaintext(text)
    ApplicationHelper.markdown_to_plaintext(text)
  end

  def self.domain(url)
    URI.parse(url).host
  end

  def self.image_path_regex
    # Matches "%{image_path('filename')}" and "%{image_path('filename.ext')}"
    # For use in config/locales/domains.lang.yml
    /%\{image_path\(['"](\S+)['"]\)\}/
  end

  def self.image_embeds_allowed
    Rails.configuration.application[:messages][:images][:embed_allowed]
  end

  def self.image_embed_max_size
    Rails.configuration.application[:messages][:images][:embed_max_width]
  end

  def self.default_theme_color
    # HTML color code for the Light (Default) theme-color meta tag
    # It's just the navbar color for the Light theme
    "#007bff"
  end

  #def link(text, target, attributes = {})
  #  attributes['data-remote'] = true
  #  super
  #end

  # Sorting
  def sort_direction(direction: nil, human_readable: false)
    if direction && human_readable
      return {'asc': 'Ascending', 'desc': 'Descending'}[direction.to_sym]
    end
    user_direction = Current.user.user_preference.default_sort_order
    %w[asc desc].include?(params[:direction]) ? params[:direction] : user_direction
      # Only uses user preferences default direction when user has not
      # clicked on any columns
  end

  def inverse_sort_direction
    if sort_direction == "asc" then "desc" else "asc" end
  end

  # Datetime Handling
  def self.datetime_from_str(date_str, user: Current.user, format: nil,
                             has_tz: false, use_seconds: false, use_utc: false)
    # Turns a string into a datetime.
    # format - format string to use with strptime.
    #   More info on date formatting: https://ruby-doc.org/stdlib-2.6.1/libdoc/date/rdoc/DateTime.html#method-i-strftime
    # has_tz - if the string has the timezone offset.
    #   If true, the string has the timezone offset already.
    #   If false, the string does not have the timezone offset and it will
    #   have to be added manually.
    # use_seconds - if the string will have seconds in it or not.
    # use_utc - assume the given string is in UTC.
    #   If true, no offset will be applied.
    #   If false, the string will be converted with the user's timezone offset.
    unless date_str
      raise ArgumentError, "date_str cannot be nil"
    end
    unless date_str.is_a?(String)
      raise ArgumentError, "date_str must be of type String"
    end
    format ||= ApplicationHelper.user_datetime_format(user: user, seconds: use_seconds, with_tz: true)
    unless has_tz # Add the timezone
      if use_utc
        date_str << " #{UserPreference.utc_tz_offset}"
      else
        date_str << " #{user.user_preference.tz_offset}"
      end
    end
    date_dt = DateTime.strptime(date_str, format)
  end

  def self.datetime_to_str(datetime, user: Current.user, format: nil,
                           show_offset: true, show_seconds: false,
                           use_utc: false)
    # This method takes a datetime (as ActiveSupport::TimeWithZone,
    # DateTime, or Time) and converts it into a String representation
    # in the current user's format with their offset.
    # format - format string to use with strftime.
    #   More info on date formatting: https://ruby-doc.org/stdlib-2.6.1/libdoc/date/rdoc/DateTime.html#method-i-strftime
    # show_offset - if the output needs to show the (UTC or user's) offset.
    #   If true, the offset will be added as "+hh:mm", or "-hh:mm".
    #   If false, no offset will be displayed.
    # use_utc - convert datetime to UTC before outputting.
    #   If true, the time will be converted to UTC (if not already in UTC).
    #   If false, the time will be converted to the user's timezone offset
    #   (if not already in the user's timezone).
    unless datetime
      raise ArgumentError, "datetime cannot be nil"
    end
    unless datetime.is_a?(ActiveSupport::TimeWithZone) || datetime.is_a?(DateTime) || datetime.is_a?(Time)
      raise ArgumentError, "datetime must be one of: ActiveSupport::TimeWithZone, DateTime, Time"
    end

    format ||= ApplicationHelper.user_datetime_format(user: user, seconds: show_seconds, with_tz: show_offset)

    if use_utc
      offset = UserPreference.utc_tz_offset
      datetime.to_datetime.new_offset(offset).strftime(format)
      # Have to use .to_datetime since ActiveSupport::TimeWithZone does not
      # have the .new_offset method.
    else
      offset = user.user_preference.tz_offset
      datetime.to_datetime.new_offset(offset).strftime(format)
    end
  end

  def self.datetime_from_epoch(epochtime)
    # Takes an epoch timestamp and converts it to a datetime object.
    # Assumes epochtime is in milliseconds. Can be String or Integer.
    # Unix timestamps are always UTC, so no offset calculations are needed.
    unless epochtime
      raise ArgumentError, "epochtime cannot be nil"
    end
    unless epochtime.to_i.to_s == epochtime.to_s
      raise ArgumentError, "epochtime must be an Integer"
    end

    epochtime = epochtime.to_s
    DateTime.strptime(epochtime, '%Q')
  end

  def self.datetime_to_epoch(datetime)
    # Converts a datetime to a Unix timestamp in milliseconds (integer).
    # Unix timestamps are always UTC, so no offset calculations are needed.
    unless datetime
      raise ArgumentError, "datetime cannot be nil"
    end
    unless datetime.is_a?(ActiveSupport::TimeWithZone) || datetime.is_a?(DateTime) || datetime.is_a?(Time)
      raise ArgumentError, "datetime must be one of: ActiveSupport::TimeWithZone, DateTime, Time"
    end

    datetime.strftime('%Q').to_i # %Q - Unix timestamp, in milliseconds
  end

  # Regex Handling

  def self.is_regex?(str)
    # Returns true if str is regex. Returns false if it is not or if it is
    # escaped regex.
    str[0] == '/' && str[-1] == '/'
  end

  def self.isolate_regex(str)
    # Returns str, minus the first and last characters (which should be '/'
    # if you are calling this method).
    str[1...-1]
  end

  def self.is_escaped_regex?(str)
    # Returns true if str is escaped regex. Returns false if it is regex or
    # of it is not regex at all.
    str[0, 2] == '\\/'
  end

  def self.unescape_regex(str)
    # Returns an escaped version of the string.
    if str[0, 2] == '\\/'
      str = str[1, str.length] # Remove leading \, preserve /
      if str[-2, str.length] == '\\/'
        # Only modify the end of the string if the front was also escaped
        str = str[0, str.length - 2] << '/' # Remove \/, re-add /
      end
    end
    str
  end

  def self.pg_valid_regex?(str)
    # Returns true if the regex is valid in Postgres, false if not valid.
    # Note that valid Postgres regex != valid Ruby regex. For example,
    # "\mHello\M" is valid Postgres but invalid Ruby regex, which is why
    # using Regexp.new(str) (which would throw an exception for "\mHello\M")
    # is not an option for validating regex.
    # Credit for this goes to https://stackoverflow.com/a/48391562

    # There is no Rails method for testing regex for DBMS compatibility,
    # so I made this method to safely test regex without relying on string
    # interpretation, which would be vulnerable to SQL injections.
    binds = [
      ActiveRecord::Relation::QueryAttribute.new("regex_to_test", str,
                                                 ActiveRecord::Type::Text.new)
    ]
    # Explanation: binds is only used by exec_query if the elements are
    # QueryAttribute objects, so I have to construct the QueryAttributes.
    # Using "regex_to_test" is optional (I could just use ""). Normally, this
    # is the param name (e.g. "id", "channel", "message"), but since I am not
    # searching a specific parameter (this helper method is intended to be
    # used for all regex validation), I made one up so it would be easy to
    # identify in the logs.
    # str - self-explanatory.
    # ActiveRecord::Type::Text.new - datatype for string.
    begin
      ActiveRecord::Base.connection.exec_query("SELECT regexp_matches('', $1);",
                                               'PG Regex Test', binds)
    rescue ActiveRecord::StatementInvalid
      return false
    end
    return true
    # When dealing with exec_query, you do not use ?, you use $n. $1 maps to
    # binds[0][1], $2 maps to binds[1][1], ..., $n maps to binds[n][1].
    # regexp_matches('', $1) - arg0 is the string to match against, arg1 is
    # the regex, arg2 is any special flags (optional). For testing regex
    # validity, we do not need to provide any text to match against, hence
    # the ''. Source:
    # https://www.postgresql.org/docs/current/functions-matching.html#FUNCTIONS-POSIX-TABLE
    # 'PG Regex Test' - query name which will be logged. This is the initial
    # part of a SQL statement in Rails logs. For example, in:
    # User Load (0.6ms)  SELECT  "users".* FROM "users" WHERE "users"."id" = $1 LIMIT $2  [["id", 2], ["LIMIT", 1]]
    # "User Load" is the query name. In the logs, whenever this method is
    # called, the log will look something like this:
    # PG Regex Test (0.8ms)  SELECT regexp_matches('', $1);  [["regex_to_test", "foo\\M\s+\mbar"]]
  end

  # Common Form Settings

  def self.common_prefs(user: Current.user)
    prefs = {}
    # Set datetime format variable for date picker in view to reference
    prefs[:js_dt_format] = date_format = PreferencesHelper.valid_date_formats.key(user.user_preference.date_format).to_s
    prefs[:js_dt_format] << " "
    prefs[:js_dt_format] << ApplicationHelper.time_format(format: :momentjs, use_24hr_time: user.user_preference.use_24hr_time, seconds: false)
      # https://tempusdominus.github.io/bootstrap-4/Options/#format
      # http://momentjs.com/docs/#/displaying/format/
    # Set datetime format variable for help menu in view to reference
    if prefs[:js_dt_format].end_with?(" A")
      prefs[:user_dt_format] = prefs[:js_dt_format][0...prefs[:js_dt_format].length - 1]
      prefs[:user_dt_format] << "AM/PM"
      # Remove "A" from end and replace with "AM/PM" if 12 hour time
    else
      prefs[:user_dt_format] = prefs[:js_dt_format]
      # 24 hour time needs no modifications
    end
    prefs[:per_page] = user.user_preference.search_results
    prefs[:tz_offset] = user.user_preference.tz_offset

    prefs
  end

  # Error Handling

  def self.pg_invalid_regex_locator
    # This is regex that isolates the specific regex error message from Postgres.
    # For example, from this error message:
    #   PG::InvalidRegularExpression: ERROR: invalid regular expression: parentheses () not balanced : SELECT COUNT(*) FROM ...
    # The following will be captured:
    #   parentheses () not balanced
    # Sample usage:
    #   rescue RegexpError, ActiveRecord::StatementInvalid => err
    #     if err.message.match(ApplicationHelper.pg_invalid_regex_locator)
    #       errors << "One or more invalid regular expressions (details: #{err.message.match(ApplicationHelper.pg_invalid_regex_locator)[1]})"
    #     else
    #       errors << "One or more invalid regular expressions (unable to extract details from error message)"
    #     end
    #   end
    /(?:PG\:\:InvalidRegularExpression\:\sERROR\:\s+invalid\sregular\sexpression\:\s)(.*)(?:\s\:\sSELECT.*|\\n\:\sSELECT.*)/
  end

  #private

  # Date/Time Helpers

  def self.date_formats(format)
    # Get the key for a given value
    if format_value
      return PreferencesHelper.valid_date_formats.key(format_value)
    else
      raise ArgumentError, "Must have a non-nil format"
    end
  end

  def self.time_format(format: :ruby, use_24hr_time: true, seconds: true)
    valid_formats = [:ruby, :momentjs]
    # Ruby reference: https://foragoodstrftime.com/
    # MomentJS reference: http://momentjs.com/docs/#/displaying/format/

    if format == :ruby
      if use_24hr_time
        if seconds
          return "%H:%M:%S"
        else # !seconds
          return "%H:%M"
        end
      else # !use_24hr_time
        if seconds
          return "%I:%M:%S %p"
        else # !seconds
          return "%I:%M %p"
        end
      end
    elsif format == :momentjs
      if use_24hr_time
        if seconds
          return "HH:mm:ss"
        else # !seconds
          return "HH:mm"
        end
      else # !use_24hr_time
        if seconds
          return "hh:mm:ss A"
        else # !seconds
          return "hh:mm A"
        end
      end
    else
      raise ArgumentError, "Invalid format: #{format}. Must be one of: " \
        "#{valid_formats}."
    end
  end

  def self.user_datetime_format(user: Current.user, format: :datetime,
                                seconds: true, with_tz: false)
    # Returns the user's datetime format
    # format - one of [:datetime, :date, :time]
    # seconds - include seconds in format string
    # with_tz - include timezone in format string
    case format
    when :datetime
      datetime_format = user.user_preference.date_format.clone
      # I have to use .clone on this because if I do not, it copies a reference
      # to the record in the database, meaning when I append the time format,
      # it modifies the UserPreference record.
      # HOWEVER, for some reason, it only does this in controllers. That is,
      # if you call this method from Conversations#index, the attribute changes
      # from "%Y-%m-%d" to "%Y-%m-%d %H:%M" to "%Y-%m-%d %H:%M %Z". The second
      # time you call this method from Conversations#index, it will end up with
      # it changing "%Y-%m-%d %H:%M %Z" into "%Y-%m-%d %H:%M %Z %H:%M", and
      # then into "%Y-%m-%d %H:%M %Z %H:%M %Z".
      # When being called from a view, this *never* happens. The value does not
      # mutate. My only guess is that a controller method has state that
      # extends to the end of the current scope (method), and when that value
      # is not written to the database, it is discarded. Views have scope too
      # (you can define variables in ERB), but it might behave differently?
      # I have no idea, really. If someone has a good explanation, please let
      # me know.
      datetime_format << " #{ApplicationHelper.time_format(use_24hr_time: user.user_preference.use_24hr_time, seconds: seconds)}"
      if with_tz
        datetime_format << " %Z"
      end
      return datetime_format
    when :date
      return user.user_preference.date_format
    when :time
      time_format = ApplicationHelper.time_format(use_24hr_time: user.user_preference.use_24hr_time, seconds: seconds)
      if with_tz
        time_format << " %Z"
      end
      return time_format
    else
      raise ArgumentError, "Invalid format option: #{format}"
    end
  end

  def self.user_tz_offset_ms(user: Current.user, offset_str: nil)
    if !offset_str
      offset_str = user.user_preference.tz_offset
    end
    
    # Parse user's UTC timezone offset
    tz_regex = /^([+-]?)(\d\d):(\d\d)$/
    # Group 0 - entire string
    # Group 1 - +/- (or '' if neither are present)
    # Group 2 - hour offset
    # Group 3 - minute offset
    tz_match = offset_str.match(tz_regex)
    if !tz_match.nil? && tz_match.length == 4
      tz_offset = tz_match[2].to_i * 60000 * 60 # 1 hour = 3,600,000 ms
      tz_offset += tz_match[3].to_i * 60000 # 1 minute = 60,000 ms
      if tz_match[1] == '-'
        tz_offset *= -1
      end
      return tz_offset
    else
      logger.error "Unable to parse tz_offset \"#{offset_str}\" for user with ID #{user.id}."
      return 0
    end
  end
end

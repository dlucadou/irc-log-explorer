module ChatLogsHelper
  include ApplicationHelper

  # Sorting
  def chatlog_sort_link(column, title = nil)
    # The title parameter is used if the column name isn't what I want to use
    title ||= column.titleize
    direction = column == chatlog_sort_column && sort_direction == "asc" ? "desc" : "asc"
    icon = sort_direction == "asc" ? "fas fa-caret-up" : "fas fa-caret-down"
    icon = column == chatlog_sort_column ? icon : ""
    link_to "#{title} <span class='#{icon}'></span>".html_safe, params.permit(:column, :direction, :channel, :sender, :query, :advanced_query, :case_sensitive, :base_id, :start_id, :end_id, :base_time, :start_time, :end_time, :valid_search, :page, :utf8, :highlight, :per_page).merge(:column => column, :direction => direction)
  end

  def chatlog_sortable_columns
    ["channel", "sender", "date"]
  end

  def chatlog_sort_column
    user_column = Current.user.user_preference.default_sort_col
    chatlog_sortable_columns.include?(params[:column]) ? params[:column] : user_column
      # Only uses user preferences default column sort when user has
      # not clicked on any other columns
  end

  # Validations
  def self.chatlog_channel_regex
    if Rails.configuration.application[:chat_logs][:channel][:require_prefix]
      /^[#&][a-z0-9_]{#{Rails.configuration.application[:chat_logs][:channel][:min_length]},#{Rails.configuration.application[:chat_logs][:channel][:max_length]}}$/
    else
      /^[#&]?[a-z0-9_]{#{Rails.configuration.application[:chat_logs][:channel][:min_length]},#{Rails.configuration.application[:chat_logs][:channel][:max_length]}}$/
    end
  end

  def chatlog_channel_regex
    ChatLogsHelper.chatlog_channel_regex
  end

  def self.chatlog_channel_regex_description
    # This has 2 paths depending on the configuration, hence this method rather
    # than just calling the I18n string from each instance it is needed in the
    # code.
    min_length = Rails.configuration.application[:chat_logs][:channel][:min_length]
    max_length = Rails.configuration.application[:chat_logs][:channel][:max_length]
    if Rails.configuration.application[:chat_logs][:channel][:require_prefix]
      #"must start with a # or & and contain #{Rails.configuration.application[:chat_logs][:channel][:min_length]}-#{Rails.configuration.application[:chat_logs][:channel][:max_length]} lowercase letters, numbers, or underscores"
      I18n.t('.activerecord.errors.models.chat_log.attributes.channel.invalid_require_prefix',
             min: min_length, max: max_length)
    else
      #"may start with a # or & and must contain #{Rails.configuration.application[:chat_logs][:channel][:min_length]}-#{Rails.configuration.application[:chat_logs][:channel][:max_length]} lowercase letters, numbers, or underscores"
      I18n.t('.activerecord.errors.models.chat_log.attributes.channel.invalid_optional_prefix',
             min: min_length, max: max_length)
    end
  end

  def chatlog_channel_regex_description
    ChatLogsHelper.chatlog_channel_regex_description
  end

  def self.chatlog_sender_regex
    /^[a-z0-9_]{#{Rails.configuration.application[:chat_logs][:sender][:min_length]},#{Rails.configuration.application[:chat_logs][:sender][:max_length]}}$/
  end

  def chatlog_sender_regex
    ChatLogsHelper.chatlog_sender_regex
  end

  def self.chatlog_sender_regex_description
    min_length = Rails.configuration.application[:chat_logs][:sender][:min_length]
    max_length = Rails.configuration.application[:chat_logs][:sender][:max_length]
    #"must contain #{Rails.configuration.application[:chat_logs][:sender][:min_length]}-#{Rails.configuration.application[:chat_logs][:sender][:max_length]} lowercase letters, numbers, or underscores"
    I18n.t('.activerecord.errors.models.chat_log.attributes.sender.invalid',
           min: min_length, max: max_length)
  end
  
  def chatlog_sender_regex_description
    ChatLogsHelper.chatlog_sender_regex_description
  end
  
  def self.chatlog_max_id
    if User.type_for_attribute('id') == :bigint
      2**63-1
    else
      2**31-1
    end
  end
  
  def chatlog_max_id
    ChatLogsHelper.chatlog_max_id
  end

  def self.chatlog_edit_path_regex
    # Used to make sure a URL is for editing a ChatLog in the _form view, as
    # the delete button is only shown for existing ChatLogs, and you cannot
    # call "current_page?(controller: 'chat_logs', action: 'edit')" from the
    # create view, as it will throw an error (since a non-existent ChatLog
    # cannot have an edit view).
    # This will match "chat_logs/123" in "/chat_logs/123/edit". I did not try
    # to match "/edit" in the URL, as a validation error will cause a redirect
    # with _form rendering on "/chat_logs/123". This ensures the first and n-th
    # attempts at editing a ChatLog will still display the delete button.
    /chat_logs\/\d+/
  end
end

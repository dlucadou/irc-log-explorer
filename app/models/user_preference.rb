class UserPreference < ApplicationRecord
  belongs_to :user
  validates_with UserPreferenceValidator

  def self.default_tz_offset
    UserPreference.column_defaults['tz_offset']
    # Credit for this goes to https://stackoverflow.com/a/34659251
  end

  def self.utc_tz_offset
    "+00:00"
    # This is the column default, but I created this method just incase someone
    # modified the schema in their deployment.
  end
end

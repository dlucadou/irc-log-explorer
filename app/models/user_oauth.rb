class UserOauth < ApplicationRecord
  belongs_to :user
  validates_uniqueness_of :user_id, scope: [:domain, :provider]
  validates_uniqueness_of :uid, scope: [:domain, :provider]
  # Ensures no user can have 2 Oauth records from the same provider, and no UID
  # can be shard with multiple providers (so you cannot add the same Oauth
  # login to multiple Users).
  # I have this implemented as a constraint in the DB schema, but using this
  # model check will provide easier to handle exceptions.
  # https://api.rubyonrails.org/classes/ActiveRecord/Validations/ClassMethods.html#method-i-validates_uniqueness_of

  def self.create_oauth_record(auth, domain, user)
    # auth - auth Hash from OmniAuth
    # domain - the domain the UserOauth will be created for
    # user - the User record to create this for
    user_oauth = UserOauth.new(user_id: user.id, provider: auth.provider,
                               token: auth.credentials.token,
                               uid: auth.uid, domain: domain)
    case auth.provider
    when "discord"
      user_oauth.email = auth.info.email
      user_oauth.username = auth.extra.raw_info.username + '#'
                            + auth.extra.raw_info.discriminator
    when "facebook"
      user_oauth.email = auth.info.email
      user_oauth.username = auth.extra.raw_info.username

      # Facebook's token doesn't last forever
      user_oauth.expires_at = Time.at(auth.credentials.expires_at)
    when "github", "gitlab", "twitch"
      user_oauth.email = auth.info.email
      if auth.info.nickname? # GitHub, Twitch
        user_oauth.username = auth.info.nickname
      else # GitLab
        user_oauth.username = auth.info.username
      end
    when "google_oauth2"
      user_oauth.email = auth.info.email

      # Google's token doesn't last forever
      user_oauth.expires_at = Time.at(auth.credentials.expires_at)
    when "twitter"
      user_oauth.email = auth.info.email
        # Again, I'm just assuming this is where it would be if you have
        # elevated permissions, not sure if this is the correct location.
      user_oauth.username = auth.info.nickname
    end

    user_oauth.save!
  end

  def self.normalize_oauth_name(provider)
    case provider.to_s
    when "discord"
      provider = "Discord"
    when "facebook"
      provider = "Facebook"
    when "github"
      provider = "GitHub"
    when "gitlab"
      provider = "GitLab"
    when "google_oauth2"
      provider = "Google"
    when "twitter"
      provider = "Twitter"
    when "twitch"
      provider = "Twitch"
    else
      provider = "unknown"
    end
    return provider
  end
end

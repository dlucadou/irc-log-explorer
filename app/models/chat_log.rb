class ChatLog < ApplicationRecord
  attr_accessor :flash_message
  has_many :chat_log_edits, dependent: :nullify
  accepts_nested_attributes_for :chat_log_edits
  validates_with LogValidator

  def self.max_message_length
    Rails.configuration.application[:chat_logs][:message][:max_length]
  end

  # Search helper methods

  def self.search(channel: nil, sender: nil, basic_search: nil, advanced_search: nil,
                  case_sensitive: nil, start_id: nil, end_id: nil, start_time: nil,
                  end_time: nil, limit: nil, sort: nil)
    with_channel(channel)
      .with_sender(sender)
      .with_basic_search(basic_search)
      .with_advanced_search(advanced_search, case_sensitive)
      .with_id_range(start_id, end_id)
      .with_time_range(start_time, end_time)
      .with_order(sort)
      .with_limit(limit)
  end

  def self.validate_search(channel: nil, sender: nil, basic_search: nil,
                           advanced_search: nil, case_sensitive: nil,
                           start_id: nil, end_id: nil, start_time: nil,
                           end_time: nil, limit: nil, sort: nil, per_page: nil)
    errors = {}

    # Validate channel
    if channel.present? && !channel.match(ChatLogsHelper.chatlog_channel_regex)
      errors[I18n.t('.activerecord.errors.models.chat_log.attributes.channel.name')] =
        ChatLogsHelper.chatlog_channel_regex_description
    end

    # Validate sender
    if sender.present? && !sender.match(ChatLogsHelper.chatlog_sender_regex)
      errors[I18n.t('.activerecord.errors.models.chat_log.attributes.sender.name')] =
        ChatLogsHelper.chatlog_sender_regex_description
    end

    # No validation needed for basic_search

    # Validate advanced_search
    if advanced_search.present? && ApplicationHelper.is_regex?(advanced_search) &&
        !ApplicationHelper.pg_valid_regex?(ApplicationHelper.isolate_regex(advanced_search))
       errors[I18n.t('.activerecord.errors.models.chat_log.attributes.message.name')] =
         I18n.t('.activerecord.errors.models.chat_log.attributes.message.invalid_regex')
    end

    # No validation needed for case_sensitive

    # Validate start_id
    # Make sure start_id is int and in range
    if start_id.present? && start_id.to_i.to_s != start_id.to_s # start_id is not an int
      errors[I18n.t('.activerecord.errors.models.chat_log.attributes.start_id.name')] =
        I18n.t('.activerecord.errors.models.chat_log.attributes.start_id.invalid')
    elsif start_id.present? && start_id.to_i <= 0 # start_id is a zero/negative int
      errors[I18n.t('.activerecord.errors.models.chat_log.attributes.start_id.name')] =
        I18n.t('.activerecord.errors.models.chat_log.attributes.start_id.invalid_min_id')
    elsif start_id.present? && start_id.to_i > ChatLogsHelper.chatlog_max_id # start_id is an int that is too large
      errors[I18n.t('.activerecord.errors.models.chat_log.attributes.start_id.name')] =
        I18n.t('.activerecord.errors.models.chat_log.attributes.start_id.invalid_max_id',
               max: ChatLogsHelper.chatlog_max_id)
    elsif start_id.present?
      start_id_valid = true
    end

    # Validate end_id
    if end_id.present? && end_id.to_i.to_s != end_id.to_s # end_id is not an int
      errors[I18n.t('.activerecord.errors.models.chat_log.attributes.end_id.name')] =
        I18n.t('.activerecord.errors.models.chat_log.attributes.end_id.invalid')
    elsif end_id.present? && end_id.to_i <= 0 # end_id is a zero/negative int
      errors[I18n.t('.activerecord.errors.models.chat_log.attributes.end_id.name')] =
        I18n.t('.activerecord.errors.models.chat_log.attributes.end_id.invalid_min_id')
    elsif end_id.present? && end_id.to_i > ChatLogsHelper.chatlog_max_id # end_id is an int that is too large
      errors[I18n.t('.activerecord.errors.models.chat_log.attributes.end_id.name')] =
        I18n.t('.activerecord.errors.models.chat_log.attributes.end_id.invalid_max_id',
               max: ChatLogsHelper.chatlog_max_id)
    elsif end_id.present?
      if start_id_valid && start_id.to_i > end_id.to_i # end_id <= start_id
        errors[I18n.t('.activerecord.errors.models.chat_log.attributes.start_id.name')] =
          I18n.t('.activerecord.errors.models.chat_log.attributes.start_id.invalid_end_id')
      end
    end

    # Validate start_time & end_time
    if start_time.present? && start_time.length > 0 # start_time present
      begin
        start_dt = ApplicationHelper.datetime_from_str(start_time) # start_time valid datetime
      rescue ArgumentError # Invalid start_time
        errors[I18n.t('.activerecord.errors.models.chat_log.attributes.start_date.name')] =
          I18n.t('.activerecord.errors.models.chat_log.attributes.start_date.invalid')
        start_dt = nil
      end

      if end_time.present? && end_time.length > 0 # end_time present
        begin
          end_dt = ApplicationHelper.datetime_from_str(end_time) # end_time valid datetime
          if start_dt && start_dt >= end_dt # end_time !> start_time
            errors[I18n.t('.activerecord.errors.models.chat_log.attributes.start_date.name')] =
              I18n.t('.activerecord.errors.models.chat_log.attributes.start_date.invalid_end_time')
            start_dt = nil
            end_dt = nil
          end
        rescue ArgumentError # Invalid end_time
          errors[I18n.t('.activerecord.errors.models.chat_log.attributes.end_date.name')] =
            I18n.t('.activerecord.errors.models.chat_log.attributes.end_time.invalid')
          end_dt = nil
        end
      end
    elsif end_time.present? && end_time.length > 0 # end_time present
      start_dt = nil
      begin
        end_dt = ApplicationHelper.datetime_from_str(end_time) # end_time valid datetime
      rescue ArgumentError # Invalid end_time
        errors[I18n.t('.activerecord.errors.models.chat_log.attributes.end_date.name')] =
          I18n.t('.activerecord.errors.models.chat_log.attributes.end_time.invalid')
        end_dt = nil
      end
    end

    # Validate per_page
    if per_page.present? && (per_page.to_i.to_s != per_page.to_s || !PreferencesHelper.valid_per_page(per_page))
      # Either per_page is not an int OR is is not a valid number
      errors[I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.name')] =
        I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.invalid', min: PreferencesHelper.min_per_page, max: PreferencesHelper.max_per_page)
    end

    return errors
  end

  private

  scope :with_channel, proc { |channel|
    if channel.present?
      where(channel: channel)
    end
  }

  scope :with_sender, proc { |sender|
    if sender.present?
      where(sender: sender)
    end
  }

  scope :with_basic_search, proc { |basic_search|
    if basic_search.present?
      where("lower(message) ILIKE ?", "%#{basic_search.downcase}%")
    end
  }

  scope :with_advanced_search, proc { |advanced_search, case_sensitive|
    if advanced_search.present?
      if ApplicationHelper.is_regex?(advanced_search) # Using regex
        if !case_sensitive.nil?
          if case_sensitive == "true" # The parameter is casted to a string before arriving here, so I have to compare to a string to determine truthfulness
            where("message ~ ?", ApplicationHelper.isolate_regex(advanced_search))
          else
            where("message ~* ?", ApplicationHelper.isolate_regex(advanced_search))
          end
        else
          # Assume case sensitive regex
          where("message ~ ?", ApplicationHelper.isolate_regex(advanced_search))
        end
      else # Not using regular expressions
        # See if they escaped forward slashes at the start and/or end of the string,
        # and if so, remove them.
        # I do this because having forward slashes at the start and end of the string
        # indicates regex, but they can escape this with backslashes.
        if ApplicationHelper.is_escaped_regex?(advanced_search)
          advanced_search = ApplicationHelper.unescape_regex(advanced_search)
        end

        if !case_sensitive.nil?
          if case_sensitive == "true" # The parameter is casted to a string before arriving here, so I have to compare to a string to determine truthfulness
            where("message LIKE ?", "%#{advanced_search}%")
          else
            where("lower(message) ILIKE ?", "%#{advanced_search.downcase}%")
          end
        else # Assume not case sensitive
          where("lower(message) ILIKE ?", "%#{advanced_search.downcase}%")
        end
      end
    end
  }

  scope :with_id_range, proc { |start_id, end_id|
    # Make sure start_id is int and in range

    if start_id.present?
      if end_id.present? # Valid start & end ID
        where("id >= ? AND id <= ?", start_id, end_id)
      else # Only valid start ID
        where("id >= ?", start_id)
      end
    elsif end_id.present? # Only valid end ID
      where("id <= ?", end_id)
    end
  }

  scope :with_time_range, proc { |start_time, end_time|
    if start_time.present? && start_time.length > 0 # start_time present
      start_dt = ApplicationHelper.datetime_from_str(start_time) # start_time valid datetime

      if end_time.present? && end_time.length > 0 # end_time present
        end_dt = ApplicationHelper.datetime_from_str(end_time) # end_time valid datetime
      else # end_time not present
        end_dt = nil
      end
    elsif end_time.present? && end_time.length > 0 # end_time present
      start_dt = nil
      end_dt = ApplicationHelper.datetime_from_str(end_time) # end_time valid datetime
    else # Neither start_time nor end_time present
      start_dt = nil
      end_dt = nil
    end

    if start_dt && end_dt
      start_dt_epoch = ApplicationHelper.datetime_to_epoch(start_dt)
      end_dt_epoch = ApplicationHelper.datetime_to_epoch(end_dt)
      where("date >= ? AND date <= ?", start_dt_epoch, end_dt_epoch)
    elsif start_dt
      start_dt_epoch = ApplicationHelper.datetime_to_epoch(start_dt)
      where("date >= ?", start_dt_epoch)
    elsif end_dt
      end_dt_epoch = ApplicationHelper.datetime_to_epoch(end_dt)
      where("date <= ?", end_dt_epoch)
    end
  }

  scope :with_order, proc { |sort|
    if sort.present?
      order(sort)
    end
  }

  scope :with_limit, proc { |limit|
    if limit.present?
      limit(limit)
    end
  }
end

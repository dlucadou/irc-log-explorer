class ChatLogEditValidator < ActiveModel::Validator
  extend ActiveSupport::Concern

  def validate(chat_log_edit)
    min_internal_length = ChatLogEdit.internal_justification_min_length
    max_internal_length = ChatLogEdit.internal_justification_max_length
    min_external_length = ChatLogEdit.external_justification_min_length
    max_external_length = ChatLogEdit.external_justification_max_length

    if chat_log_edit.chat_log_edited.nil? || chat_log_edit.chat_log_id.nil?
      chat_log_edit.errors.add(I18n.t('.activerecord.errors.models.chat_log_edit.attributes.chat_log_id.name'),
                               I18n.t('.activerecord.errors.models.chat_log_edit.attributes.default.not_present'))
      logger.error('ChatLogEdit chat_log_edited field is nil somehow') if chat_log_edit.chat_log_edited.nil?
      logger.error('ChatLogEdit chat_log_id field is nil somehow') if chat_log_edit.chat_log_id.nil?
    elsif chat_log_edit.chat_log_edited != chat_log_edit.chat_log_id
      chat_log_edit.errors.add(I18n.t('.activerecord.errors.models.chat_log_edit.attributes.chat_log_id.name'),
                               I18n.t('.activerecord.errors.models.chat_log_edit.attributes.default.not_matching'))
      logger.error('ChatLogEdit chat_log_edited field does not match chat_log_id somehow')
      logger.debug("ChatLogEdit chat_log_edited: #{chat_log_edit.chat_log_edited} chat_log_id: #{chat_log_edit.chat_log_id}")
    end

    if chat_log_edit.edited_by.nil? || chat_log_edit.editor_id.nil?
      chat_log_edit.errors.add(I18n.t('.activerecord.errors.models.edited_by.attributes.edited_by.name'),
                               I18n.t('.activerecord.errors.models.chat_log_edit.attributes.default.not_present'))
      logger.error('ChatLogEdit edited_by field is nil somehow') if chat_log_edit.edited_by.nil?
      logger.error('ChatLogEdit editor_id field is nil somehow') if chat_log_edit.editor_id.nil?
    elsif chat_log_edit.edited_by != chat_log_edit.editor_id
      chat_log_edit.errors.add(I18n.t('.activerecord.errors.models.chat_log_edit.attributes.edited_by.name'),
                               I18n.t('.activerecord.errors.models.chat_log_edit.attributes.default.not_matching'))
      logger.error('ChatLogEdit chat_log_edited_by field does not match chat_log_editor_id somehow')
      logger.debug("ChatLogEdit chat_log_edited: #{chat_log_edit.chat_log_edited_by} chat_log_id: #{chat_log_edit.chat_log_editor_id}")
    end

    if chat_log_edit.internal_justification.nil?
      chat_log_edit.errors.add(I18n.t('.activerecord.errors.models.chat_log_edit.attributes.internal_justification.name'),
                               I18n.t('.activerecord.errors.models.chat_log_edit.attributes.default.not_present'))
      logger.error('ChatLogEdit internal_justification field is nil somehow')
    elsif chat_log_edit.internal_justification.length < min_internal_length || chat_log_edit.internal_justification.length > max_internal_length
      chat_log_edit.errors.add(I18n.t('.activerecord.errors.models.chat_log_edit.attributes.internal_justification.name'),
                               I18n.t('.activerecord.errors.models.chat_log_edit.attributes.default.length', min: min_internal_length, max: max_internal_length))
    end

    if chat_log_edit.external_justification.nil?
      chat_log_edit.errors.add(I18n.t('.activerecord.errors.models.chat_log_edit.attributes.external_justification.name'),
                               I18n.t('.activerecord.errors.models.chat_log_edit.attributes.default.not_present'))
      logger.error('ChatLogEdit external_justification field is nil somehow')
    elsif chat_log_edit.external_justification.length < min_external_length || chat_log_edit.external_justification.length > max_external_length
      chat_log_edit.errors.add(I18n.t('.activerecord.errors.models.chat_log_edit.attributes.external_justification.name'),
                               I18n.t('.activerecord.errors.models.chat_log_edit.attributes.default.length', min: min_external_length, max: max_external_length))
    end
  end
end

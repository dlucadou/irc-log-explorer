class UserPreferenceValidator < ActiveModel::Validator
  include PreferencesHelper
  
  def validate(user_preference)
    if !Current.user.nil?
      current_user_prefs = Current.user.user_preference
      current_user_prefs ||= current_user.user_preference
      i18n_base = ".activerecord.errors.models.user_preference.attributes"
      
      ## Appearance Preferences
      
      #valid_booleans = ['true', 'false']
      #valid_date_formats = {'YYYY-MM-DD': '%Y-%m-%d', 'YYYY-DD-MM': '%Y-%d-%m', 'MM-DD-YYYY': '%m-%d-%Y', 'DD-MM-YYYY': '%d-%m-%Y', 'MM-YYYY-DD': '%m-%Y-%d', 'DD-YYYY-MM': '%d-%Y-%m'}
      #valid_sort_columns = {'Channel': 'channel', 'Sender': 'sender', 'Time': 'date'}
      #valid_sort_orders = ['asc', 'desc']
      #valid_themes = ['Dark', 'Light', 'Solarized (Dark)', 'Ubuntu (Light)']
      #tz_offset_regex = /(^[+-]([01]\d|[2][0123]):[012345]\d$)|(^00:00$)/
      
      # Ensure user_id is present & valid
      if !user_preference.user_id.nil? && User.find_by_id(user_preference.user_id).nil?
        user_preference.errors.add(I18n.t("#{i18n_base}.user_id.name"), I18n.t("#{i18n_base}.user_id.invalid"))
        Rails.logger.warn "User with ID #{Current.user.id} has submitted the preferences form for a user that does not exist (#{user_preference.user_id})."
      elsif !user_preference.user_id.nil? && user_preference.user_id != Current.user.id
        user_preference.errors.add(I18n.t("#{i18n_base}.user_id.name"), I18n.t("#{i18n_base}.user_id.mismatch"))
        Rails.logger.warn "User with ID #{Current.user.id} has submitted the preferences form for a different user (user_id #{user_preference.user_id})."
      elsif user_preference.user_id.nil?
        user_preference.errors.add(I18n.t("#{i18n_base}.user_id.name"), I18n.t("#{i18n_base}.user_id.nil"))
        Rails.logger.warn "User with ID #{Current.user.id} has submitted the preferences form with nil user_id."
      end
      # Check default_sort_col
      if user_preference.default_sort_col.nil? || valid_sort_columns.key(user_preference.default_sort_col).nil?
        user_preference.errors.add(I18n.t("#{i18n_base}.default_sort_col.name"), I18n.t("#{i18n_base}.default_sort_col.invalid"))
        Rails.logger.warn "User with ID #{Current.user.id} appears to have tampered with the UI settings form. Option sort was submitted with the value \"#{user_preference.default_sort_col}\" instead of one of #{valid_sort_columns}."
      end
      # Check default_sort_order
      if user_preference.default_sort_order.nil? || valid_sort_orders.index(user_preference.default_sort_order).nil?
        user_preference.errors.add(I18n.t("#{i18n_base}.default_sort_order.name"), I18n.t("#{i18n_base}.default_sort_order.invalid"))
        Rails.logger.warn "User with ID #{Current.user.id} appears to have tampered with the UI settings form. Option default_sort_order was submitted with the value \"#{user_preference.default_sort_order}\" instead of one of #{valid_sort_orders}."
      end
      # Check search_results
      if user_preference.search_results.nil? || !PreferencesHelper.valid_per_page(user_preference.search_results) || user_preference.search_results_before_type_cast.to_i.to_s != user_preference.search_results_before_type_cast
        # If the user tries to insert text, str.to_i is 0, so this will fail
        # If the user modifies the form to accept a decimal,
        # results.to_i.to_s == results will be false. I cannot check
        # search_results.class == Integer since it's a String in the form
        # submission, so casting to int, then string, and comparing back
        # to the original is the only way to ensure it's an integer.
        user_preference.errors.add(I18n.t("#{i18n_base}.search_results.name"), I18n.t("#{i18n_base}.search_results.invalid"))
        Rails.logger.warn "User with ID #{Current.user.id} appears to have tampered with the UI settings form. Option search_results was submitted with the value \"#{user_preference.search_results}\" instead of a number in the range [#{PreferencesHelper.min_per_page}, #{PreferencesHelper.max_per_page}]."
      end
      # Check theme
      if user_preference.theme.nil? || valid_themes.index(user_preference.theme).nil?
        #(flash[:error] ||= error_list_start) << "<li>Invalid theme option: #{prefs[:theme]}. Must be one of #{valid_themes}.</li>"
        user_preference.errors.add(I18n.t("#{i18n_base}.theme.name"), I18n.t("#{i18n_base}.theme.invalid"))
        Rails.logger.warn "User with ID #{Current.user.id} appears to have tampered with the UI settings form. Option theme was submitted with the value \"#{user_preference.theme}\" instead of one of #{valid_themes}."
      end
      
      ## Localization Preferences
      
      # Check date_format
      if user_preference.date_format.nil? || valid_date_formats.key(user_preference.date_format).nil?
        user_preference.errors.add(I18n.t("#{i18n_base}.date_format.name"), I18n.t("#{i18n_base}.date_format.invalid"))
        Rails.logger.warn "User with ID #{Current.user.id} appears to have tampered with the UI settings form. Option date_format was submitted with the value \"#{user_preference.date_format}\"."
      end
      # Check use_24hr_time
      if user_preference.use_24hr_time.nil? || valid_booleans.index(user_preference.use_24hr_time_before_type_cast).nil?
        # user_preference.use_24hr_time_before_type_cast is used because if a
        # user tampers with a form and changes "true" to "none", that gets auto-
        # casted to false. But if they previously did have 24 hour time on,
        # I do not want to disable that because their invalid response was auto-
        # casted to false.
        # To work around this, I grab the original user submission with the
        # _before_type_cast helper and check if it is one of ['true', 'false'].
        # This allows me to make sure they actually did have a valid entry
        # without worrying about changing settings for incorrect values.
        # https://stackoverflow.com/a/44459223
        user_preference.errors.add(I18n.t("#{i18n_base}.use_24hr_time.name"), I18n.t("#{i18n_base}.use_24hr_time.invalid"))
        Rails.logger.warn "User with ID #{Current.user.id} appears to have tampered with the UI settings form. Option use_24hr_time was submitted with the value \"#{user_preference.use_24hr_time}\" instead of one of #{valid_booleans}."
      end
      # Check tz_offset format
      if !user_preference.tz_offset.nil? && !tz_offset_regex.match(user_preference.tz_offset).nil?
        if !tz_offset_regex.match(user_preference.tz_offset)[3].nil?
          # When true, offset == "00:00" because:
          # Matching "00:00" will return ["00:00", nil, nil, "00:00"]
          # Matching "+00:00" will return ["+00:00", "+00:00", "00", nil]
          user_preference.tz_offset = UserPreference.default_tz_offset
        end
      else
        user_preference.errors.add(I18n.t("#{i18n_base}.tz_offset.name"), I18n.t("#{i18n_base}.tz_offset.invalid"))
      end
      
      ## Notification Preferences
      
      #valid_checkboxes = [true, false]
      user = Current.user
      user ||= current_user
      @UserSettablePreferences = user_settable_prefs(user: user)
      # @UserSettablePreferences = {
        # :notify_pwchange_pri => true,
        # :notify_pwchange_bkp => Current.user.backup_email.length > 0,
        # :notify_pwreset_pri => false,
        # :notify_pwreset_bkp => Current.user.backup_email.length > 0,
        # :notify_sso_pri => true,
        # :notify_sso_bkp => Current.user.backup_email.length > 0,
        # :notify_2fa_pri => false,
        # :notify_2fa_bkp => Current.user.backup_email.length > 0,
        # :notify_admin_pri => false,
        # :notify_admin_bkp => Current.user.backup_email.length > 0
      # }
        # This determines what checkboxes will be enabled on the notification
        # preferences page.
        # Eventually, this will be able to be controlled via a configuration
        # file.
      
      # Check notify_pwchange_pri
      user_preference = validate_notification_preference(user_preference, user_preference.notify_pwchange_pri, user_preference.notify_pwchange_pri_before_type_cast, current_user_prefs, :notify_pwchange_pri, :notify_pwchange)
      # Check notify_pwchange_bkp
      user_preference = validate_notification_preference(user_preference, user_preference.notify_pwchange_bkp, user_preference.notify_pwchange_bkp_before_type_cast, current_user_prefs, :notify_pwchange_bkp, :notify_pwchange)
      # Check notify_pwreset_pri
      user_preference = validate_notification_preference(user_preference, user_preference.notify_pwreset_pri, user_preference.notify_pwreset_pri_before_type_cast, current_user_prefs, :notify_pwreset_pri, :notify_pwreset)
      # Check notify_pwreset_bkp
      user_preference = validate_notification_preference(user_preference, user_preference.notify_pwreset_bkp, user_preference.notify_pwreset_bkp_before_type_cast, current_user_prefs, :notify_pwreset_bkp, :notify_pwreset)
      # Check notify_sso_pri
      user_preference = validate_notification_preference(user_preference, user_preference.notify_sso_pri, user_preference.notify_sso_pri_before_type_cast, current_user_prefs, :notify_sso_pri, :notify_sso)
      # Check notify_sso_bkp
      user_preference = validate_notification_preference(user_preference, user_preference.notify_sso_bkp, user_preference.notify_sso_bkp_before_type_cast, current_user_prefs, :notify_sso_bkp, :notify_sso)
      # Check notify_2fa_pri
      user_preference = validate_notification_preference(user_preference, user_preference.notify_2fa_pri, user_preference.notify_2fa_pri_before_type_cast, current_user_prefs, :notify_2fa_pri, :notify_2fa)
      # Check notify_2fa_bkp
      user_preference = validate_notification_preference(user_preference, user_preference.notify_2fa_bkp, user_preference.notify_2fa_bkp_before_type_cast, current_user_prefs, :notify_2fa_bkp, :notify_2fa)
      # Check notify_admin_pri
      user_preference = validate_notification_preference(user_preference, user_preference.notify_admin_pri, user_preference.notify_admin_pri_before_type_cast, current_user_prefs, :notify_admin_pri, :notify_admin)
      # Check notify_admin_bkp
      user_preference = validate_notification_preference(user_preference, user_preference.notify_admin_bkp, user_preference.notify_admin_bkp_before_type_cast, current_user_prefs, :notify_admin_bkp, :notify_admin)
    end
  end
  
  private
  
  def validate_notification_preference(user_preference, pref, pref_before_cast, existing_prefs, attr_name, i18n_attr)
    i18n_base = ".activerecord.errors.models.user_preference.attributes"
    valid_checkboxes = [true, false]
    checkbox_values = {'1': true, '0': false, 'true': true, 'false': false}
    
    if !@UserSettablePreferences[attr_name] && !pref.nil?
      # User is not allowed to change this, but a value has been submitted
      if pref_before_cast != existing_prefs[attr_name]
        # Value set does not match existing preference (user is attempting to
        # change it)
        Rails.logger.warn "User with ID #{Current.user.id} appears to have tampered with the notification settings form. Option #{attr_name} was submitted with the value \"#{pref}\" when the field was disabled."
        if attr_name[-3, attr_name.length] == 'bkp'
          # Setting is for backup email
          if Current.user.backup_email.nil?
            # User does not have backup email
            user_preference.errors.add(I18n.t("#{i18n_base}.#{i18n_attr}.name"), I18n.t("#{i18n_base}.#{i18n_attr}.noemail_bkp"))
          else
            # User does have backup email
            user_preference.errors.add(I18n.t("#{i18n_base}.#{i18n_attr}.name"), I18n.t("#{i18n_base}.#{i18n_attr}.unsettable_bkp"))
          end
        else
          # Setting is for primary email
          user_preference.errors.add(I18n.t("#{i18n_base}.#{i18n_attr}.name"), I18n.t("#{i18n_base}.#{i18n_attr}.unsettable_pri"))
        end
        # Else value matches existing preference (expected behavior to prevent
        # nil value exceptions), do nothing.
      end
    elsif !pref.nil?
      # User is allowed to change this setting
      if checkbox_values.keys.index(pref_before_cast.to_s.to_sym).nil?
        # User did not provide a valid value
        Rails.logger.warn "User with ID #{Current.user.id} appears to have tampered with the notification settings form. Option #{attr_name} was submitted with the invalid value \"#{pref_before_cast}\"."
        if attr_name[-3, attr_name.length] == 'pri'
          # Setting is for primary email address
          user_preference.errors.add(I18n.t("#{i18n_base}.#{i18n_attr}.name"), I18n.t("#{i18n_base}.#{i18n_attr}.invalid_pri"))
        else
          # Setting is for backup email address
          user_preference.errors.add(I18n.t("#{i18n_base}.#{i18n_attr}.name"), I18n.t("#{i18n_base}.#{i18n_attr}.invalid_bkp"))
        end
      else
        # User did provide a valid value
        user_preference[attr_name] = checkbox_values[pref_before_cast.to_s.to_sym]
      end
    end
    return user_preference
  end
end

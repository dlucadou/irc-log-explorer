class ReportValidator < ActiveModel::Validator
  include CommonLogValidations
  
  def validate(report)
    if report.reporter.nil? || (!report.user_id.nil? && User.find_by_id(report.reporter).nil?)
      # If user_id is not nil, then the associated user account must still
      # be active. However, if the user deleted their account, reporter would
      # be not nil, but User.find_by_id would return nil.
      # I have set up the Report model so deleting an account does not delete
      # the reports it made, but user_id will be nil when the user deletes
      # their account.
      report.errors.add(I18n.t('.activerecord.errors.models.report.attributes.reporter.name'), I18n.t('.activerecord.errors.models.report.attributes.reporter.invalid'))
    end
    if report.report_reason.length < 1 || report.report_reason.length > Report.max_report_reason_length
      report.errors.add(I18n.t('.activerecord.errors.models.report.attributes.report_reason.name'), I18n.t('.activerecord.errors.models.report.attributes.report_reason.invalid'))
      # Eventually, I'll make minimum report length settable via a configuration file
    end
    if report.resolving_action && (report.resolving_action.length > Report.max_resolving_action_length || report.resolving_action.length < 1)
      # This is only present when a report is resolved. Because the validator
      # can't find out if the user was resolving a report or making a new
      # report, it will be up to the controller to check for resolving_action
      # being nil (or not nil) and enforcing the appropriate behavior.
      report.errors.add(I18n.t('.activerecord.errors.models.report.attributes.resolving_action.name'), I18n.t('.activerecord.errors.models.report.attributes.resolving_action.invalid'))
      # Eventually, I'll make minimum resolving action length settable via a
      # configuration file
    end
    if !report.resolver.nil? && User.find_by_id(report.resolver).nil?
      report.errors.add(I18n.t('.activerecord.errors.models.report.attributes.resolver.name'), I18n.t('.activerecord.errors.models.report.attributes.resolver.invalid'))
    end
    
    # Validation of msg_channel, msg_sender, msg_text, and msg_date are
    # handled in CommonLogValidations.
  end
end

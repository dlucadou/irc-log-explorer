class ConversationValidator < ActiveModel::Validator
  def validate(conversation)
    validate_subject(conversation)
    validate_user_id(conversation)
    validate_target_id(conversation)
    validate_creator_id(conversation)
  end

  def validate_subject(conversation)
    if !conversation.subject ||
        conversation.subject.length < Rails.configuration.application[:conversations][:subject][:min_length] ||
        conversation.subject.length > Rails.configuration.application[:conversations][:subject][:max_length]
      conversation.errors.add(I18n.t('activerecord.errors.models.conversation.attributes.subject.name'),
        I18n.t('activerecord.errors.models.conversation.attributes.subject.invalid_length',
               min: Rails.configuration.application[:conversations][:subject][:min_length],
               max: Rails.configuration.application[:conversations][:subject][:max_length]))
    end
  end

  def validate_user_id(conversation)
    if conversation.user_id == Rails.configuration.application[:common][:system_user_id]
      conversation.errors.add(I18n.t('activerecord.errors.models.conversation.attributes.target_user.name'),
        I18n.t('activerecord.errors.models.conversation.attributes.target_user.invalid'))
    elsif conversation.user_id
      begin
        User.find(conversation.user_id)
      rescue ActiveRecord::RecordNotFound
        # User should exist since this is a non-null column
        conversation.errors.add(I18n.t('activerecord.errors.models.conversation.attributes.target_user.name'),
                                I18n.t('activerecord.errors.models.conversation.attributes.target_user.not_found'))
      end
    end
  end

  def validate_target_id(conversation)
    if conversation.target_id == Rails.configuration.application[:common][:system_user_id]
      conversation.errors.add(I18n.t('activerecord.errors.models.conversation.attributes.target_user.name'),
        I18n.t('activerecord.errors.models.conversation.attributes.target_user.invalid'))
    end
  end

  def validate_creator_id(conversation)
    if conversation.creator_id == Rails.configuration.application[:common][:system_user_id]
      conversation.errors.add(I18n.t('activerecord.errors.models.conversation.attributes.creator.name'),
        I18n.t('activerecord.errors.models.conversation.attributes.creator.invalid'))
    elsif conversation.creator_id
        begin
          User.find(conversation.creator_id)
        rescue ActiveRecord::RecordNotFound
          # User should exist since this is a non-null column
          conversation.errors.add(I18n.t('activerecord.errors.models.conversation.attributes.creator.name'),
                                  I18n.t('activerecord.errors.models.conversation.attributes.creator.not_found'))
        end
    end
  end
end

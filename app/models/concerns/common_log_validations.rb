require 'time'
module CommonLogValidations
  include ChatLogsHelper
  extend ActiveSupport::Concern
  # For validations that are shared between ChatLogs and Reports
  def validate(record)
    # Since ChatLogs and Reports use different attribute names for the same
    # data, I use a hash to reference the correct attribute names for each.
    # I can extend this to other models, if needed.
    if record.is_a? ChatLog
      attributes = {:id => record.id, :channel => record.channel,
                    :sender => record.sender, :message => record.message,
                    :date => record.date}
      attribute_names = {id: :id, channel: :channel,
                        sender: :sender, message: :message,
                        date: :date}
    elsif record.is_a? Report
      attributes = {:id => record.msg_id, :channel => record.msg_channel,
                    :sender => record.msg_sender, :message => record.msg_text,
                    :date => record.msg_date}
      attribute_names = {id: :msg_id, channel: :msg_channel,
                        sender: :msg_sender, message: :msg_text,
                        date: :msg_date}
    else
      raise ArgumentError, I18n.t('.activerecord.errors.models.chat_log.attributes.default.type')
    end
    
    if (attributes[:channel] =~ chatlog_channel_regex) != 0
      # Eventually, this regex be able to be set via a configuration file
      min_length = Rails.configuration.application[:chat_logs][:channel][:min_length]
      max_length = Rails.configuration.application[:chat_logs][:channel][:max_length]
      if Rails.configuration.application[:chat_logs][:channel][:require_prefix]
        record.errors.add(I18n.t('.activerecord.errors.models.chat_log.attributes.channel.name'), I18n.t('.activerecord.errors.models.chat_log.attributes.channel.invalid_require_prefix', min: min_length, max: max_length))
      else
        record.errors.add(I18n.t('.activerecord.errors.models.chat_log.attributes.channel.name'), I18n.t('.activerecord.errors.models.chat_log.attributes.channel.invalid_optional_prefix', min: min_length, max: max_length))
      end
    end
    if (attributes[:sender] =~ chatlog_sender_regex) != 0
      # Eventually, this regex be able to be set via a configuration file
      min_length = Rails.configuration.application[:chat_logs][:sender][:min_length]
      max_length = Rails.configuration.application[:chat_logs][:sender][:max_length]
      record.errors.add(I18n.t('.activerecord.errors.models.chat_log.attributes.sender.name'), I18n.t('.activerecord.errors.models.chat_log.attributes.sender.invalid', min: min_length, max: max_length))
    end
    if attributes[:message].length < 1 || attributes[:message].length > 500
      # Eventually, message length will be adjustable via a configuration file
      min_length = Rails.configuration.application[:chat_logs][:message][:min_length]
      max_length = Rails.configuration.application[:chat_logs][:message][:max_length]
      record.errors.add(I18n.t('.activerecord.errors.models.chat_log.attributes.message.name'), I18n.t('.activerecord.errors.models.chat_log.attributes.message.length', min: min_length, max: max_length))
    end
    if !attributes[:date] || (attributes[:date] >= Time.now.to_i * 1000 || attributes[:date] <= 1)
      # Casting a non-number to int will be 0, so <= 1 will ensure people
      # don't stick non-numerical values into the field.
      # Time.now.to_i is in seconds, * 1000 converts it to milliseconds
      record.errors.add(I18n.t('.activerecord.errors.models.chat_log.attributes.date.name'), I18n.t('.activerecord.errors.models.chat_log.attributes.date.invalid'))
    end
  end
end

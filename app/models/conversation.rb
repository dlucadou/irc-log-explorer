class Conversation < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :target, foreign_key: :target_id, class_name: 'User', optional: true
  belongs_to :creator, foreign_key: :created_by, class_name: 'User', optional: true
  has_many :messages, dependent: :destroy
  accepts_nested_attributes_for :messages
  validates_with ConversationValidator
  
  def admin_unread_count
    # Just the unread messages count for a specific conversation
    if self.read_by_admins
      messages.where("created_at >= ?", self.read_by_admins).count
    else # read_by_admins is nil, therefore all visible messages are unread
      messages.count
    end
  end

  def num_responses
    # Returns the number of Messages that reply to the original Message.
    # This is just the number of Messages in the Conversation, minus 1
    # (to exclude the original Message).
    # Does not filter out Messages that are deleted or hidden from the user
    # since this method is intended to only be called from Admin views.
    self.messages.count - 1
  end

  def user_unread_count
    # Just the unread messages count for a specific conversation that are visible to the user
    if self.read_by_user
      messages.where("user_visible = true AND created_at >= ?", self.read_by_user).count
    else # read_by_user is nil, therefore all visible messages are unread
      messages.where(user_visible: true).count
    end
  end

  def self.admin_unread_count
    # The count of all messages with >= 1 unread (not the total unread messages)

    # This code works, but since it filters within Rails (instead of Postgres), it will not scale -
    # not only does it grab an insane amount of records, it introduces O(n) queries to get the last
    # Message in each Conversation.
    # I'm keeping it around for reference purposes (since it's much easier to read than the SQL).
    # Inspired by: https://stackoverflow.com/questions/8977613/query-with-a-model-method
    #unreads = 0
    #Conversation.where(locked: false).each { |convo| unreads += 1 if convo.messages.last.created_at < convo.read_by_admins }
    #return unreads
    results = ActiveRecord::Base.connection.exec_query(unread_count_calculation_sql(is_admin: true))
    return results.count
  end

  def self.conversation_count(user_id: Current.user.id)
    # Returns the number of Conversations targetting the specified user_id
    Conversation.where(target_id: user_id).count
  end

  def self.max_subject_length
    Rails.configuration.application[:common][:string_type_max_length]
  end

  def self.recent_conversations(user_id: Current.user.id, num_conversations: 5)
    # Returns the num_conversations most recent Conversations targetting the specified user_id
    Conversation.where(target_id: user_id).order(created_at: :desc).limit(num_conversations)
  end

  def self.search(status: nil, subject: nil, msg_target: nil, start_time: nil, end_time: nil,
                  has_admin_unreads: nil, has_user_unreads: nil, op_msg_txt: nil, any_msg_txt: nil,
                  limit: nil, sort: nil, is_admin: false)
    with_status(status)
      .with_subject(subject)
      .with_target_user(msg_target)
      .with_start_time(start_time)
      .with_end_time(end_time)
      .with_admin_unreads(has_admin_unreads)
      .with_user_unreads(has_user_unreads, msg_target)
      .with_op_msg(op_msg_txt)
      .with_any_msg(any_msg_txt, is_admin)
      .with_sort(sort)
      .with_limit(limit)
  end

  def self.user_unread_count(user_id)
    # The count of all messages with >= 1 unread (not the total unread messages)

    # This code works, but since it filters within Rails (instead of Postgres), it will not scale -
    # not only does it grab an insane amount of records, it introduces O(n) queries to get the last
    # Message in each Conversation.
    # I'm keeping it around for reference purposes (since it's much easier to read than the SQL).
    # Inspired by: https://stackoverflow.com/questions/8977613/query-with-a-model-method
    #unreads = 0
    #Conversation.where(target_id: user_id, locked: false).each { |convo| unreads += 1 if convo.messages.last.created_at < convo.read_by_user }
    #return unreads
    results = ActiveRecord::Base.connection.exec_query(unread_count_calculation_sql(user_id: user_id))
    return results.count
  end

  def self.validate_search(status: nil, subject: nil, msg_target: nil,
                           start_time: nil, end_time: nil, has_admin_unreads: nil,
                           has_user_unreads: nil, op_msg_txt: nil, any_msg_txt: nil,
                           limit: nil, sort: nil, is_admin: false, per_page: nil)
    errors = {}

    # Validate status
    if !status.nil? && ![true, false].include?(status) # .present? returns false for "false", so I have to use !.nil?
      errors[I18n.t('.activerecord.errors.models.conversation.attributes.locked.name')] =
        I18n.t('.activerecord.errors.models.conversation.attributes.default.invalid')
    end

    # Validate subject
    if subject.present? && ApplicationHelper.is_regex?(subject) && !ApplicationHelper.pg_valid_regex?(subject)
      errors[I18n.t('.activerecord.errors.models.conversation.attributes.subject.name')] =
        I18n.t('.activerecord.errors.models.conversation.attributes.subject.invalid_regex')
    end

    # Validate msg_target
    if msg_target.present? && msg_target.to_i.to_s != msg_target.to_s
      # Non-int ID; check for email address
      targets = User.where(email: msg_target)
      if targets.count > 1
        errors[I18n.t('.activerecord.errors.models.conversation.attributes.target_user.name')] =
          I18n.t('.activerecord.errors.models.conversation.attributes.target_user.invalid')
      elsif targets.count < 1
        errors[I18n.t('.activerecord.errors.models.conversation.attributes.target_user.name')] =
          I18n.t('.activerecord.errors.models.conversation.attributes.target_user.not_found')
      end
    elsif msg_target.present? && Conversation.where(target_id: msg_target).none?
      errors[I18n.t('.activerecord.errors.models.conversation.attributes.target_user.name')] =
        I18n.t('.activerecord.errors.models.conversation.attributes.target_user.not_found')
    end

    # Validate start_time
    if start_time.present? && !(start_time.is_a?(ActiveSupport::TimeWithZone) || start_time.is_a?(DateTime) || start_time.is_a?(Time))
      # start_time is not a Time-related object; make sure it is a valid string
      begin
        start_time = ApplicationHelper.datetime_from_str(start_time)
        valid_start_time = true
      rescue ArgumentError
        errors[I18n.t('.activerecord.errors.models.conversation.attributes.created_at.name_start')] =
          I18n.t('.activerecord.errors.models.conversation.attributes.created_at.invalid_format')
      end
    elsif start_time.present?
      valid_start_time = true
    end

    # Validate end_time
    if end_time.present? && !(end_time.is_a?(ActiveSupport::TimeWithZone) || end_time.is_a?(DateTime) || end_time.is_a?(Time))
      # end_time is not a Time-related object; make sure it is a valid string
      begin
        end_time = ApplicationHelper.datetime_from_str(end_time)
        if valid_start_time
          if start_time >= end_time
            errors[I18n.t('.activerecord.errors.models.conversation.attributes.created_at.name_end')] =
              I18n.t('.activerecord.errors.models.conversation.attributes.created_at.error_end_before_start')
          end
        end
      rescue ArgumentError
        errors[I18n.t('.activerecord.errors.models.conversation.attributes.created_at.name_end')] =
          I18n.t('.activerecord.errors.models.conversation.attributes.created_at.invalid_format')
      end
    elsif end_time.present? && valid_start_time # Make sure start_time < end_time
      if start_time >= end_time
        errors[I18n.t('.activerecord.errors.models.conversation.attributes.created_at.name_end')] =
          I18n.t('.activerecord.errors.models.conversation.attributes.created_at.error_end_before_start')
      end
    end

    # Validate has_admin_unreads
    if !has_admin_unreads.nil? && ![true, false].include?(has_admin_unreads) # .present? returns false for "false", so I have to use !.nil?
      errors[I18n.t('.activerecord.errors.models.conversation.attributes.read_by_admins.name')] =
        I18n.t('.activerecord.errors.models.conversation.attributes.default.invalid')
    end

    # Validate has_user_unreads
    if !has_user_unreads.nil? && ![true, false].include?(has_user_unreads) # .present? returns false for "false", so I have to use !.nil?
      errors[I18n.t('.activerecord.errors.models.conversation.attributes.read_by_user.name')] =
        I18n.t('.activerecord.errors.models.conversation.attributes.default.invalid')
    end

    # Validate op_msg_txt
    if op_msg_txt.present? && ApplicationHelper.is_regex?(op_msg_txt) && !ApplicationHelper.pg_valid_regex?(op_msg_txt)
      errors[I18n.t('.activerecord.errors.models.conversation.attributes.op_msg_txt.name')] =
        I18n.t('.activerecord.errors.models.conversation.attributes.op_msg_txt.invalid_regex')
    end

    # Validate any_msg_txt
    if any_msg_txt.present? && ApplicationHelper.is_regex?(any_msg_txt) && !ApplicationHelper.pg_valid_regex?(any_msg_txt)
      errors[I18n.t('.activerecord.errors.models.conversation.attributes.any_msg_txt.name')] =
        I18n.t('.activerecord.errors.models.conversation.attributes.any_msg_txt.invalid_regex')
    elsif any_msg_txt.present? && !is_admin.nil? && ![true, false].include?(is_admin) # Valid any_msg_txt, invalid is_admin
      errors[I18n.t('.activerecord.errors.models.conversation.any_msg_txt.name')] =
        I18n.t('.activerecord.errors.models.conversation.attributes.any_msg_txt.unsearchable_is_admin_invalid')
    elsif any_msg_txt.present? && is_admin.nil? # Valid any_msg_txt, no is_admin
      errors[I18n.t('.activerecord.errors.models.conversation.any_msg_txt.name')] =
        I18n.t('.activerecord.errors.models.conversation.attributes.any_msg_txt.unsearchable_is_admin_missing')
      # No error case for any_msg_txt being nil, as that means this field is not being searched
    end

    # Validate per_page
    if per_page.present? && (per_page.to_i.to_s != per_page.to_s || !PreferencesHelper.valid_per_page(per_page))
      # Either per_page is not an int OR is is not a valid number
      errors[I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.name')] =
        I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.invalid', min: PreferencesHelper.min_per_page, max: PreferencesHelper.max_per_page)
    end

    return errors
  end

  private

  def self.unread_count_calculation_sql(is_admin: false, user_id: nil, include_semicolon: true)
    # Note: user_id MUST be an integer (or nil)!
    # Warning: wall of SQL incoming!
    # I could not figure out how to do this without raw SQL, unfortunately.
    # This is likely an inefficient query, but I don't really know how to improve it.
    # If you know how to improve this SQL, feel free to put in a merge request!
    if is_admin
      role = 'admins'
      user_visible = ""
      user_targeting = ""
    else
      role = 'user'
      user_visible = "WHERE messages.user_visible = true"
      if user_id.is_a? Integer # This check will prevent any weird calls from potentially doing an SQL injection
        user_targeting = "AND conversations.target_id = #{user_id}"
      else
        user_targeting = "AND conversations.target_id = -1"
        logger.warn "Conversation.unread_count_calculation_sql was called with is_admin=false but invalid user_id! Using -1, which will cause it to match 0 records."
        logger.debug "Value of user_id=#{user_id} (class=#{user_id.class})"
      end
    end

    # For use in the with_admin_unreads and with_user_unreads search scope
    if include_semicolon
      semicolon = ';'
    else # Semicolon is not needed in subqueries
      semicolon = ''
    end

    # This SQL is used to calculate the number of Conversations with 1 or more
    # unread Messages either for a specific User (if viewed via user dashboard)
    # or all Users (if viewed via admin dashboard)
    return "SELECT conversations.id" \
      "    FROM conversations" \
      "    INNER JOIN (" \
      "        SELECT DISTINCT ON (conversations.id) messages.conversation_id, messages.created_at" \
      "        FROM conversations" \
      "        INNER JOIN messages" \
      "        ON messages.conversation_id = conversations.id #{user_visible}" \
      "        ORDER BY conversations.id, messages.created_at DESC" \
      "    ) AS vmessage" \
      "    ON conversations.id = vmessage.conversation_id" \
      "    WHERE (conversations.read_by_#{role} <= vmessage.created_at" \
      "      OR conversations.read_by_#{role} IS NULL) #{user_targeting} #{semicolon}"
  end

  def self.message_contains_sql(first_msg: true, is_regex: false, is_admin: false, include_semicolon: false)
    if !is_admin # Non-admin view; do not search hidden messages
      user_visible = "WHERE messages.user_visible = true"
      user_targeting = "AND conversations.target_id = #{Current.user.id}"
    else # Admin view; search hidden messages
      user_visible = ""
      user_targeting = ""
    end

    if is_regex
      comparator = "~*"
    else
      comparator = "ILIKE"
    end

    if include_semicolon
      semicolon = ';'
    else
      semicolon = ''
    end

    if first_msg
      # This SQL selects the first Message in a Conversation
      msg_selector = "SELECT DISTINCT ON (conversations.id) messages.conversation_id," \
        "    messages.created_at, messages.body_plaintext" \
        "    FROM conversations" \
        "    INNER JOIN messages" \
        "    ON messages.conversation_id = conversations.id #{user_visible}" \
        "    ORDER BY conversations.id, messages.created_at ASC"
    else
      # This SQL selects all Messages in a Conversation
      msg_selector = "SELECT conversations.id, messages.conversation_id, messages.created_at," \
        "    messages.body_plaintext" \
        "    FROM conversations" \
        "    INNER JOIN messages" \
        "    ON messages.conversation_id = conversations.id #{user_visible}" \
        "    ORDER BY conversations.id, messages.created_at ASC"
    end

    # This SQL will match all Messages for some string, but the caller has to
    # insert the text to search for (substituting into the '?').
    # Unlike the unread_count_calculation_sql method, this does not target by
    # any user IDs - this is because if a non-admin User is searching, their
    # ID should automatically be specified in the :with_target_user scope.
    # TODO: evaluate if it would make it more efficient to take a user ID and
    # filter within this SQL
    return "SELECT conversations.id /*, vmessages.conversation_id, vmessages.body_plaintext*/" \
      "    FROM conversations" \
      "    INNER JOIN (#{msg_selector})" \
      "    AS vmessages" \
      "    ON conversations.id = vmessages.conversation_id" \
      "    WHERE vmessages.body_plaintext #{comparator} ? #{semicolon} #{user_targeting}"
  end

  scope :with_status, proc { |status|
    if !status.nil?
      where(locked: status)
    end
  }

  scope :with_subject, proc { |subject|
    # Case insensitive; regex-enabled
    if subject.present?
      if ApplicationHelper.is_regex?(subject) # Using regex
        where("subject ~* ?", ApplicationHelper.isolate_regex(subject))
      else # Not using regex
        if ApplicationHelper.is_escaped_regex?(subject)
          subject = ApplicationHelper.unescape_regex(subject)
        end
        where("subject ILIKE ?", "%#{subject}%") # Case insensitive search
      end
    end
  }

  scope :with_target_user, proc { |msg_target|
    if msg_target.present? # Get numerical ID if not already numerical ID
      if msg_target.to_i.to_s == msg_target.to_s
        where(target_id: msg_target)
      else
        targets = User.where(email: msg_target)
        where(target_id: targets.first.id)
        # This assumes that .validate_search was called, which will alert when
        # there is !=1 user
      end
    end
  }

  scope :with_start_time, proc { |start_time|
    if start_time.present?
      where("created_at >= ?", start_time)
    end
  }

  scope :with_end_time, proc { |end_time|
    if end_time.present?
      where("created_at <= ?", end_time)
    end
  }

  scope :with_admin_unreads, proc { |has_admin_unreads|
    if !has_admin_unreads.nil?
      if has_admin_unreads
        where("id IN (#{unread_count_calculation_sql(is_admin: true, include_semicolon: false)})")
        # Using string interpolation is necessary here, as using ? insertion escapes the SQL
      else
        where("id NOT IN (#{unread_count_calculation_sql(is_admin: true, include_semicolon: false)})")
      end
    end
  }

  scope :with_user_unreads, proc { |has_user_unreads, msg_target: Current.user.id|
    # Note: msg_target MUST be present

    # This assumes msg_target is an int already - it will not locate the user by email address.
    # The reason for this is because this is intended to be searched for from the user's view,
    # so they cannot actually control msg_target - it must be auto-filled by the controller
    # before executing the search.
    if !has_user_unreads.nil? && !msg_target.nil?
      if has_user_unreads
        where("id IN (#{unread_count_calculation_sql(user_id: msg_target, include_semicolon: false)})")
        # Using string interpolation is necessary here, as using ? insertion escapes the SQL
      else
        where("id NOT IN (#{unread_count_calculation_sql(user_id: msg_target, include_semicolon: false)})")
      end
    end
  }

  scope :with_op_msg, proc { |op_msg_txt|
    # Determine if regex or not
    if op_msg_txt.present?
      if ApplicationHelper.is_regex?(op_msg_txt) # Using regex
        where("id IN (#{message_contains_sql(first_msg: true, is_regex: true, is_admin: false)})",
              ApplicationHelper.isolate_regex(op_msg_txt))
        # Using ", op_msg_txt" because there is a '?' in the returned string from message_contains_sql
        # that I can use for sanitized insertion.
        # is_admin is false because the first Message in a Conversation cannot be hidden, so there is
        # no need for it to be true.
      else # Not using regex
        # Check for escaped forward slashes
        if ApplicationHelper.is_escaped_regex?(op_msg_txt)
          op_msg_txt = ApplicationHelper.unescape_regex(op_msg_txt)
        end
        where("id IN (#{message_contains_sql(first_msg: true, is_regex: false, is_admin: false)})",
              "%#{op_msg_txt}%")
        # Using ", op_msg_txt" because there is a '?' in the returned string from message_contains_sql
        # that I can use for sanitized insertion
        # is_admin is false because the first Message in a Conversation cannot be hidden, so there is
        # no need for it to be true.
      end
    end
  }

  scope :with_any_msg, proc { |any_msg_txt, is_admin|
    # Note: is_admin MUST be present and must be true or false

    # Determine if regex or not
    if any_msg_txt.present? && !is_admin.nil?
      if ApplicationHelper.is_regex?(any_msg_txt) # Using regex
        where("id IN (#{message_contains_sql(first_msg: false, is_regex: true, is_admin: is_admin)})",
              ApplicationHelper.isolate_regex?(any_msg_txt))
        # Using ", any_msg_txt" because there is a '?' in the returned string from message_contains_sql
        # that I can use for sanitized insertion
      else # Not using regex
        # Check for escaped forward slashes
        if ApplicationHelper.is_escaped_regex?(any_msg_txt)
          any_msg_txt = ApplicationHelper.unescape_regex(any_msg_txt)
        end
        where("id IN (#{message_contains_sql(first_msg: false, is_regex: false, is_admin: is_admin)})",
              "%#{any_msg_txt}%")
        # Using ", any_msg_txt" because there is a '?' in the returned string from message_contains_sql
        # that I can use for sanitized insertion
      end
    end
  }

  scope :with_sort, proc { |sort|
    if sort.present?
      order(sort)
    end
  }

  scope :with_limit, proc { |limit|
    if limit.present?
      limit(limit)
    end
  }
end

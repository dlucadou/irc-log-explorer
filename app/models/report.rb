class Report < ApplicationRecord
  validates_with ReportValidator
  belongs_to :user, foreign_key: :user_id, optional: true
  # optional: true required in Rails 6, per https://stackoverflow.com/a/37803756

  def self.admin_unread_count
    Report.where(:resolved_at => nil).count
  end

  def self.max_report_reason_length
    Rails.configuration.application[:common][:string_type_max_length]
  end

  def self.max_resolving_action_length
    Rails.configuration.application[:common][:string_type_max_length]
  end

  def self.recent_reports(user_id: Current.user.id, num_reports: 5)
    Report.where(reporter: user_id).order(created_at: :desc).limit(num_reports)
  end

  def self.report_count(user_id: Current.user.id, resolved: nil)
    if resolved.nil?
      Report.where(reporter: user_id).count
    else
      Report.where(reporter: user_id, resolved: resolved).count
    end
  end

  def self.report_resolved_count(user_id: Current.user.id)
    Report.where(resolver: user_id).count
  end

  # Search helper methods

  def self.search(reporter: nil, resolved: nil, resolver: nil, reason: nil,
                  channel: nil, message: nil, sender: nil, msg_id: nil,
                  start_time: nil, end_time: nil)
    with_reporter(reporter)
      .with_resolved(resolved)
      .with_resolver(resolver)
      .with_reason(reason)
      .with_msg_channel(channel)
      .with_msg_text(message)
      .with_msg_sender(sender)
      .with_msg_id(msg_id)
      .with_start_time(start_time)
      .with_end_time(end_time)
  end

  def self.validate_search(reporter: nil, resolved: nil, resolver: nil,
                           reason: nil, channel: nil, message: nil, sender: nil,
                           msg_id: nil, start_time: nil, end_time: nil,
                           per_page: nil)
    errors = {}

    # Validate reporter
    if reporter.present? && reporter.to_i.to_s == reporter.to_s && !User.find_by_id(reporter) # reporter is int but not valid user ID
      errors[I18n.t('.activerecord.errors.models.report.attributes.reporter.name_id')] =
        I18n.t('.activerecord.errors.models.report.attributes.reporter.not_found')
    elsif reporter.present? && reporter.to_i.to_s != reporter.to_s # reporter is not an int
      users = User.where(email: reporter)
      if users.count > 1 # reporter matches multiple users
        errors[I18n.t('.activerecord.errors.models.report.attributes.reporter.name_id')] =
          I18n.t('.activerecord.errors.models.report.attributes.reporter.multiple_found')
        logger.error "User with ID #{current_user.id} searched for Reports by user with email #{reporter} and #{users.count} results were returned."
      elsif users.count == 0 # reporter matches no users
        errors[I18n.t('.activerecord.errors.models.report.attributes.reporter.name_id')] =
          I18n.t('.activerecord.errors.models.report.attributes.reporter.not_found')
      end
    end

    # Validate resolved
    if !resolved.nil? && ReportsHelper.report_statuses.key(resolved).nil?
      errors[I18n.t('.activerecord.errors.models.report.attributes.resolved.name')] =
        I18n.t('.activerecord.errors.models.report.attributes.resolved.invalid')
    end

    # Validate resolver
    if resolver.present? && resolver.to_i.to_s == resolver.to_s && !User.find_by_id(resolver) # resolver is int but not valid user ID
      errors[I18n.t('.activerecord.errors.models.report.attributes.resolver.name_id')] =
        I18n.t('.activerecord.errors.models.report.attributes.resolver.not_found')
    elsif resolver.present? && resolver.to_i.to_s != resolver.to_s # resolver is not an int
      users = User.where(email: resolver)
      if users.count > 1 # resolver matches multiple users
        errors[I18n.t('.activerecord.errors.models.report.attributes.resolver.name_id')] =
          I18n.t('.activerecord.errors.models.report.attributes.resolver.multiple_found')
        logger.error "User with ID #{current_user.id} searched for Reports by user with email #{resolver} and #{users.count} results were returned."
      elsif users.count == 0 # resolver matches no users
        errors[I18n.t('.activerecord.errors.models.report.attributes.resolver.name_id')] =
          I18n.t('.activerecord.errors.models.report.attributes.resolver.not_found')
      end
    end

    # Validate reason
    if reason.present? && reason.length > 0 && ApplicationHelper.is_regex?(reason) && !ApplicationHelper.pg_valid_regex?(reason)
      errors[I18n.t('.activerecord.errors.models.report.attributes.report_reason.name')] =
        I18n.t('.activerecord.errors.models.report.attributes.report_reason.invalid_regex')
    end

    # Validate channel
    if channel.present? && !channel.match(ChatLogsHelper.chatlog_channel_regex)
      errors[I18n.t('.activerecord.errors.models.report.attributes.msg_channel.name')] =
        I18n.t('.activerecord.errors.models.report.attributes.msg_channel.invalid',
               fmat: ChatLogsHelper.chatlog_channel_regex_description)
    end

    # Validate message
    if message.present? && message.length > 0 && ApplicationHelper.is_regex?(message) && !ApplicationHelper.pg_valid_regex?(message)
      errors[I18n.t('.activerecord.errors.models.report.attributes.msg_text.name')] =
        I18n.t('.activerecord.errors.models.report.attributes.msg_text.invalid_regex')
    end

    # Validate sender
    if sender.present? && !sender.match(ChatLogsHelper.chatlog_sender_regex)
      errors[I18n.t('.activerecord.errors.models.report.attributes.msg_sender.name')] =
        I18n.t('.activerecord.errors.models.report.attributes.msg_sender.invalid',
               fmat: ChatLogsHelper.chatlog_sender_regex_description)
    end

    # Validate msg_id
    if msg_id.present? && msg_id.length > 0
      if (msg_id.to_i < 1 || msg_id.to_i.to_s != msg_id) && msg_id.to_i <= ChatLogsHelper.chatlog_max_id
        # Either the ID is actually 0 (and message ID will never be 0) or
        # the ID is a non-integer value
        errors[I18n.t('.activerecord.errors.models.report.attributes.msg_id.name')] =
          I18n.t('.activerecord.errors.models.report.attributes.msg_id.invalid_not_positive')
      elsif msg_id.to_i > ChatLogsHelper.chatlog_max_id
        errors[I18n.t('.activerecord.errors.models.report.attributes.msg_id.name')] =
          I18n.t('.activerecord.errors.models.report.attributes.msg_id.invalid_max_id',
                 max: ChatLogsHelper.chatlog_max_id)
      end
    end

    # Validate start_time
    if start_time.present?
      begin
        start_dt = ApplicationHelper.datetime_from_str(start_time) # start_time valid datetime
        if start_dt > Time.now() # Valid start_time, but it comes after the current system time
          errors[I18n.t('.activerecord.errors.models.report.attributes.msg_date.name_start')] =
            I18n.t('.activerecord.errors.models.report.attributes.msg_date.invalid_system_time')
          start_dt = nil
        end
      rescue ArgumentError # Invalid start_time
        errors[I18n.t('.activerecord.errors.models.report.attributes.msg_date.name_start')] =
          I18n.t('.activerecord.errors.models.report.attributes.msg_date.invalid',
                 format: ChatLogsHelper.user_datetime_format(format: :datetime, seconds: false))
        start_dt = nil
      end
    end

    # Validate end_time
    if end_time.present?
      begin
        end_dt = ApplicationHelper.datetime_from_str(end_time) # end_time valid datetime
        if end_dt > Time.now() # Valid end_time, but it comes after the current system time
          errors[I18n.t('.activerecord.errors.models.report.attributes.msg_date.name_end')] =
            I18n.t('.activerecord.errors.models.report.attributes.msg_date.invalid_system_time')
          end_dt = nil
        end
        if start_dt && end_dt && start_dt >= end_dt # start_time comes after end_time
          errors[I18n.t('.activerecord.errors.models.report.attributes.msg_date.name_end')] =
            I18n.t('.activerecord.errors.models.report.attributes.msg_date.invalid_start_time')
        end
      rescue ArgumentError # Invalid end_time
        errors[I18n.t('.activerecord.errors.models.report.attributes.msg_date.name_start')] =
          I18n.t('.activerecord.errors.models.report.attributes.msg_date.invalid',
                 format: ChatLogsHelper.user_datetime_format(format: :datetime, seconds: false))
        end_dt = nil
      end
    end

    # Validate per_page
    if per_page && (per_page.to_i.to_s != per_page.to_s || !PreferencesHelper.valid_per_page(per_page.to_i))
      errors[I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.name')] =
        I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.invalid', min: PreferencesHelper.min_per_page, max: PreferencesHelper.max_per_page)
    end

    return errors
  end

  private

  scope :with_reporter, proc { |reporter|
    if reporter.present?
      if reporter.to_i.to_s == reporter.to_s # Reporter ID is int
        where(reporter: reporter)
      else # Reporter ID is string
        where(reporter: User.where(email: reporter).first.id)
        # Assumes there will be exactly 1 reponse, which you would find out if you called .validate_search beforehand
      end
    end
  }

  scope :with_resolved, proc { |resolved|
    # Needs to be T/F/nil (TrueClass/FalseClass or String "true"/"false")
    if !resolved.nil?
      # Using resolved.present? returns false for FalseClass, so I have to use
      # !resolved.nil? to have resolved be searched for.
      where(resolved: resolved)
    end
  }

  scope :with_resolver, proc { |resolver|
    if resolver.present?
      if resolver.to_i.to_s == resolver.to_s # Resolver ID is int
        where(resolver: resolver)
      else # Resolver ID is string
        where(resolver: User.where(email: resolver).first.id)
        # Assumes there will be exactly 1 reponse, which you would find out if you called .validate_search beforehand
      end
    end
  }

  scope :with_reason, proc { |reason|
    if reason.present?
      if ApplicationHelper.is_regex?(reason) # Using regex
        where("report_reason ~ ?", ApplicationHelper.isolate_regex(reason))
        # Assumes regex is valid, which you would find out if you called .validate_search beforehand
      else # Not using regular expressions
        # See if they escaped forward slashes at the start and/or end of the
        # string, and if so, unescape them.
        # I do this because having forward slashes at the start and end of the
        # string indicates regex, but they can escape this with backslashes.
        if ApplicationHelper.is_escaped_regex?(reason)
          reason = ApplicationHelper.unescape_regex(reason)
        end
        where("lower(report_reason) ILIKE ?", "%#{reason.downcase}%")
      end
    end
  }

  scope :with_msg_channel, proc { |channel|
    if channel.present?
      where(msg_channel: channel)
    end
  }

  scope :with_msg_text, proc { |message|
    if message.present?
      if ApplicationHelper.is_regex?(message) # Using regex
        where("msg_text ~ ?", ApplicationHelper.isolate_regex(message))
        # Assumes regex is valid, which you would find out if you called .validate_search beforehand
      else # Not using regular expressions
        # See if they escaped forward slashes at the start and/or end of the string,
        # and if so, remove them.
        # I do this because having forward slashes at the start and end of the string
        # indicates regex, but they can escape this with backslashes.
        if ApplicationHelper.is_escaped_regex?(message)
          message = ApplicationHelper.unescape_regex(message)
        end
        where("lower(msg_text) ILIKE ?", "%#{message.downcase}%")
      end
    end
  }

  scope :with_msg_sender, proc { |sender|
    if sender.present?
      where(msg_sender: sender)
    end
  }

  scope :with_msg_id, proc { |id|
    if id.present?
      where(msg_id: id)
    end
  }

  scope :with_start_time, proc { |start_time|
    if start_time.present?
      where("created_at >= ?", start_time)
    end
  }

  scope :with_end_time, proc { |end_time|
    if end_time.present?
      where("created_at <= ?", end_time)
    end
  }
end

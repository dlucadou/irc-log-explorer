class Message < ApplicationRecord
  before_save :set_body_plaintext
  belongs_to :sender, foreign_key: :sender_id, class_name: 'User', optional: true
  belongs_to :deleter, foreign_key: :deleter_id, class_name: 'User', optional: true
  belongs_to :conversation
  validates_with MessageValidator

  def admin_relative_id(index_from: 1)
    conversation.messages.index(self) + index_from
  end

  def deletable?
    self.hideable?
  end

  def deleted?
    !self.deleted_by.nil?
  end

  def edit_period_end
    self.created_at.utc + Message.edit_window
  end

  def edited?
    !self.edited_at.nil?
    # I manually set edited_at when editing the message body because using
    # updated_at is inaccurate - deleting or hiding a message would also
    # modify updated_at, so this attribute is guaranteed to be reflect edits.
  end

  def editable?
    Time.now.utc < edit_period_end && !self.deleted? && self.sent_by == Current.user.id && !self.conversation.locked
  end

  def hidden?
    !self.user_visible
  end

  def hideable?
    self.admin_relative_id != 1 && !self.hidden? && !self.deleted? && !self.conversation.locked
  end

  def user_relative_id(index_from: 1)
    conversation.messages.where(user_visible: true).order(:created_at).index(self) + index_from
  end

  def self.edit_window
    Rails.configuration.application[:messages][:body][:edit_window_minutes].minutes
  end

  def self.edit_window_str
    # The reason for /60 is because x.minutes.to_s returns seconds, i.e.
    # 10.minutes.to_s (or 10.minutes.to_i) => 600
    # So I divide by 60 to get the number of minutes.
    if Message.edit_window % 60 == 0
      "#{ActionController::Base.helpers.pluralize(Message.edit_window / 60, 'minute')}"
    else
      "#{ActionController::Base.helpers.pluralize(Message.edit_window / 60, 'minute')}, " \
        "#{ActionController::Base.helpers.pluralize(Message.edit_window % 60, 'second')}"
    end
  end

  def self.max_body_length
    Rails.configuration.application[:messages][:body][:max_length]
  end

  private

  def set_body_plaintext
    self.body_plaintext = ApplicationHelper.markdown_to_plaintext(self.body)
  end
end

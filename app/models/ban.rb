class Ban < ApplicationRecord
  validates_with BanValidator
  after_create :message_user_after_ban
  belongs_to :user, foreign_key: :user_id, optional: true
  # optional: true required in Rails 6, per https://stackoverflow.com/a/37803756

  def ban_appealable?
    if user.is_banned?
      if user.permanently_banned?
        Rails.configuration.application[:users][:bans][:appeals_allowed_perm]
      else
        Rails.configuration.application[:users][:bans][:appeals_allowed_temp] &&
          (self.ban_end_time - self.created_at >
            Rails.configuration.application[:users][:bans][:appeals_allowed_temp_hours].hours)
      end
    else
      false
    end
  end

  def banning_user_account
    # Returns the User who created the ban, if the account exists, otherwise
    # returns nil.
    if User.where(id: banning_user).count != 0
      return User.find(banning_user)
    else
      return nil
    end
  end

  def banned_user_account
    # Returns the User who was banned, if the account exists, otherwise
    # returns nil.
    if self.user_id?
      return User.find(user_id)
    else
      return nil
    end
  end
  
  def self.max_internal_reason_length
    Rails.configuration.application[:users][:bans][:max_internal_reason_length]
  end

  def self.max_external_reason_length
    Rails.configuration.application[:common][:string_type_max_length]
  end

  private

  def message_user_after_ban
    user = self.banned_user_account

    msg_subject = ''
    msg_body = ''
    if self.ban_reversal
      msg_subject += I18n.t('activerecord.errors.models.ban.attributes.system_message.subject.unbanned')
      msg_body += I18n.t('activerecord.errors.models.ban.attributes.system_message.body.length_unban')
      msg_body += "\r\n\r\n"
      # I have to use " instead of ' because '\r' becomes "\\r", while "\r" does not
      # get escaped by Ruby.
      msg_body += I18n.t('activerecord.errors.models.ban.attributes.system_message.body.reason_ext',
                         ext_reason: self.external_reason)
      msg_body += "\r\n\r\n"
      msg_body += I18n.t('activerecord.errors.models.ban.attributes.system_message.body.sig_unban')
    else
      msg_subject += I18n.t('activerecord.errors.models.ban.attributes.system_message.subject.banned')
      if self.permanent_ban
        msg_body += I18n.t('activerecord.errors.models.ban.attributes.system_message.body.length_perm')
      else
        msg_body += I18n.t('activerecord.errors.models.ban.attributes.system_message.body.length_temp')
      end
      msg_body += "\r\n\r\n"
      msg_body += I18n.t('activerecord.errors.models.ban.attributes.system_message.body.reason_ext',
                         ext_reason: self.external_reason)
      msg_body += "\r\n\r\n"
      if self.ban_appealable?
        msg_body += I18n.t('activerecord.errors.models.ban.attributes.system_message.body.sig_ban_appeal')
      else
        msg_body += I18n.t('activerecord.errors.models.ban.attributes.system_message.body.sig_ban_no_appeal')
      end
    end

    user_convo = Conversation.create(user_id: user.id,
                                     target_id: user.id,
                                     created_by: Rails.configuration.application[:common][:system_user_id],
                                     subject: msg_subject,
                                     read_by_admins: Time.now + 1.second)
    user_convo.messages.create(conversation_id: user_convo.id,
                               sent_by: Rails.configuration.application[:common][:system_user_id],
                               body: msg_body,
                               user_visible: true,
                               sent_anonymously: false)
    user_convo.locked = true
    # Locked Conversations will not allow new messages, so I have to lock *after*
    # Message creation.
    if !user_convo.save
      logger.error "Failed to save Conversation notifying #{self.banned_user} of their ban"
      logger.error "Make sure to check for Conversations with no corresponding Messages in the database"
      # I use .save instead of .save! because .save! causes silent failures that I
      # cannot directly communicate back to the controller, issuing rollbacks on
      # the ban. If a save fails, it is better for it to be silent to the user.
    end
  end
end

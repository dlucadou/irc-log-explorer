class Search < ApplicationRecord
  belongs_to :user, foreign_key: :user_id, optional: true
  # optional: true required in Rails 6, per https://stackoverflow.com/a/37803756

  def self.recent_searches(user_id: nil, num_searches: 5)
    if user_id.nil?
      Search.order(created_at: :desc).limit(num_searches)
    else
      Search.where(searcher: user_id).order(created_at: :desc).limit(num_searches)
    end
  end

  def self.num_searches(user_id: nil)
    if user_id.nil?
      Search.count
    else
      Search.where(searcher: user_id).count
    end
  end

  def self.avg_search_time(user_id: nil)
    if user_id.nil?
      Search.average(:execution_time) || 0
    else
      Search.where(searcher: user_id).average(:execution_time) || 0
        # || 0 prevents nil values from being returned if no records are
        # returned (which always happens if a user has no Searches).
    end
  end

  # Search helper methods

  def self.search(query_type: nil, query_notes: nil, query_scope: nil,
                  execution_time_min: nil, execution_time_max: nil,
                  searcher: nil, start_time: nil, end_time: nil)
    with_query_type(query_type)
      .with_query_notes(query_notes)
      .with_query_scope(query_scope)
      .with_searcher(searcher)
      .with_execution_time_min(execution_time_min)
      .with_execution_time_max(execution_time_max)
      .with_start_time(start_time)
      .with_end_time(end_time)
  end

  def self.validate_search(query_type: nil, query_notes: nil, query_scope: nil,
                           execution_time_min: nil, execution_time_max: nil,
                           searcher: nil, start_time: nil, end_time: nil,
                           per_page: nil)
    errors = {}

    # Verify query_type
    if !query_type.nil? && Admin::DashboardHelper.dash_search_types.key(query_type).nil?
      errors[I18n.t('.activerecord.errors.models.searches.attributes.query_type.name')] =
        I18n.t('.activerecord.errors.models.searches.attributes.query_type.invalid')
    end

    # Verify query_notes
    if !query_notes.nil? && Admin::DashboardHelper.dash_search_errors.key(query_notes).nil?
      errors[I18n.t('.activerecord.errors.models.searches.attributes.query_notes.name_errors')] =
        I18n.t('.activerecord.errors.models.searches.attributes.query_notes.invalid')
    end

    # Verify query_scope
    if !query_scope.nil? && Admin::DashboardHelper.dash_search_scopes.key(query_scope).nil?
      errors[I18n.t('.activerecord.errors.models.searches.attributes.query_scope.name')] =
        I18n.t('.activerecord.errors.models.searches.attributes.query_scope.invalid')
    end

    # Verify searcher
    if searcher.present? && searcher.to_i.to_s == searcher.to_s && !User.find_by_id(searcher)
      errors[I18n.t('.activerecord.errors.models.searches.attributes.searcher.name')] =
        I18n.t('.activerecord.errors.models.searches.attributes.searcher.not_found')
    elsif searcher.present?
      users = User.where(email: searcher)
      if users.count > 1
        errors[I18n.t('.activerecord.errors.models.searches.attributes.searcher.name')] =
          I18n.t('.activerecord.errors.models.searches.attributes.searcher.multiple_found')
        logger.error "User with ID #{current_user.id} searched for Searches by reporting user with email #{searcher} and #{users.count} results were returned."
      elsif users.count < 1
        errors[I18n.t('.activerecord.errors.models.searches.attributes.searcher.name')] =
          I18n.t('.activerecord.errors.models.searches.attributes.searcher.not_found')
      end
    end

    # Verify execution_time_min and execution_time_max
    if execution_time_min.present? && execution_time_min.length > 0 && execution_time_min.to_f > 0
      if execution_time_max.present? && execution_time_max.length > 0 && execution_time_max.to_f <= 0
        errors[I18n.t('.activerecord.errors.models.searches.attributes.execution_time.name_max')] =
          I18n.t('.activerecord.errors.models.searches.attributes.execution_time.invalid_gt0')
      elsif execution_time_max.present? && execution_time_max.length > 0 && execution_time_min.to_f >= execution_time_max.to_f
        errors[I18n.t('.activerecord.errors.models.searches.attributes.execution_time.name_max')] =
          I18n.t('.activerecord.errors.models.searches.attributes.execution_time.invalid_min')
      end
    elsif execution_time_min.present? && execution_time_min.length > 0 && execution_time_min.to_f <= 0
      errors[I18n.t('.activerecord.errors.models.searches.attributes.execution_time.name_min')] =
        I18n.t('.activerecord.errors.models.searches.attributes.execution_time.invalid_gt0')
      if execution_time_max.present? && execution_time_max.length > 0 && execution_time_max.to_f <= 0
        errors[I18n.t('.activerecord.errors.models.searches.attributes.execution_time.name_max')] =
          I18n.t('.activerecord.errors.models.searches.attributes.execution_time.invalid_gt0')
      end
    elsif execution_time_max.present? && execution_time_max.length > 0 && execution_time_max.to_f <= 0
      errors[I18n.t('.activerecord.errors.models.searches.attributes.execution_time.name_max')] =
        I18n.t('.activerecord.errors.models.searches.attributes.execution_time.invalid_gt0')
    end

    # Verify start_time
    if start_time.present?
      begin
        # Check for valid datetime
        start_time = ApplicationHelper.datetime_from_str(start_time)
        # Make sure start_time isn't past current system time
        if start_time > Time.now().utc
          errors[I18n.t('.activerecord.errors.models.searches.attributes.created_at.name_start')] =
            I18n.t('.activerecord.errors.models.searches.attributes.created_at.invalid_system_time')
        end
      rescue ArgumentError
        start_time = nil
        errors[I18n.t('.activerecord.errors.models.searches.attributes.created_at.name_start')] =
          I18n.t('.activerecord.errors.models.searches.attributes.created_at.invalid')
      end
    end

    # Verify end_time
    if end_time.present?
      begin
        # Check for valid datetime
        end_time = ApplicationHelper.datetime_from_str(end_time)
        # Make sure end_time isn't past current system time
        if end_time > Time.now().utc
          errors[I18n.t('.activerecord.errors.models.searches.attributes.created_at.name_end')] =
            I18n.t('.activerecord.errors.models.searches.attributes.created_at.invalid_system_time')
        end
        if start_time.present? && start_time >= end_time
          # Make sure end time is greater than start time (start < end)
          errors[I18n.t('.activerecord.errors.models.searches.attributes.created_at.name_start')] =
            I18n.t('.activerecord.errors.models.searches.attributes.created_at.invalid_end_time')
        end
      rescue ArgumentError
        errors[I18n.t('.activerecord.errors.models.searches.attributes.created_at.name_end')] =
          I18n.t('.activerecord.errors.models.searches.attributes.created_at.invalid')
      end
    end

    # Verify per_page
    if per_page.present? && !PreferencesHelper.valid_per_page(per_page)
      errors[I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.name')] =
        I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.invalid',
               min: PreferencesHelper.min_per_page, max: PreferencesHelper.max_per_page)
    end

    return errors
  end

  private

  scope :with_query_type, proc { |query_type|
    if query_type.present?
      if query_type != 'all'
        where(query_type: query_type)
      end
    end
  }

  scope :with_query_notes, proc { |query_notes|
    if query_notes.present?
      if query_notes == 'true'
        # True and false params are strings, and I can't "cast" to boolean
        # in Ruby easily, so I just compare to strings in here. Potential
        # values for query_notes include:
        # ["true", "false", "both"]
        # True - just searches that generated an error
        # False - just searches that generated no errors
        # Both - all searches, regardless of errors
        where("(query_notes->'error') is not null")
        # Credit goes to https://stackoverflow.com/a/32029506
      elsif query_notes == 'false'
        where("(query_notes->'error') is null")
      end
      # This could be extended to search for results with an error, a warning,
      # etc. like this:
      # where("(query_notes->'?') is not null", query_notes)
      # I could even do a loop to search for multiple result types.
    end
  }

  scope :with_query_scope, proc { |query_scope|
    if query_scope.present?
      if query_scope == 'admin'
        #where(query_scope: query_scope)
        where("query_type IN (?,?,?,?)", 'report_browse', 'report_query', 'search_browse', 'search_query')
      elsif query_scope == 'all_no-admin-search'
        where("query_type NOT IN (?,?)", 'search_browse', 'search_query')
      elsif query_scope == 'user'
        where("query_type NOT IN (?,?,?,?)", 'report_browse', 'report_query', 'search_browse', 'search_query')
      end
    end
  }

  scope :with_searcher, proc { |searcher|
    if searcher.present?
      if searcher.to_i.to_s == searcher.to_s # searcher ID is int
        where(searcher: searcher)
      else # searcher ID is string
        where(searcher: User.where(email: searcher).first.id)
        # Assumes there will be exactly 1 reponse, which you would find out if you called .validate_search beforehand
      end
    end
  }

  scope :with_execution_time_min, proc { |execution_time_min|
    if execution_time_min.present?
      where("execution_time >= ?", execution_time_min)
    end
  }

  scope :with_execution_time_max, proc { |execution_time_max|
    if execution_time_max.present?
      where("execution_time <= ?", execution_time_max)
    end
  }

  scope :with_start_time, proc { |start_time|
    if start_time.present?
      where("created_at >= ?", start_time)
    end
  }

  scope :with_end_time, proc { |end_time|
    if end_time.present?
      where("created_at <= ?", end_time)
    end
  }
end

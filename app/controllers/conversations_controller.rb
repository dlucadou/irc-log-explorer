class ConversationsController < ApplicationController
  include ConversationsHelper
  before_action :authenticate_user!
  before_action :set_conversation, only: [:show, :destroy]
  before_action :set_tab_data
  before_action :verify_ownership, only: [:show]
  before_action :verify_permissions, except: [:set_conversation] #, only: [:index, :new, :show, :create, :destroy]

  def index
    @prefs = ApplicationHelper.common_prefs

    # Search Fields
    search_params = search_conversation_params

    # Extract params
    conv_status = search_params[:locked] if search_params[:locked] && search_params[:locked].length > 0
    conv_subject = search_params[:subject] if search_params[:subject] && search_params[:subject].length > 0
    conv_op_msg = search_params[:op_msg_query] if search_params[:op_msg_query] && search_params[:op_msg_query].length > 0
    conv_any_msg = search_params[:all_msg_query] if search_params[:all_msg_query] && search_params[:all_msg_query].length > 0
    conv_user_unread = search_params[:user_unreads] if search_params[:user_unreads] && search_params[:user_unreads].length > 0
    conv_tgt_usr = current_user.id
    conv_start_time = search_params[:start_time] if search_params[:start_time] && search_params[:start_time].length > 0
    conv_end_time = search_params[:end_time] if search_params[:end_time] && search_params[:end_time].length > 0
    conv_per_page = search_params[:per_page] if search_params[:per_page] && search_params[:per_page].length > 0

    case conv_status
    when "all"
      conv_status = nil
    when "locked"
      conv_status = true
    when "unlocked"
      conv_status = false
    end

    case conv_user_unread
    when "all"
      conv_user_unread = nil
    when "true"
      conv_user_unread = true
    when "false"
      conv_user_unread = false
    end

    @search_errors = Conversation.validate_search(status: conv_status, subject: conv_subject, msg_target: conv_tgt_usr,
                                                  start_time: conv_start_time, end_time: conv_end_time,
                                                  has_user_unreads: conv_user_unread, op_msg_txt: conv_op_msg,
                                                  any_msg_txt: conv_any_msg, per_page: conv_per_page)

    query_notes = {}
    if @search_errors.length == 0
      begin
        query_time = Benchmark.realtime {
          @conversations = Conversation.search(status: conv_status, subject: conv_subject, msg_target: conv_tgt_usr,
                                               start_time: conv_start_time, end_time: conv_end_time,
                                               has_user_unreads: conv_user_unread, op_msg_txt: conv_op_msg,
                                               any_msg_txt: conv_any_msg)
        }
        @conversations.count # If I do not have this here, the exception will
          # not appear until @conversations.count in the view for generating
          # the table. Why that is I do not know, but the point remains that
          # I have to have this to catch any regex exceptions.
      rescue RegexpError, ActiveRecord::StatementInvalid => err # User entered invalid regex
        if !@search_errors.any? { |error| error.include? 'regex' }
          # Only add error message if invalid regex messages not yet added to list
          if err.message.match(ApplicationHelper.pg_invalid_regex_locator)
            @search_errors["#{I18n.t('.activerecord.errors.models.conversation.attributes.default.regex_error_generic')}"] =
              " (#{I18n.t('.activerecord.errors.models.conversation.attributes.default.regex_error_details_known')}: #{err.message.match(ApplicationHelper.pg_invalid_regex_locator)[1]})"
          else
            @search_errors["#{I18n.t('.activerecord.errors.models.conversation.attributes.default.regex_error_generic')}"] =
              " (#{I18n.t('.activerecord.errors.models.conversation.attributes.default.regex_error_details_unknown')})"
            logger.warn("User Conversations controller - failed to match rescued regex error message")
            logger.debug("Regex error message: #{err.message}, class: #{err.message.class}")
          end
        end
        query_time = Benchmark.realtime {
          @conversations = current_user.conversations
        }
      end
    else
      if @search_errors.length > 0
        flash[:error] = I18n.t('.activerecord.errors.generic.search.failed')
        query_notes = flash.to_h.merge(:search_errors => @search_errors)
      end
      query_time = Benchmark.realtime {
        @conversations = current_user.conversations
      }
    end

    custom_per_page = per_page == params[:per_page]
    # TODO: create Search record here

    @conversations = @conversations.order("#{conversation_sort_column} #{sort_direction}").page(params[:page]).per(conv_per_page)
  end

  def new
    @conversation = Conversation.new
    @conversation.messages.build
  end

  def show
    @conversation.read_by_user = Time.now
    @conversation.save!
  end

  def create
    convo_params = create_conversation_params
    convo_params[:user_id] = current_user.id
    convo_params[:target_id] = current_user.id
    convo_params[:creator_id] = current_user.id
    convo_params[:created_by] = current_user.id
    convo_params[:messages_attributes]['0'][:sender_id] = current_user.id
    convo_params[:messages_attributes]['0'][:sent_by] = current_user.id
    @conversation = Conversation.new(convo_params)
    if @conversation.save!
      redirect_to conversation_path(@conversation.id), notice: "#{I18n.t('.activerecord.errors.models.message.attributes.default.name')} #{I18n.t('.activerecord.errors.models.message.attributes.default.created', time: Message.edit_window_str)}"
    end
  end

  def destroy
  end

  private

  def conversation_params
    params.require(:conversation).permit(
      :user_id,
      :subject,
      :read_by_user,
      :read_by_admins,
      :locked
    )
  end

  def create_conversation_params
    params.require(:conversation).permit(
      :subject,
      messages_attributes: [
        :body,
        :body_plaintext]
    )
  end

  def search_conversation_params
    params.permit(
      :utf8,
      :locked,
      :subject,
      :op_msg_query,
      :user_unreads,
      :start_time,
      :end_time,
      :all_msg_query,
      :per_page,
      :column,
      :direction
    )
  end

  def set_conversation
    begin
      @conversation = Conversation.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      @conversation = nil
      if is_admin
        flash[:error] = "#{I18n.t('.activerecord.errors.models.conversation.attributes.default.name')} #{I18n.t('.activerecord.errors.models.conversation.attributes.default.not_found')}"
        redirect_to conversations_path
      else
        flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
        redirect_to root_path
      end
    end
  end

  def set_tab_data
    # Currently, this assumes the user is on the account page, not admin page
    @messages_tab = true
  end

  def permissions_list
    {
      :index => [:admin, :user],
      :new => [:admin, :user],
      :show => [:admin, :user],
      :create => [:admin, :user],
      :destroy => [:admin, :user]
    }
  end

  def verify_permissions
    if permissions_list[action_name.to_sym]&.index(current_user.role.to_sym).nil?
      # action_name is a Rails method for the controller method name
      # ( https://stackoverflow.com/a/4274222 )
      # Safe navigation operator (&) only calls if not nil.
      flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
      redirect_to root_path #, :status => :bad_request
      # I cannot return HTTP 403 with content, so the user gets a page that
      # says "You are being redirected." with a link to the root page.
      # Returning 200 is not best practice here but it looks best to the
      # user who never has to see an unformatted redirect page.
    end
  end

  def verify_ownership
    unless @conversation && @conversation.target_id == current_user.id
      flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
      redirect_to root_path
    end
  end
end

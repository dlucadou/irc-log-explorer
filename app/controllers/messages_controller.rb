class MessagesController < ApplicationController
  after_action :clear_flashes, only: [:index]
  before_action :authenticate_user!
  before_action :set_conversation
  before_action :set_message, only: [:edit, :mark_unread, :update, :destroy]
  before_action :set_tab_data
  before_action :verify_ownership, except: [:create, :new, :mark_unread, :set_conversation]
    # Reason for mark_unread being included - user can mark any message in the thread as unread;
    # it doesn't modify the message itself, only the conversation.
  before_action :verify_permissions, only: [:new, :create, :edit, :mark_unread, :update, :destroy]

  def new
    @message = @conversation.messages.new
  end

  def create
    user_view = true
    if user_view
      msg_params = user_message_params
    else
      msg_params = message_params
    end
    msg_params[:sender_id] = current_user.id
    msg_params[:sent_by] = current_user.id
    @message = @conversation.messages.new(msg_params)
    respond_to do |format|
      begin
        if @message.save!
          format.html { redirect_to conversation_path(@conversation.id), notice: "#{I18n.t('.activerecord.errors.models.message.attributes.default.name')} #{I18n.t('.activerecord.errors.models.message.attributes.default.created', time: Message.edit_window_str)}" }
          format.json { render :show, status: :created, location: @message }
        end
      rescue ActiveRecord::RecordInvalid
        format.html { redirect_to conversation_path(@conversation.id), alert: @message.errors }
        format.json { render :show, status: :internal_server_error, location: @conversation }
      end
    end
  end

  def edit
  end

  def mark_unread
    @conversation.read_by_user = @message.created_at
    @conversation.save!
    flash[:success] = "Marked #{@conversation.user_unread_count} messages in the conversation as unread."
    redirect_to conversations_path
  end

  def update
    @message = @conversation.messages.find(params[:id])
    @message.edited_at = Time.now.utc
    respond_to do |format|
      if @message.editable? && @message.update(user_message_params)
        # some redirect, will be based on the user's permission
        format.html { redirect_to conversation_path(@conversation.id), notice: 'Message edited.' }
        format.json { render :show, status: :ok, location: @conversation }
      else
        format.html { redirect_to conversation_path(@conversation.id), notice: 'Failed to edit message, the edit period might have expired.' }
        format.json { render :show, status: :internal_server_error, location: @message }
      end
    end
  end

  def destroy
    @message = @conversation.messages.find(params[:id])
    @message.deleter_id = current_user.id
    @message.deleted_by = current_user.id
    @message.user_visible = false

    respond_to do |format|
      if @message.valid?
        @message.save!
        # some redirect, will be based on the user's permission
        format.html { redirect_to conversation_path(@conversation.id), notice: 'Message deleted.' }
        format.json { render :show, status: :ok, location: @conversation }
      else
        # If I just try to use @message.errors, it causes an error because
        # @message is nil. If I try @conversation.messages[0].errors.any?,
        # it returns false for some reason.
        # I cannot figure out any better way to pass back the error message
        # to the view, as even setting my own global variable (like @errors)
        # will be nil in the view.
        error_list = ""
        @message.errors.full_messages.each { |message| error_list << "<li>#{message}</li>" }
        if error_list.length == 0
          error_list = '.'
        else
          error_list = ": <ul>#{error_list}</ul>"
        end

        format.html { redirect_to conversation_path(@conversation.id), notice: "Failed to delete message#{error_list}" }
        format.json { render :show, status: :internal_server_error, location: @message }
      end
    end
  end

  private

  def message_params
    params.require(:message).permit(
      :sender_id,
      :body,
      :body_plaintext,
      :user_visible,
      :deleter
    )
  end

  def user_message_params
    params.require(:message).permit(:body)
  end

  def set_conversation
    begin
      @conversation = Conversation.find(params[:conversation_id])
    rescue ActiveRecord::RecordNotFound
      @conversation = nil
      if is_admin
        flash[:error] = "#{I18n.t('.activerecord.errors.models.message.attributes.default.name')} #{I18n.t('.activerecord.errors.models.message.attributes.default.not_found')}"
        redirect_to conversations_path
      else
        flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
        redirect_to root_path
      end
    end
  end

  def set_message
    begin
      @message = Message.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      @message = nil
      if is_admin
        flash[:error] = "#{I18n.t('.activerecord.errors.models.message.attributes.default.name')} #{I18n.t('.activerecord.errors.models.message.attributes.default.not_found')}"
        redirect_to conversations_path
      else
        flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
        redirect_to root_path
      end
    end
  end

  def set_tab_data
    # Currently, this assumes the user is on the account page, not admin page
    @messages_tab = true
  end

  def verify_ownership
    unless @message && @message.sent_by == current_user.id
      flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
      redirect_to root_path
    end
  end

  def permissions_list
    {
      :new => [:user, :admin],
      :create => [:user, :admin],
      :edit => [:user, :admin],
      :mark_unread => [:user, :admin],
      :update => [:user, :admin],
      :destroy => [:user, :admin]
    }
  end

  def verify_permissions
    if permissions_list[action_name.to_sym]&.index(current_user.role.to_sym).nil?
      # action_name is a Rails method for the controller method name
      # ( https://stackoverflow.com/a/4274222 )
      # Safe navigation operator (&) only calls if not nil.
      flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
      redirect_to root_path #, :status => :bad_request
      # I cannot return HTTP 403 with content, so the user gets a page that
      # says "You are being redirected." with a link to the root page.
      # Returning 200 is not best practice here but it looks best to the
      # user who never has to see an unformatted redirect page.
    end
  end
end

require 'date'

class RegistrationsController < Devise::RegistrationsController
  after_action :clear_flashes, only: [:build_qr_code]
  before_action :authenticate_user!, except: [:create, :callback, :destroy,
    :new, :accept_tos, :finish_sso]
  prepend_before_action :authenticate_scope!, :only => [:account_overview,
    :account_security_settings, :account_reports, :account_manage_totp,
    :account_regen_totp, :account_recovery_codes, :edit]
    # Sets User for resource in Devise forms

  def create
    if request.post?
      @user = User.new
    end
    super
    # Create Preferences record for the user
    begin
      UserPreference.create!(user_id: @user.id)
    rescue ActiveRecord::RecordNotUnique, ActiveRecord::RecordInvalid
      logger.info "Failed to create UserPreference from account registration page. You can usually ignore this, it typically occurs when a User object fails to create in the call to super (e.g. user enters a short password or doesn't set an email address)."
    end
    # I tried putting the code above inside a super do block, but because the
    # user hasn't actually been saved to the DB at the time the yield block is
    # executed, the user has no ID, so the UserPreference will always fail to
    # create (since user_id would be nil).
    # I could work around this by simply copy-pasting the code from Devise
    # into here and modifying it as necessary, but the problem with that is
    # I have to keep tabs on any modifications to the method upstream and
    # backport them, which I don't intend on doing.
    #
    # If someone knows a better way of doing this, let me know.
    # A copy of this code exists in the User model for accounts created with
    # SSO, as those do not call RegistrationsController.create at any point.
  end

  def callback
    auth_response = WebAuthn::AuthenticatorAttestationResponse.new(
      attestation_object: str_to_bin(params[:response][:attestationObject]),
      client_data_json: str_to_bin(params[:response][:clientDataJSON])
    )

    user = User.find_by_id(session[:user_id])

    raise "User with ID #{session[:user_id]} never initiated sign up" unless user

    render json: { status: "forbidden" },
           status: :forbidden unless auth_response.valid?(str_to_bin(user.current_challenge), request.base_url)

    credential = user.credentials.find_or_initialize_by(
      external_id: Base64.strict_encode64(auth_response.credential.id)
    )
    credential.update!(
      nickname: params[:credential_nickname],
      public_key: Base64.strict_encode64(auth_response.credential.public_key)
    )

    sign_in(user)
    render json: { status: "ok" }, status: :ok
  end

  def destroy
    user_id = current_user&.id
    user_id ||= session[:user_id]
    flash.merge!(User.delete_user(user_id: user_id, first_person: true))
    redirect_to root_path
  end

  # GET/POST /policy/tos
  def accept_tos
    # Currently, this does not force users to accept it on login or on account
    # creation, but I will need to force users to accept it in the future.

    # This view is actually a good way to test the logged-in status of a user.
    # If a user has a session_id (partial authentication), they will get a
    # prompt to accept it without being logged in. If the user is fully logged
    # in, they also get the prompt (but you can tell you're logged in because
    # there's a sign out button). If you are not signed in at all, there will
    # be no prompt, just the TOS.
    user = User.find_by_id(session[:user_id])
    if user.nil?
      user = Current.user
    end
    @User = user

    if request.get?
      @UserPresent = !user.nil? # Used to show (or not show) the "I agree" box and continue button
      if !user.nil?
        if !user.eula_acceptance_needed?
          flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.tos.already_accepted')
        end
        # In the future, I'll need to extend this to check the EULA update
        # date to figure out if the user needs to re-accept it.
        # If the user does need to re-accept it, the flash should say the TOS
        # has changed and they need to review it, otherwise it should still
        # say "You have already accepted the terms of service."
      end
    elsif request.post?
      case params[:user][:tos_accept]
      when "agree" # Agree
        user.eula_accepted_at = DateTime.now
        user.save!
        flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.tos.option_accept')
        #if is_2fa_required(user)
        #  redirect_to default_2fa_path(user)
        #else
          sign_in user
          redirect_to root_path
        #end
      when "disagree-temp" # Disagree, but do not delete account
        flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.tos.option_decline_keep')
        user.eula_accepted_at = nil
        user.save!
        session[:user_id] = nil
        sign_out user # does nothing if user isn't signed in
        redirect_to root_path
      when "disagree-perm" # Disagree, do delete account
        if user.is_banned?
          flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.tos.option_invalid', option: params[:user][:tos_accept])
          logger.error "User with ID #{user.id} appears to have tampered with the EULA acceptance form. Form was submitted with the value \"#{params[:user][:tos_accept]}\" despite being banned."
          sign_out user
          redirect_to policy_tos_path
        else
          destroy # calls RegistrationsController.destroy
          session[:user_id] = nil
          flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.tos.option_decline_delete')
          # Automatically redirects to root_path from the destroy method, no redirect needed
        end
      else
        user.eula_accepted_at = nil
        user.save!
        flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.tos.option_invalid', option: params[:user][:tos_accept])
        logger.error "User with ID #{user.id} appears to have tampered with the EULA acceptance form. Form was submitted with the value \"#{params[:user][:tos_accept]}\"."
        session[:user_id] = nil
        sign_out user # does nothing if user isn't signed in
        redirect_to root_path
      end
    end
  end

  # GET/DELETE /account/(:id/)finish_sso
  def finish_sso
    if Current.user.nil?
      # The way this works is that after doing Oauth, if email was not given,
      # the user is redirected here without being signed in, and they will
      # have session data, including a user_id.
      # Not being signed in means Current.user will be nil. However, if a user
      # opens this page in another browser, they have not signed in but also
      # do not have any session data to identify the user with, which is why I
      # have the rescue section below.
      begin
        user = User.find(session[:user_id])
        if user.facebook_oauth
          provider = "Facebook"
        elsif user.twitter_oauth
          provider = "Twitter"
        else
          provider = "(unknown)"
        end

        if request.post? && params[:user][:email] && provider != "(unknown)"
          begin
            if params[:user][:email].length >= UserHelper.min_email_length &&
                user.update!(:email => params[:user][:email])
              flash[:success] = I18n.t('.activerecord.errors.models.user.attributes.oauth.email_set_success')
              if user.eula_acceptance_needed?
                redirect_to policy_tos_path
              else
                sign_in_and_redirect user, notice: "Signed in!"
              end
            elsif params[:user][:email].length == 0
              flash[:error] = "#{I18n.t('.activerecord.errors.models.user.attributes.backup_email.name')} #{I18n.t('.activerecord.errors.models.user.attributes.email.blank')}"
              redirect_to finish_sso_path
            elsif params[:user][:email].length < 6
              flash[:error] = "#{I18n.t('.activerecord.errors.models.user.attributes.backup_email.name')} #{I18n.t('.activerecord.errors.models.user.attributes.email.invalid')}"
              redirect_to finish_sso_path
            else
              logger.error "Unknown error updating email address '#{params[:user][:email]}' to user with ID #{session[:user_id]} from the finish_sso page. The user's email is currently '#{user.email}', callback is '#{params[:callback]}'."
              flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.oauth.email_set_error')
            end
          rescue ActiveRecord::RecordInvalid
            flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.oauth.access_denied_email_used', :provider => provider)
            redirect_to finish_sso_path
          end
        elsif request.post? && provider == "(unknown)"
          flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.oauth.integration_unknown')
          redirect_to root_path
        elsif request.delete?
          destroy # calls RegistrationsController.destroy
          flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.oauth.integration_released', :provider => provider)
        elsif !request.get?
          logger.error "Unknown error on finish_sso page."
        end
      rescue ActiveRecord::RecordNotFound
        # Not signed in and hasn't used Oauth w/o providing email.
        flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.oauth.access_denied_email_grab_page')
        redirect_to root_path
      end
    else
      # User is signed in and has already provided an email address (or their
      # Oauth provider included it), so they should not be on this page.
      flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.oauth.access_denied_already_setup')
      redirect_to root_path
    end
  end

  def account_manage_totp
    # To quickly test TOTP without using an app, https://totp.danhersam.com/ is very helpful
    user = Current.user
    if request.get?
      if user.otp_required_for_login
        # User is on the disable TOTP page
        @user_has_u2f = user.two_factor_u2f_enabled?
      else
        # User is on the enable TOTP page
        if user.otp_secret.nil?
          # User is going to enable TOTP
          user.otp_secret = User.generate_otp_secret(32)
            # In Gitlab CI, without adding the length of 32, it will cause
            # an ArgumentError that the key must be 32 bytes.
            # By default, devise-two-factor uses 24, so this overrides it
            # and prevents this error.
            # This error only occurs in Gitlab CI - not in dev, prod, or when
            # running gitlab-runner locally. I have no idea why, but this
            # should prevent CI test failures.
          user.otp_verification_timeout = DateTime.now() + 5.minutes
          user.save!
        else
          # User is probably attempting to enable totp but entered a wrong
          # code, no need to generate a secret
          # User is probably attempting to enable TOTP
          if !user.otp_verification_timeout.nil? && DateTime.now() > user.otp_verification_timeout
            # User has waited too long, the TOTP seed has expired.
            # Note that this is different from a user POSTing a confirmation
            # code after 6 minutes, as this requires a user to have not POSTed
            # a code, i.e. the user opened the page, hit back, and returned
            # after some length of time. Maybe the user accidentally clicked
            # on the link and didn't want to enable TOTP, but now they do.
            # The different error message is designed to address that case.
            flash[:error] ||= I18n.t('.activerecord.errors.models.user.attributes.totp.seed_timeout_setup')
            user.otp_secret = User.generate_otp_secret
            user.otp_verification_timeout = DateTime.now() + 5.minutes
            user.save!
          end
        end
        # QR code generation has to be done after TOTP seed generation
        # or the user will get a QR code with invalid data
        @QRCodeSVG = build_qr_code(type: :svg)
        if Rails.configuration.application[:users][:totp][:generate_png_qrcodes]
          @QRCodePNG = build_qr_code(type: :png)
        else
          @QRCodePNG = nil
        end
      end
    elsif request.post?
      if params[:user][:otp_attempt] && valid_otp_attempt?(user, params[:user])
        # User has entered a valid TOTP code or recovery code
        if user.otp_required_for_login
          # User is disabling TOTP
          user.otp_required_for_login = false
          user.otp_secret = nil
          user.otp_enabled_at = nil
          user.save!

          if !is_2fa_required(user) && user.otp_backup_codes
            # Only clears recovery codes if no other 2FA methods are enabled
            # and the user has recovery codes
            user.otp_backup_codes = nil
            user.otp_backup_codes_enabled_at = nil
            user.save!
            flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.totp.disabled_recovery_codes')
          end
          flash[:success] = I18n.t('.activerecord.errors.models.user.attributes.totp.disabled')
          redirect_to account_security_path
        else
          # User is attempting to enable TOTP
          if !user.otp_verification_timeout.nil? && DateTime.now() > user.otp_verification_timeout
            # User did not enter TOTP code in the 5 minute window
            flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.totp.seed_timeout_setup')
            user.otp_secret = User.generate_otp_secret
            user.otp_verification_timeout = DateTime.now() + 5.minutes
            user.save!
            @QRCodeSVG = build_qr_code(type: :svg)
            if Rails.configuration.application[:users][:totp][:generate_png_qrcodes]
              @QRCodePNG = build_qr_code(type: :png)
            else
              @QRCodePNG = nil
            end
          else
            # User did enter the TOTP code in the 5 minute window
            user.otp_required_for_login = true
            user.otp_verification_timeout = nil
            user.otp_enabled_at = DateTime.now()
            user.save!
            flash[:success] = I18n.t('.activerecord.errors.models.user.attributes.totp.code_valid_enable')
            redirect_to account_security_path
          end
        end
      else # TOTP/recovery code is not valid
        if user.otp_required_for_login
          # User is trying to disable TOTP
          flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.totp.code_invalid_disable')
          # Probably a timing error.
          redirect_to account_security_2fa_totp_path
        else
          # User is trying to enable TOTP
          if !user.otp_verification_timeout.nil? && DateTime.now() > user.otp_verification_timeout
            flash[:error] = flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.totp.seed_timeout_reset')
          else
            flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.totp.code_invalid')
          end
          redirect_to account_security_2fa_totp_path
        end
      end
    end
  end

  def account_manage_totp_with_u2f
    # Make sure TOTP is enabled or recovery codes exist, and that U2F is enabled
    user = User.find_by_id(current_user.id)
    if !user.two_factor_otp_enabled?
      flash[:error] = "Must have TOTP setup to disable it."
      redirect_to account_security_path and return
    elsif !user.two_factor_u2f_enabled?
      flash[:error] = "Must have a U2F token setup to remove TOTP with them."
      redirect_to account_security_path and return
    end
    @SecurityTabPage = true
    @user_has_totp = user.two_factor_otp_enabled?
    @user_has_recovery_codes = user.otp_backup_codes.length > 0 if user.otp_backup_codes

    if request.post?
      if params[:id] && params[:response]
        # These params are present in responses from the token
        auth_response = WebAuthn::AuthenticatorAssertionResponse.new(
          credential_id: str_to_bin(params[:id]),
          client_data_json: str_to_bin(params[:response][:clientDataJSON]),
          authenticator_data: str_to_bin(params[:response][:authenticatorData]),
          signature: str_to_bin(params[:response][:signature])
        )

        allowed_credentials = user.credentials.map do |cred|
          {
            id: str_to_bin(cred.external_id),
            public_key: str_to_bin(cred.public_key)
          }
        end

        if !auth_response.valid?(str_to_bin(user.current_challenge), request.base_url, allowed_credentials: allowed_credentials)
          flash[:error] = "Unrecognized token, did you insert an unregistered device?"
          render json: { status: "forbidden" }, status: :forbidden unless auth_response.valid?(
            str_to_bin(user.current_challenge),
            request.base_url,
            allowed_credentials: allowed_credentials
          )
        else
          flash[:success] = "Disabled TOTP."
          render json: { status: "ok" }, status: :ok
          # No redirect statement needed, that occurs in
          # assets/javascripts/credentials_helper.js

          # User is disabling TOTP
          user.otp_required_for_login = false
          user.otp_secret = nil
          user.otp_enabled_at = nil
          user.save!
          # No need to clear recovery codes, as the user has to have U2F
          # enabled to be on this page.
        end
      else
        # User has pressed the U2F prompt button
        user = User.find_by_id(session[:user_id])

        if user
          credential_options = WebAuthn.credential_request_options
          credential_options[:allowCredentials] = user.credentials.map do |cred|
            { id: cred.external_id, type: "public-key" }
          end

          credential_options[:challenge] = bin_to_str(credential_options[:challenge])
          user.update!(current_challenge: credential_options[:challenge])

          respond_to do |format|
            format.json { render json: credential_options }
          end
        else
          respond_to do |format|
            format.json { render json: { errors: ["Username doesn't exist"] }, status: :unprocessable_entity }
          end
        end
      end
    end
  end

  def account_regen_totp
    if request.post?
      user = Current.user
      if !user.otp_required_for_login?
        user.otp_secret = nil
        user.otp_verification_timeout = nil
        user.save!
        flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.totp.seed_reset_succeeded')
        redirect_to account_security_2fa_totp_path
      else
        flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.totp.seed_reset_failed_disable')
        logger.warn "User with ID #{user.id} attempted to reset their TOTP seed despite having TOTP enabled. The button to do this should not have been visible; the user may be attempting to mess with the service."
        redirect_to account_security_2fa_totp_path
      end
    end
  end

  def account_overview
    if request.get?
      user = Current.user
      # Current login info
      @UserLastLoginTime = ApplicationHelper.datetime_to_str(user.current_sign_in_at, show_offset: true, show_seconds: true)
      @UserLastLoginIP = user.current_sign_in_ip
      # Previous login info
      @UserPreviousLoginTime = ApplicationHelper.datetime_to_str(user.current_sign_in_at, show_offset: true, show_seconds: true)
      @UserPreviousLoginIP = user.last_sign_in_ip
    end
  end

  def account_reports
    # this will gather all the user's reports so they can be displayed, using
    # logic similar to the ChatLogsController index method
  end

  def account_recovery_codes
    if current_user.two_factor_enabled?
      user = current_user
      @AccountRecoveryCodes = user.generate_otp_backup_codes!
      user.otp_backup_codes_enabled_at = DateTime.now()
      user.save!
    else
      if current_user.otp_backup_codes
        # Only clears recovery codes if no other 2FA methods are enabled
        # and the user has recovery codes
        current_user.otp_backup_codes = nil
        current_user.otp_backup_codes_enabled_at = nil
        current_user.save!
        flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.totp.disabled_recovery_codes')
      else
        flash[:notice] = I18n.t('.activerecord.errors.models.user.attributes.totp.recovery_codes_ungeneratable')
      end
      redirect_to account_security_path
    end
  end

  def account_security_settings
    user = Current.user
    # Current login info
    @UserLastLoginTime = ApplicationHelper.datetime_to_str(user.current_sign_in_at, show_offset: true, show_seconds: true)
    @UserLastLoginIP = user.current_sign_in_ip
    # Previous login info
    @UserPreviousLoginTime = ApplicationHelper.datetime_to_str(user.last_sign_in_at, show_offset: true, show_seconds: true)
    @UserPreviousLoginIP = user.last_sign_in_ip
    # Time TOTP was enabled at
    if user.otp_required_for_login?
      @TOTPEnabledTime = ApplicationHelper.datetime_to_str(user.otp_enabled_at, show_offset: false, show_seconds: true)
    end
    # Number of U2F devices
    @U2FDevices = user.credentials.all.length
    # Time U2F was enabled at
    if @U2FDevices > 0
      @U2FEnabledTime = ApplicationHelper.datetime_to_str(user.credentials.all.first.created_at, show_offset: false, show_seconds: true)
      # The first U2F device will be the oldest one, so its created_at date
      # can be considered when U2F was enabled for the account since the
      # devise-fido-u2f gem provides no easy helper method or attribute for
      # when the user originally added a U2F token. (The first token added
      # may have been deleted, so the first in the list is not necessarily
      # indicative of the time U2F was first enabled).
    end

    if user.otp_backup_codes_enabled_at
      @RecoveryCodeGenerationDate = ApplicationHelper.datetime_to_str(user.otp_backup_codes_enabled_at, show_offset: false, show_seconds: true)
    end
    if request.post?
      @SecurityTabPage = true
        # The account_tabs file will not properly highlight the Security tab
        # after POSTing for some reason, this guarantees it will highlight.
      params[:user][:email] = current_user.email
        # Cannot update resource without email (note that just because backup
        # email is not assigned does not mean it will be erased if the user
        # has a backup email address setup).
      update_resource(current_user, account_update_params)
      #resource_updated = update_resource(current_user, account_update_params)
      #if resource_updated
      #  flash[:notice] = flash[:success] = I18n.t('.activerecord.errors.models.user.attributes.default.update_succeeded')
      #end
    end
  end

  protected

  def after_sign_up_path_for(resource)
    # Overrides superclass's after_sign_up_path_for method to
    # force EULA acceptance for newly created traditional accounts.
    # This method is called from the superclass RegistrationsController.create
    # method.
    if @user.eula_acceptance_needed?
      # Technically this is optional, but if I add a checkbox in the future
      # that says "I accept the terms of service" on the registration form,
      # this will be necessary.
      policy_tos_path
    else
      super
    end
  end

  def after_update_path_for(resource)
    if request.referrer.index(account_security_path)
      account_security_path
    else
      account_settings_path
    end
  end

  def sign_up(resource_name, resource)
    # Overrides Registrations.sign_up to force EULA acceptance for newly
    # registered traditional accounts.
    session[:user_id] = @user.id
    if @user.eula_acceptance_needed?
      # Doesn't call sign_in method (the accept_tos method will sign them in
      # after they accept the TOS)
      return
    else
      super # Calls the sign_in method
    end
  end

  def update_resource(resource, params)
    # Based on https://www.reddit.com/r/rails/comments/5eufg4/devise_and_omniauth_set_password_after/daoc17h/
    if !resource.confirmable # Oauth only
      if !params[:email].blank?
        resource.email = params[:email]
      else
        resource.errors.add(I18n.t('.activerecord.errors.models.user.attributes.email.name'), I18n.t('.activerecord.errors.models.user.attributes.email.blank'))
      end

      if !params[:backup_email].nil?
        # I can't use .blank? here because backup_email is an optional
        # field, so if the user wants to remove backup_email, .blank?
        # will always be true (and !.blank? will be false), so it will
        # not update. .nil? is only true (and !.nil? only false) when
        # the user deletes the text field from the page.
        resource.backup_email = params[:backup_email]
      end

      if !params[:password].blank? && params[:password] == params[:password_confirmation]
        resource.password = params[:password]
        resource.confirmable = true # No longer an SSO-only account
      end
      if params[:password].length < resource.minimum_password_length || params[:password_confirmation].length < resource.minimum_password_length
        # True when either password or password_confirmation do not meet the
        # minimum password length
        resource.errors.add(I18n.t('.activerecord.errors.models.user.attributes.password.name'), I18n.t('.activerecord.errors.models.user.attributes.password.invalid'))
      end
      if params[:password] != params[:password_confirmation]
        # True when the user enters non-matching  password and password_confirmation
        resource.errors.add(I18n.t('.activerecord.errors.models.user.attributes.password.name'), I18n.t('.activerecord.errors.models.user.attributes.password.confirmation'))
      end

      if resource.errors.any?
        flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.default.update_failed')
        return false
      end

      begin
        resource.save!
        if !params[:password].blank?
          clean_up_passwords(resource)
          flash[:success] = I18n.t('.activerecord.errors.models.user.attributes.default.sso_conversion')
        end
        # Users will get flash[:notice] = "Your account has been updated successfully." from Devise on successful save, so no flash for non-password success is needed.
        return true
      rescue ActiveRecord::RecordInvalid
        # Validation errors will be listed by calls to
        # "render 'devise/shared/error_messages'" within views.
        flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.default.save_failed')
        return false
      rescue # All other errors
        flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.default.unknown_error')
        logger.error "Unknown error attempting to save account settings (params: #{params.keys}) on SSO-only user with ID #{session[:user_id]}. Resource did not save."
        return false
      end
    else
      updated = resource.update_with_password(params)
      if resource.errors.any?
        flash[:error] = I18n.t('.activerecord.errors.models.user.attributes.default.update_failed')
      else
        flash[:success] = I18n.t('.activerecord.errors.models.user.attributes.default.update_succeeded')
      end
      return updated
    end
  end

  private

  def valid_otp_attempt?(user, user_params)
    user.validate_and_consume_otp!(user_params[:otp_attempt]) ||
      user.invalidate_otp_backup_code!(user_params[:otp_attempt])
  end

  def valid_otp_attempt_no_recovery?(user, user_params)
    user.validate_and_consume_otp!(user_params[:otp_attempt])
  end

  def build_qr_code(type: :svg)
    issuer = Rails.configuration.application[:users][:totp][:issuer_name]
    uri = current_user.otp_provisioning_uri("#{issuer}:#{current_user.email}", issuer: issuer)
    Base64.encode64(RQRCode::render_qrcode(uri, type, level: :m, unit: 3, fill: 'ffffff'))
    # Fill color prevents the QR code from being unreadable with Solarized &
    # Dark themes
  end

  def sign_up_params
    params.require(:user).permit(
      :name,
      :email,
      :backup_email,
      :password,
      :password_confirmation
    )
  end

  def account_update_params
    if(@user.confirmable)
      params.require(:user).permit(
        :name,
        :email,
        :backup_email,
        :password,
        :password_confirmation,
        :current_password,
        :reset_password_token
      )
    else
      params.require(:user).permit(
        :name,
        :email,
        :backup_email,
        :password,
        :password_confirmation,
        :reset_password_token
      )
    end
  end
end

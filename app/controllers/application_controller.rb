require 'base64'

class ApplicationController < ActionController::Base
  after_action :clear_flashes, except: [:authenticate_user!]
  before_action :set_current_user
  before_action :configure_permitted_parameters, if: :devise_controller?
  helper_method :generate_header_tags, :get_application_name, :registerable
  protect_from_forgery with: :exception, prepend: true

  def registerable
    # Eventually, this will read in from a settings file
    registerable = true
    return registerable
  end

  def generate_header_tags
    # Helpers do not have access to the request hash, so I have to put this
    # method in a Controller.
    if I18n.t('.domains')[request.host.to_sym]
      domain_config = I18n.t('.domains')[request.host.to_sym][:header_tags]
    else
      logger.error "Accessed by domain not in translation file: #{request.host} (will generate default tags)"
      domain_config = I18n.t('.domains')[:default][:header_tags]
    end

    header_tags = ""
    domain_config.each { |tag, data|
      if data.is_a? String
        header_tags << "<#{tag}>#{data}</#{tag}>\n"
      elsif data.is_a? Array # Array of hashes
        data.each { |element| # Each element of array
          header_tags << "<#{tag} "
          element.each { |attribute, value| # Hash of element of array
            # Insert any needed values
            if value.include? '%{host}'
              value.gsub!('%{host}', request.host)
              # request.host returns hostname. e.g. if the application is
              # accessed on logs.example.com, it returns "logs.example.com"
            end
            if value.include? '%{domain}'
              value.gsub!('%{domain}', request.domain)
              # request.domain returns domain. e.g. if the application is
              # accessed on logs.example.com, it returns "example.com"
            end
            if value.include? '%{favicon_path}'
              value.gsub!('%{favicon_path}',
                          ActionController::Base.helpers.image_path('favicon.png'))
            end
            if value.match(ApplicationHelper.image_path_regex)
              value.gsub!(value.match(ApplicationHelper.image_path_regex)[0],
                          ActionController::Base.helpers.image_path(
                            value.match(ApplicationHelper.image_path_regex)[1]
                          ))
              # Used to reference non-favicon images
              # e.g. "%{image_path('test.png')}" => "/assets/test-hash.png"
              # When an asset is not present, it raises a
              # Sprockets::Rails::Helper::AssetNotFound error. I could catch
              # this, but I do not do this for any other asset errors (plus I
              # include debugging instructions in README.md), so I see no
              # reason to rescue the error here. Plus, the error might never be
              # noticed otherwise if it just resulted in embeds on 1 website
              # not including an image. This ensures the error will be obvious.
            end
            if value.include? '%{path}'
              value.gsub!('%{path}', request.path)
            end
            header_tags << "#{attribute}=\"#{value}\" "
          }
          header_tags << "/>\n"
        }
      end
    }
    return header_tags
  end

  def get_application_name
    # Helpers do not have access to the request hash, so I have to put this
    # method in a Controller.
    if I18n.t('.domains')[request.host.to_sym]
      domain_config = I18n.t('.domains')[request.host.to_sym]
    else
      logger.error "Accessed by domain not in translation file: #{request.host} (will use default application name)"
      domain_config = I18n.t('.domains')[:default]
    end
    return domain_config[:application_name]
  end

  protected

  def admin_tab_badge_data
    # For the unread reports tab badge
    @UnresolvedReports = Report.where(:resolved_at => nil).count
  end

  def clear_flashes
    flash.discard
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_in, keys: [:username, :email, :password, :login, :remember_me, :otp_attempt])
  end

  def authenticate_user! (*args)
    # I've noticed going to the chat logs path (currently /chat_logs) without
    # being logged in calls authenticate_user! with 0 args, while going to
    # the settings path (currently /users/edit) calls with the param "force",
    # a boolean which is true (force.class == TrueClass).
    # I use *args to absorb any parameters, just incase some controller
    # decides to throw 5 at this method in the future for whatever reason.
    # Credit to https://stackoverflow.com/a/23585023 for this method
    # (however, before_filter doesn't work so I have to use before_action)
    # and https://stackoverflow.com/a/4683890 for *args
    if user_signed_in? # User is signed in
      if current_user.eula_acceptance_needed?
        sign_out current_user
        flash[:info] = I18n.t('.activerecord.errors.models.user.attributes.tos.option_decline_keep')
        redirect_to login_path
      else
        if !current_user.is_banned? # User is not banned
          super
        else # User is banned
          verify_not_banned!
        end
      end
    else # User is not signed in
      redirect_to login_path, :notice => I18n.t('.activerecord.errors.permissions.general.auth_required')
    end
  end

  def is_admin
    current_user.role == 'admin'
  end

  def partially_authenticate_user!
    if !session[:user_id]
      redirect_to login_path, notice: I18n.t('.activerecord.errors.permissions.general.auth_required')
    elsif user_signed_in?
      redirect_to root_path, notice: I18n.t('.activerecord.errors.permissions.general.partial_auth_required')
    end
  end

  def set_domain
    @domain = ApplicationHelper.domain(request.url)
  end

  def verify_not_banned! (*args)
    if user_signed_in? && current_user.is_banned?
      # user_signed_in? prevents exceptions from current_user being nil
      # when not signed in
      if current_user.permanently_banned?
        notice = "You have been permanently banned. Reason:" \
                  " #{current_user.bans.last.external_reason}"
      else
        notice = "You have been banned until" \
                  " #{ApplicationHelper.datetime_to_str(current_user.banned_until, show_offset: true, show_seconds: false)}." \
                  " Reason: #{current_user.bans.last.external_reason}"
      end

      # Check if ban can be appealed
      if current_user.bans.last.ban_appealable?
        # Since the ban can be appealed, the user will not be logged out
        # However, their actions are very limited
        notice += "<br />You may appeal this decision by <a href=\"#{url_for(conversations_path)}\">contacting the administrators</a>.".html_safe
        if !ApplicationHelper.banned_user_permissions.include? controller_name
          error = I18n.t('activerecord.errors.permissions.general.unauthorized')
          flash[:error] = error
          redirect_to root_path
        end
      else
        # Since the ban cannot be appealed, the user will be logged out
        sign_out current_user
        redirect_to root_path
      end
      flash[:notice] = notice
    end
  end

  private

  def str_to_bin(str)
    Base64.strict_decode64(str)
  end

  def bin_to_str(bin)
    Base64.strict_encode64(bin)
  end

  def set_current_user
    Current.user = current_user
    # This is intended to allow easy access to the current user from the model,
    # specifically the user model (which handles Oauth, so I need to be able
    # to access the current user to add/remove integrations).
    # This is not best practice, but it is a necessary evil.
    # Source: https://evilmartians.com/chronicles/rails-5-2-active-storage-and-beyond#current-everything
    # Found the source on: https://stackoverflow.com/a/49886703
  end

  def is_2fa_required(resource)
    return (resource&.credentials && resource.credentials.count > 0) || resource&.otp_required_for_login
    # &. is the safe navigation operator
  end

  def default_2fa_path(resource)
    if resource.credentials.count > 0
      account_auth_2fa_u2f_prompt_path
    else # User must only have TOTP 2FA enabled
      account_auth_2fa_totp_path
    end
  end
end

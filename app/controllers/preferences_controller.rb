class PreferencesController < ApplicationController
  after_action :clear_flashes, except: [:account_display_settings, :account_notification_settings]
  before_action :authenticate_user!
  include PreferencesHelper

  def account_display_settings
    user = Current.user
    user ||= current_user
    @UserPreference = user.user_preference

    # Validation stuff
    date_key = PreferencesHelper.valid_date_formats.key(current_user.user_preference.date_format).to_s
    sort_key = PreferencesHelper.valid_sort_columns.key(@UserPreference[:default_sort_col]).to_s
    @UserPreferencesHash = {:tz_offset => @UserPreference[:tz_offset],
      :use_24hr_time => @UserPreference[:use_24hr_time],
      :date_format => [date_key, @UserPreference[:date_format]],
      :default_sort_col => [sort_key, @UserPreference[:default_sort_col]],
      :default_sort_order => @UserPreference[:default_sort_order],
      :search_results => @UserPreference[:search_results],
      :theme => @UserPreference[:theme]
    }
      # The reason I have @UserPreferencesHash is because for some reason,
      # I cannot get the form to auto-fill the user's preferences.
      # It works just fine when editing a ChatLog or User, but for some
      # reason UserPreference objects will not auto-populate, so I had to
      # make this as a workaround. It's probably because it's a joined
      # table, but I've tried countless fixes and even found some tutorials
      # that looked promising, but nothing has worked.
      # If anyone figures out what is going on and can fix the view so this
      # workaround is unnecessary, please submit an MR, this feels like a
      # dirty hack and I'd love to get rid of it.

    # The following are not hacks, they're just the most efficient way to
    # load options for <select> menus without seeding the DB with Users and
    # UserPreferences that have all the valid options and then looping over
    # those in the view.
    # If anyone knows of a better way, please let me know.
    @DateFormats = PreferencesHelper.valid_date_formats.to_a
      # Although Hash.to_a is [[:'YYYY-MM-DD', '%Y-%m-%d'], ...] (note that
      # :'YYYY-MM-DD' is a symbol), when I shove it into a view, it converts
      # symbols to strings, so the : is not shown.
    @SortColumns = valid_sort_columns
    @SortOrders = valid_sort_orders
    @Themes = valid_themes
      # I could easily just hardcode these valeus in the view since there
      # are not many options, but using this will allow me to add more
      # themes later on without hunting for references in views later on.
    if request.patch? || request.post? || request.put?
      prefs = params[:user_preference]
      if !prefs.nil?
        @UserPreference[:default_sort_order] = prefs[:default_sort_order]
        @UserPreference[:default_sort_col] = prefs[:default_sort_col]
        @UserPreference[:search_results] = prefs[:search_results]
        @UserPreference[:theme] = prefs[:theme]
        @UserPreference[:date_format] = prefs[:date_format]
        @UserPreference[:use_24hr_time] = prefs[:use_24hr_time]
        @UserPreference[:tz_offset] = prefs[:tz_offset]

        # Add rest of settings to prevent validation errors
        @UserPreference.notify_pwchange_pri = @UserPreference.notify_pwchange_pri
        @UserPreference.notify_pwchange_bkp = @UserPreference.notify_pwchange_bkp
        @UserPreference.notify_pwreset_pri = @UserPreference.notify_pwreset_pri
        @UserPreference.notify_pwreset_bkp = @UserPreference.notify_pwreset_bkp
        @UserPreference.notify_sso_pri = @UserPreference.notify_sso_pri
        @UserPreference.notify_sso_bkp = @UserPreference.notify_sso_bkp
        @UserPreference.notify_2fa_pri = @UserPreference.notify_2fa_pri
        @UserPreference.notify_2fa_bkp = @UserPreference.notify_2fa_bkp
        @UserPreference.notify_admin_pri = @UserPreference.notify_admin_pri
        @UserPreference.notify_admin_bkp = @UserPreference.notify_admin_bkp

        respond_to do |format|
          if @UserPreference&.save
            flash[:success] = "Successfully updated settings."
            format.html { redirect_to account_ui_path }
            format.json { render :account_ui, status: :ok, location: @UserPreference }
          else
            failure_message = "Failed to update report, see "
            if @UserPreference.errors.count > 1
              failure_message << "errors"
            else
              failure_message << "error"
            end
            failure_message << " below."
            flash[:error] = failure_message
            format.html { render 'account_display_settings' }
            format.json { render json: @UserPreference.errors, status: :unprocessable_entity }
          end
        end
      else # prefs is nil
        flash[:error] = "Error: blank form submitted."
        logger.warn "User with ID #{user.id} appears to have tampered with the UI settings form. prefs was nil, so the user probably deleted every option on the page."
        redirect_to account_ui_path
      end
    end
  end

  def account_notification_settings
    user = Current.user
    user ||= current_user
    @UserPreference = user.user_preference
    @UserSettablePreferences = user_settable_prefs
      # This determines what checkboxes will be enabled on the notification
      # preferences page.
      # Eventually, this will be able to be controlled via a configuration
      # file.

    @UserPreferencesHash = {
      :notify_pwchange_pri => @UserPreference[:notify_pwchange_pri],
      :notify_pwchange_bkp => @UserPreference[:notify_pwchange_bkp],
      :notify_pwreset_pri => @UserPreference[:notify_pwreset_pri],
      :notify_pwreset_bkp => @UserPreference[:notify_pwreset_bkp],
      :notify_sso_pri => @UserPreference[:notify_sso_pri],
      :notify_sso_bkp => @UserPreference[:notify_sso_bkp],
      :notify_2fa_pri => @UserPreference[:notify_2fa_pri],
      :notify_2fa_bkp => @UserPreference[:notify_2fa_bkp],
      :notify_admin_pri => @UserPreference[:notify_admin_pri],
      :notify_admin_bkp => @UserPreference[:notify_admin_bkp]
    }
      # See comment in account_display_settings for why I have this.
    if request.patch? || request.post? || request.put?
      prefs = params[:user_preference]
      if !prefs.nil?
        @UserPreference.notify_pwchange_pri = prefs[:notify_pwchange_pri] if !prefs[:notify_pwchange_pri].nil?
        @UserPreference.notify_pwchange_bkp = prefs[:notify_pwchange_bkp] if !prefs[:notify_pwchange_bkp].nil?
        @UserPreference.notify_pwreset_pri = prefs[:notify_pwreset_pri] if !prefs[:notify_pwreset_pri].nil?
        @UserPreference.notify_pwreset_bkp = prefs[:notify_pwreset_bkp] if !prefs[:notify_pwreset_bkp].nil?
        @UserPreference.notify_sso_pri = prefs[:notify_sso_pri] if !prefs[:notify_sso_pri].nil?
        @UserPreference.notify_sso_bkp = prefs[:notify_sso_bkp] if !prefs[:notify_sso_bkp].nil?
        @UserPreference.notify_2fa_pri = prefs[:notify_2fa_pri] if !prefs[:notify_2fa_pri].nil?
        @UserPreference.notify_2fa_bkp = prefs[:notify_2fa_bkp] if !prefs[:notify_2fa_bkp].nil?
        @UserPreference.notify_admin_pri = prefs[:notify_admin_pri] if !prefs[:notify_admin_pri].nil?
        @UserPreference.notify_admin_bkp = prefs[:notify_admin_bkp] if !prefs[:notify_admin_bkp].nil?

        # Add rest of settings to prevent validation errors
        @UserPreference.default_sort_order = @UserPreference.default_sort_order
        @UserPreference.default_sort_col = @UserPreference.default_sort_col
        @UserPreference.search_results = @UserPreference.search_results.to_s
        @UserPreference.theme = @UserPreference.theme
        @UserPreference.date_format = @UserPreference.date_format
        @UserPreference.use_24hr_time = @UserPreference.use_24hr_time.to_s
        @UserPreference.tz_offset = @UserPreference.tz_offset
          # I have to add .to_s to search_results and use_24hr_time because
          # they use _before_type_cast to validate, which would cause them
          # to throw errors since true != 'true', 10 != '10', etc.

        respond_to do |format|
          if @UserPreference&.save
            flash[:success] = "Successfully updated settings."
            format.html { redirect_to account_notifications_path }
            format.json { render :account_notifications, status: :ok, location: @UserPreference }
          else
            failure_message = "Failed to update settings, see "
            if @UserPreference.errors.count > 1
              failure_message << "errors"
            else
              failure_message << "error"
            end
            failure_message << " below."
            flash[:error] = failure_message
            format.html { render 'account_notification_settings' }
            format.json { render json: @UserPreference.errors, status: :unprocessable_entity }
          end
        end
      else # prefs is nil
        flash[:error] = "Error: blank form submitted."
        logger.warn "User with ID #{user.id} appears to have tampered with the notification settings form. prefs was nil, so the user probably deleted every option on the page."
        redirect_to account_notifications_path
      end
    end
  end

  def create
    @preference = Preferences.create(preference_params)
  end

  def edit
    @preference = Preferences.find(params[:user])
  end

  def index
    @preference = Preferences.where(:user => Current.user.id).first
  end

  def update
    @preference = Preferences.where(:user => Current.user.id)
    @preference.update_attributes!(preference_params)
  end

  def destroy
    @preference = Preferences.find(params[:user])
    @preference.destroy
  end

  private

  def preference_params
    params.require(:preference).permit(
      :user_id,
      :tz_offset,
      :dst_enabled,
      :autodetect_tz,
      :use_24hr_time,
      :date_format,
      :theme,
      :notify_pwchange_pri,
      :notify_pwchange_bkp,
      :notify_pwreset_pri,
      :notify_pwchange_bkp,
      :notify_sso_pri,
      :notify_sso_bkp,
      :notify_2fa_pri,
      :notify_2fa_bkp,
      :notify_admin_pri,
      :notify_admin_bkp
    )
  end
end

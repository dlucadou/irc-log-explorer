class Admin::BansController < ApplicationController
  after_action :clear_flashes
  before_action :authenticate_user!
  before_action :admin_tab_badge_data
  before_action :set_ban, only: [:show, :edit, :update, :destroy]
  before_action :set_user, only: [:show, :new, :edit, :create, :update]
  before_action :verify_permissions

  def index
  end

  def new
    @admin_user_profile_page = true
      # current_page? cannot properly target this page, so I have to use
      # this variable instead.
    @prefs = ApplicationHelper.common_prefs
    @prefs[:max_internal_reason_length] = Ban.max_internal_reason_length

    if params[:type] == 'adjust'
      last_ban = @user.bans.last
      if last_ban.permanent_ban
        @ban = Ban.new(internal_reason: last_ban.internal_reason, external_reason: last_ban.external_reason, ban_end_time: last_ban.ban_end_time)
        # If I did not have this special case, it would cause an ArgumentError since you cannot convert null in datetime_to_str
      else
        @ban = Ban.new(internal_reason: last_ban.internal_reason, external_reason: last_ban.external_reason, ban_end_time: ApplicationHelper.datetime_to_str(last_ban.ban_end_time, show_offset: false))
      end
      @prefs[:revise] = @user.is_banned?
        # I check if the user is banned to prevent incorrect text being
        # displayed if the user is unbanned before the page loads
      @prefs[:unban] = false
    else
      @ban = Ban.new
      @prefs[:unban] = @user.is_banned?
    end
  end

  def create
    @admin_user_profile_page = true
      # current_page? cannot properly target this page, so I have to use
      # this variable instead.
    @prefs = ApplicationHelper.common_prefs
    @prefs[:max_internal_reason_length] = Ban.max_internal_reason_length

    if params[:type] == 'adjust'
      @prefs[:revise] = @user.is_banned?
        # I check if the user is banned to prevent incorrect text being
        # displayed if the user is unbanned before the page loads
      @prefs[:unban] = false
    else
      @prefs[:unban] = @user.is_banned?
    end

    # Convert DateTime to String
    if ban_params[:ban_end_time] && ban_params[:ban_end_time].length > 0
      #params[:ban][:ban_end_time] = DateTime.strptime(ApplicationHelper.datetime_from_str(ban_params[:ban_end_time]).to_s, '%Q')
      params[:ban][:ban_end_time] = ApplicationHelper.datetime_from_str(ban_params[:ban_end_time])
    end
    # Set some fields if ban_reversal (if a user does fill these in, they
    # tampered with the form and it is safe to overwrite them)
    if ban_params[:ban_reversal] && ban_params[:ban_reversal] == 'true'
      params[:ban][:banned_until] = nil
      params[:ban][:permanently_banned] = false
    end

    # Set banned_user field so that if the user deletes their account, I still have the banned user's ID recorded
    params[:ban][:banned_user] = @user.id

    @ban = Ban.new(ban_params)
    @ban.user_id = @user.id
    @ban.banning_user = current_user.id

    # Check for bad_adjustment param, which indicates the user is changing the
    # ban length of the most recent ban (the render logic below ensures this
    # is param is not lost, which would mean the form would adjust to the
    # unban state, which is very different from the adjust/ban form)
    if params[:ban][:ban_adjustment]
      adjustment = true
    end

    respond_to do |format|
      if @ban.save
        format.html { redirect_to admin_manage_user_path(@user), notice: 'Ban instantiated.' }
        format.json { render :show, status: :created, location: @ban }
      else
        if adjustment
          format.html { render :new, params[:type] => 'adjust' }
        else
          format.html { render :new }
        end
        format.json { render json: @ban.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  end

  def update
  end

  def destroy
  end

  private

  def ban_params
    params.require(:ban).permit(
      :user_id,
      :banned_user,
      :banning_user,
      :ban_end_time,
      :permanent_ban,
      :ban_reversal,
      :internal_reason,
      :external_reason
    )
  end

  def verify_permissions
    if permissions_list[action_name.to_sym]&.index(current_user.role.to_sym).nil?
      # action_name is a Rails method for the controller method name
      # ( https://stackoverflow.com/a/4274222 )
      # Safe navigation operator (&) only calls if not nil.
      flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
      redirect_to root_path #, :status => :bad_request
      # I cannot return HTTP 403 with content, so the user gets a page that
      # says "You are being redirected." with a link to the root page.
      # Returning 200 is not best practice here but it looks best to the
      # user who never has to see an unformatted redirect page.
    end
  end

  def permissions_list
    {
      :index => [:admin],
      :new => [:admin],
      :create => [:admin],
      :show => [:admin],
      :delete => [:admin],
      :update => [:admin]
    }
  end

  def set_ban
    @ban = Ban.find(params[:id])
  end

  def set_user
    @user = User.find_by_id(params[:user])
  end
end

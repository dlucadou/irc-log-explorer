function binToStr(bin) {
  //console.log(bin);
  return btoa(new Uint8Array(bin).reduce(
    (s, byte) => s + String.fromCharCode(byte), ''
  ));
}

function strToBin(str) {
  //console.log(str);
  return Uint8Array.from(atob(str), c => c.charCodeAt(0));
}

function callback(url, body) {
  fetch(url, {
    method: "POST",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "X-CSRF-Token": document.querySelector('meta[name="csrf-token"]').getAttribute("content")
    },
    credentials: 'same-origin'
  }).then(function() {
    var redirectUrl;
    if (window.location.pathname === "/account/auth/2fa/u2f/prompt") {
      redirectUrl = "/";
    } else if (window.location.pathname === "/account/security/2fa/totp/u2f") {
      redirectUrl = "/account/security";
    } else {
      redirectUrl = "/account/security/2fa/u2f/keys";
    }
    
    window.location.replace(redirectUrl);
  });
}

function create(callbackUrl, credentialOptions) {
  navigator.credentials.create({ "publicKey": credentialOptions }).then(function(attestation) {
    callback(callbackUrl, {
      id: attestation.id,
      response: {
        clientDataJSON: binToStr(attestation.response.clientDataJSON),
        attestationObject: binToStr(attestation.response.attestationObject)
      }
    });
  }).catch(function(error) {
    //console.log(error);
    callback(callbackUrl, {
      error: error
    });
  });

  //console.log("Creating new public key credential...");
}

function get(credentialOptions) {
  navigator.credentials.get({ "publicKey": credentialOptions }).then(function(credential) {
    var assertionResponse = credential.response;
    var callbackUrl;
    callbackUrl = window.location.pathname;
    
    callback(callbackUrl, {
      id: binToStr(credential.rawId),
      response: {
        clientDataJSON: binToStr(assertionResponse.clientDataJSON),
        signature: binToStr(assertionResponse.signature),
        userHandle: binToStr(assertionResponse.userHandle),
        authenticatorData: binToStr(assertionResponse.authenticatorData)
      }
    });
  }).catch(function(error) {
    //console.log(error);
  });

  //console.log("Getting public key credential...");
}

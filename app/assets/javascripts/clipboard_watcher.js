//$(document).ready(function(){
$(window).bind('turbolinks:load', function(){
  /* I use 'turbolinks:load' here because I need to wait for Turbolinks or
   * this does not work. I'm not entirely sure why since theoretically waiting
   * for document ready should ensure Turbolinks has done all it needs to,
   * and changing a value shouldn't break the ability to select an element
   * by its ID, but that's not how it works in practice. Without this, you
   * have to refresh the page to get the copy to clipboard working.
   * I had a similar issue with credentials.js partially working if you didn't
   * refresh the page that was also solved by waiting for Turbolinks.
   * https://stackoverflow.com/a/40698512
   */
  $('.clipboard-btn').tooltip({
    trigger: 'click',
    placement: 'bottom'
  });

  function setTooltip(btn, message) {
    $(btn).tooltip('show')
      .attr('data-original-title', message)
      .tooltip('show');
  }

  function hideTooltip(btn) {
    setTimeout(function() {
      $(btn).tooltip('hide');
    }, 1000);
  }
  
  var clipboard = new Clipboard('.clipboard-btn');
  clipboard.on('success', function(e) {
    setTooltip(e.trigger, 'Copied to clipboard');
    hideTooltip(e.trigger);
  });

  clipboard.on('error', function(e) {
    setTooltip(e.trigger, 'Failed to copy');
    hideTooltip(e.trigger);
  });
});
//document.addEventListener("DOMContentLoaded", function(event) {
$(document).on('turbolinks:load', function() {
  /* The reason for the jQuery on turbolinks:load is because without this
   * line, the user will have to refresh the page.
   * If the user does not refresh the page, the "Register Token" button
   * will post, but the WebAuthn prompt never appears. I have no idea why
   * this occurs, but I know it has something to do with TurboLinks.
   * I have tried various StackOverflow recommendations to disable TurboLinks
   * on this page, but that just results in no form being submitted at all.
   * I copied the on turbolinks:load statement from this discussion:
   * https://github.com/kossnocorp/jquery.turbolinks/issues/56#issuecomment-202256655
   * It appears as though this issue would be solved by jquery-turbolinks, IF
   * the gem was still maintained (it is incompatible with Rails 5 and the gem
   * is deprecated with no real replacement).
   * So, this is an unfortunate workaround to ensure the user doesn't have
   * to refresh the page to get a WebAuthn prompt when they hit "Register
   * Token".
   * I might reuse this snippet elsewhere, but I will do so only where I
   * absolutely have to, as it's not very obvious why it's needed.
   */
  var addCredential = document.querySelector("#add-credential");

  if (addCredential) {
    addCredential.addEventListener("ajax:success", function(event) {
      [data, status, xhr] = event.detail;
      var credentialOptions = data;

      credentialOptions["challenge"] = strToBin(credentialOptions["challenge"]);
      credentialOptions["user"]["id"] = strToBin(credentialOptions["user"]["id"]);
      credentialOptions["excludeCredentials"].forEach(function(credential) {
        credential["id"] = strToBin(credential["id"]);
      });
      credential_nickname = document.querySelector("#add-credential input[name='credential[nickname]']").value;
      callback_url = `?name=${credential_nickname}`;
      /* The submission path is provided by the form, callback_url does not
       * need to be defined.
       */

      create(encodeURI(callback_url), credentialOptions);
    });
  }
});

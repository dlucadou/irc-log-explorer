# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

accordionWatcher = ->
  $('.accordion-row-toggler').on 'click', (event) ->
    console.log($(this)[0].children[0]);
    if $(this).has('i')
      if $(this)[0].children[0].classList.contains('fa-chevron-circle-down')
        $(this)[0].children[0].className = $(this)[0].children[0].className.replace('fa-chevron-circle-down', 'fa-chevron-circle-up')
      else
        $(this)[0].children[0].className = $(this)[0].children[0].className.replace('fa-chevron-circle-up', 'fa-chevron-circle-down')

$(document).on('turbolinks:load', accordionWatcher)
# Prevent Turbolinks errors (hasn't happened yet, but just in case)
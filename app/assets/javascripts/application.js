// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require rails-ujs
//= require turbolinks
//= require popper
//= require bootstrap
//= require moment
//= require tempusdominus-bootstrap-4
//= require clipboard
//= require_tree .

/* I put this outside the turbolinks section so it will only print on refresh
 * rather than every time TurboLinks loads the page, as the console is not
 * cleared, causing it to become filled with this message otherwise.
 */
console.log("Hello there! I see you're looking at the console. Found a bug or just interested in how all this works? Feel free to check it out and contribute!\nhttps://gitlab.com/lucadou/irc-log-explorer/\nThis application is licensed under the GNU General Public License v3. A copy of this license is included with the project.\nCopyright © 2018-2020 Luna Lucadou");

$(document).on('turbolinks:load', function() {
  $('[data-toggle="popover"]').popover();
});

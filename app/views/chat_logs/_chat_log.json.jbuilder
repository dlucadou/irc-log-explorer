json.extract! chat_log, :id, :channel, :sender, :message, :date, :created_at, :updated_at
json.url chat_log_url(chat_log, format: :json)

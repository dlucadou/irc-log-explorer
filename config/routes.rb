Rails.application.routes.draw do
  devise_for :users, :controllers => { registrations: "registrations", omniauth_callbacks: "omniauth_callbacks", confirmations: "confirmations", sessions: "sessions" }, :path => "account"

  devise_scope :user do
    # Alias routes for sign in, up, and out
    get 'signin', to: redirect('account/sign_in')
    get 'sign_in', to: redirect('account/sign_in')
    get 'login', to: redirect('account/sign_in')
    get 'signup', to: redirect('account/sign_up')
    get 'sign_up', to: redirect('account/sign_up')
    get 'register', to: redirect('account/sign_up')
    get 'signout', to: redirect('account/sign_out')
    get 'sign_out', to: redirect('account/sign_out')
    get 'logout', to: redirect('account/sign_out')
    get 'account/sign_out', to: 'devise/sessions#destroy'
      # Fixes bug where users with JS disabled couldn't log out, without
      # needing to create a form to post to the sessions#destroy method.
    # Terms of Service/EULA
    match 'policy/tos' => 'registrations#accept_tos', via: [:get, :post]

    # Account information & settings
    get 'account', to: redirect('account/overview')
    get 'account/overview' => 'registrations#account_overview'
    #match 'account/common' => 'registrations#edit', via: [:get, :patch, :post] # normal settings (name, email, password, SSO, etc)
    get 'account/settings' => 'registrations#edit'
    get 'account/settings(.:id)' => 'registrations#edit', to: redirect('accounts/settings'), as: :user
      # Without this second route for accounts/settings, if the user leaves
      # password blank but types something in for confirm_password, returning
      # false from RegistrationsController.update_resources throws a
      # NoMethodError "undefined method 'user_url'". The second route fixes it.

    # Account security - managing TOTP & U2F 2FA
    match 'account/security' => 'registrations#account_security_settings', via: [:get, :post]
    match 'account/security/2fa/recovery_codes', to: 'registrations#account_recovery_codes', via: [:get]
    match 'account/security/2fa/totp', to: 'registrations#account_manage_totp', via: [:get, :post]
    match 'account/security/2fa/totp/regen', to: 'registrations#account_regen_totp', via: [:post]
    match 'account/security/2fa/totp/u2f', to: 'registrations#account_manage_totp_with_u2f', via: [:get, :post]
    scope 'account/security/2fa/u2f', as: 'account_security_2fa_u2f' do
      match 'keys', to: 'credentials#index', via: [:get]
      match 'add', to: 'credentials#create', via: [:get, :post]
      match ':token_id/edit', to: 'credentials#edit', via: [:get, :post], as: 'edit'
      match ':token_id/delete/totp', to: 'credentials#delete_with_totp', via: [:get, :post], as: 'delete_with_totp'
      match ':token_id/delete/u2f', to: 'credentials#delete_with_u2f', via: [:get, :post], as: 'delete_with_u2f'
    end
    # Authenticating with TOTP & U2F 2FA
    match 'account/auth/2fa/totp', to: 'sessions#two_factor_auth_totp', via: [:get, :post]
    scope 'account/auth/2fa/u2f', as: 'account_auth_2fa_u2f' do
      match 'prompt', to: 'sessions#two_factor_auth_webauthn', via: [:get, :post]
    end
    # Account SSO
    match 'account/sign_up/sso' => 'registrations#finish_sso', via: [:get, :post, :delete], :as => :finish_sso
    delete 'account/auth(/:provider)', to: 'omniauth_callbacks#destroy' # for removing SSO integrations from an account

    # Omniauth provider setup
    match '/account/auth/:provider/setup' => 'sessions#setup_omniauth', via: [:get]
  end

  # Account preferences (not managed by Devise)
  match 'account/ui(/:user)' => 'preferences#account_display_settings', via: [:get, :put], :as => :account_ui
  match 'account/notifications' => 'preferences#account_notification_settings', via: [:get, :post, :put]

  # Reports
  resources :reports, except: [:edit, :resolve], :path => 'account/reports'
  match 'account/reports/:id/edit', to: 'reports#edit', via: [:get, :post, :patch], :as => :edit_report
  match 'account/reports/:id/resolve', to: 'reports#resolve', via: [:get, :post, :patch], :as => :resolve_report

  # Admin console
  namespace :admin do
    get '', to: 'dashboard#index'
    get 'reports', to: 'dashboard#reports'
    get 'searches', to: 'dashboard#searches'
    get 'syslog', to: 'dashboard#system_log'
    get 'sysprefs', to: 'dashboard#system_settings'
    get 'users', to: 'dashboard#user_management'
    match 'users/(:id)', to: 'dashboard#user_profile', via: [:get, :post, :delete], as: :manage_user
    resources :eulas, only: [:new, :create, :index, :show], :path => 'tos'
    # The reason I use "resources" above is because with "resource", the :index route does not get created
    # But for bans, if I did "resources", that causes problems when trying to create new bans for some reason
    # TODO: figure out why that happens and try to make it use "resources" like eulas above
    resource :bans, only: [:new, :create, :index], :path => 'users/:user/bans', as: :user_ban
    resource :bans, only: [:show, :update, :delete], :path => 'users/:user/bans/:id', as: :user_ban
    resources :conversations, only: [:index, :show, :new, :create, :destroy], :path => 'messages' do
      resources :messages, only: [:new, :create, :edit, :destroy, :update], :path => 'replies'
      match 'replies/:id/hide', to: 'messages#hide', via: [:post], as: :hide
      match 'replies/:id/mark_unread', to: 'messages#mark_unread', via: [:post], as: :mark_unread
      match 'lock', to: 'conversations#lock', via: [:get], as: :lock
    end
  end

  # User messages
  resources :conversations, only: [:index, :show, :new, :create, :destroy], :path => 'account/messages' do
    resources :messages, only: [:new, :create, :edit, :destroy, :update], :path => 'replies'
    match 'replies/:id/hide', to: 'messages#hide', via: [:post], as: :hide
    match 'replies/:id/mark_unread', to: 'messages#mark_unread', via: [:post], as: :mark_unread
  end

  # Chat logs
  chat_log_route_exceptions = []
  unless Rails.configuration.application[:chat_logs][:allow_creation]
    chat_log_route_exceptions = [:new, :create]
  end
  resources :chat_logs, except: chat_log_route_exceptions do
    collection do
      get 'advanced_search', to: 'chat_logs#advanced_search'
      post 'advanced_search', to: 'chat_logs#advanced_search'
    end
  end

  # Static pages
  get 'help', to: 'pages#help_index'
  get 'help/markdown', to: 'pages#help_markdown'
  get 'help/regex', to: 'pages#help_regex'

  root to: 'pages#index'

end

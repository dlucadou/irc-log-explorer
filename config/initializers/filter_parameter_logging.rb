# Be sure to restart your server when you modify this file.

# Configure sensitive parameters which will be filtered from the log file.
Rails.application.config.filter_parameters += [:password, :ds_oauth_token,
  :fb_oauth_token, :gh_oauth_token, :gl_oauth_token, :goog_oauth_token,
  :jtv_oauth_token, :tw_oauth_token, :otp_attempt]

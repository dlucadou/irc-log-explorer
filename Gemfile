source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Ruby version
ruby '~> 2.7.0'

# Rake version
#gem 'rake', '~> 12.3.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.0'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 4.3.0'
# Use SCSS for stylesheets
gem 'sassc-rails', '~> 2.1.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Nokogiri
gem 'nokogiri', '~> 1.10.1'

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 5.0.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# RedCarpet for Markdown rendering in Conversations/Messages
gem 'redcarpet', '~>3.5.0'

# Use Bootstrap & Jquery
gem 'bootstrap', '~> 4.4.1'
gem 'bootstrap4-datetime-picker-rails', '~>0.3.1'
gem 'momentjs-rails', '~>2.20.1'
gem 'sprockets-rails', '~> 3.2.1'
gem 'jquery-rails'
gem 'mini_racer', '~>0.3.0'

# Pagination
gem 'kaminari', '~>1.2.0'

# Font Awesome
gem 'font-awesome-sass'

# Authentication
gem 'devise', '~>4.7.1'
gem 'omniauth'
gem 'omniauth-discord'
gem 'omniauth-facebook'
gem 'omniauth-gitlab'
gem 'omniauth-github'
gem 'omniauth-google-oauth2'
gem 'omniauth-oauth2'
gem 'omniauth-twitter'
gem 'omniauth-twitch'

# 2FA - TOTP and U2F
gem 'devise-two-factor', '~>3.1.0'
gem 'rqrcode-rails3', '~>0.1.7'
gem 'mini_magick', '~>4.10.0'
gem 'attr_encrypted', '~> 3.1.0'
gem 'webauthn', '~> 2.2.0'
gem 'rollbar', '~> 2.16'

# Clipboard.JS
gem 'clipboard-rails', '~>1.7.1'

group :development, :test do
  # Rspec for unit tests
  gem 'rspec-rails', '~>4.0.0'
  gem 'rspec-core', '~>3.9.0'
  gem 'guard-rspec', '~>4.7.3'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'pry-byebug'
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 3.18'
  gem 'webdrivers', '~> 4.3.0'
  gem 'headless'
  gem 'selenium-webdriver'
  gem 'factory_bot_rails', '~>5.2.0', :require => false
  # The reason I have this not required is because if I remove it, whenever
  # rake db commands execute, it executes factories. I don't know why it
  # runs factory code if it's not running a test, but in any case, it causes
  # all rake db commands to fail - migrate, drop, create, etc.
  # https://stackoverflow.com/a/12425729
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.7.0'
  gem 'listen', '>= 3.2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # For the RailsPanel Chrome extension
  gem 'meta_request'

  # Favicon
  gem 'rails_real_favicon'
  # Only in development because running "rails g favicon" pregenerates the
  # assets - you only need to re-run it when changing the icon
end

group :test do
  # For disabling transaction grouping in a few specific tests
  gem 'database_cleaner-active_record'
  gem 'database_cleaner-sequel'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

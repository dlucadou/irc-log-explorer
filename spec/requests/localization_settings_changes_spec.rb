require 'rails_helper'
include AuthenticationHelpers
include SettingsHelpers

RSpec.describe "LocalizationSettingsChanges", type: :request do
  before :each do
    name = 'Test User 1'
    email = 'testuser@domain.test'
    password = 'CorrectHorseBatteryStaple'
    sign_up_with(name, email, password, password)
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    visit account_ui_path
  end

  context "User can" do
    it "set date format" do
      # Test with "YYYY-DD-MM"
      set_ui_settings_with(date_format: "YYYY-DD-MM")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_date_format', selected: 'YYYY-DD-MM')
      visit chat_logs_path
      expect(page).to have_content("2017-13-11 08:48:08") # YYYY-DD-MM
      expect(page).to_not have_content("2017-11-13 08:48:08") # YYYY-MM-DD
        # The date is from the first result with default column & direction

      # Test with "YYYY-MM-DD" (default)
      set_ui_settings_with(date_format: "YYYY-MM-DD")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_date_format', selected: 'YYYY-MM-DD')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08") # YYYY-MM-DD
      expect(page).to_not have_content("2017-13-11 08:48:08") # YYYY-DD-MM
        # The .to_not will use the previous format, e.g. the section above
        # set the date format to "YYYY-DD-MM", so we expect it to not be
        # in that format anymore.

      # Test with "MM-DD-YYYY"
      set_ui_settings_with(date_format: "MM-DD-YYYY")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_date_format', selected: 'MM-DD-YYYY')
      visit chat_logs_path
      expect(page).to have_content("11-13-2017 08:48:08") # MM-DD-YYYY
      expect(page).to_not have_content("2017-11-13 08:48:08") # YYYY-MM-DD

      # Test with "DD-MM-YYYY"
      set_ui_settings_with(date_format: "DD-MM-YYYY")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_date_format', selected: 'DD-MM-YYYY')
      visit chat_logs_path
      expect(page).to have_content("13-11-2017 08:48:08") # DD-MM-YYYY
      expect(page).to_not have_content("11-13-2017 08:48:08") # MM-DD-YYYY

      # Test with "MM-YYYY-DD"
      set_ui_settings_with(date_format: "MM-YYYY-DD")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_date_format', selected: 'MM-YYYY-DD')
      visit chat_logs_path
      expect(page).to have_content("11-2017-13 08:48:08") # MM-YYYY-DD
      expect(page).to_not have_content("13-11-2017 08:48:08") # DD-YYYY-MM

      # Test with "DD-YYYY-MM"
      set_ui_settings_with(date_format: "DD-YYYY-MM")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_date_format', selected: 'DD-YYYY-MM')
      visit chat_logs_path
      expect(page).to have_content("13-2017-11 08:48:08") # DD-YYYY-MM
      expect(page).to_not have_content("11-2017-13 08:48:08") # MM-YYYY-DD
    end
    it "change to and from 24 hour time" do
      # Default 24 hour time
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")
      expect(page).to_not have_content("2017-11-13 08:48:08 AM")

      # Change to 12 hour time
      # I set sort order to Ascending because the first results are all in
      # AM, which fails if you expect "6AM" and not "6"; you have to have a
      # PM time, so you can expect "6PM" and not expect "18".
      set_ui_settings_with(sort_order: "Ascending", use_24hr_time: "No")
      expect(page).to have_content("Successfully updated settings")
      expect(find_field("user_preference_use_24hr_time_false")).to be_checked
      expect(find_field("user_preference_use_24hr_time_true")).not_to be_checked

      visit chat_logs_path
      expect(page).to have_content("2017-06-17 06:36:49 PM")
      expect(page).to_not have_content("2017-06-17 18:36:49")
        # Time is from the first entry when sorting by time ascending

      # Change back to 24 hour time
      set_ui_settings_with(sort_order: "Ascending", use_24hr_time: "Yes")
      expect(page).to have_content("Successfully updated settings")
      expect(find_field("user_preference_use_24hr_time_true")).to be_checked
      expect(find_field("user_preference_use_24hr_time_false")).not_to be_checked

      visit chat_logs_path
      expect(page).to have_content("2017-06-17 18:36:49")
      expect(page).to_not have_content("2017-06-17 06:36:49 PM")
    end
    it "change time zone" do
      # Default UTC+00:00
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")

      # Test negative offsets
      set_ui_settings_with(time_zone: '-05:25')
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_field('Time Zone', with: '-05:25')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 03:23:08")
      expect(page).to_not have_content("2017-11-13 08:48:08")

      set_ui_settings_with(time_zone: '-23:59')
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_field('Time Zone', with: '-23:59')
      visit chat_logs_path
      expect(page).to have_content("2017-11-12 08:49:08")
      expect(page).to_not have_content("2017-11-13 03:23:08")

      # Test positive offsets
      set_ui_settings_with(time_zone: '+06:30')
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_field('Time Zone', with: '+06:30')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 15:18:08")
      expect(page).to_not have_content("2017-11-12 08:49:08")

      set_ui_settings_with(time_zone: '+23:59')
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_field('Time Zone', with: '+23:59')
      visit chat_logs_path
      expect(page).to have_content("2017-11-14 08:47:08")
      expect(page).to_not have_content("2017-11-13 15:18:08")

      # Test going back to UTC (but with -00:00 instead of +00:00)
      set_ui_settings_with(time_zone: '-00:00')
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_field('Time Zone', with: '-00:00')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")
      expect(page).to_not have_content("2017-11-14 08:47:08")

      # Test UTC (but with 00:00 instead of -00:00)
      set_ui_settings_with(time_zone: '00:00')
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_field('Time Zone', with: '+00:00')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")
    end
  end

  context "User cannot" do
    it "change timezone offset to an invalid offset" do
      set_ui_settings_with(time_zone: '-01:30f')
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Timezone offset is invalid (must be between -23:59 and +23:59)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Time Zone', with: '+00:00')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")
    end
    it "change timezone offset to an offset with hour offset too high" do
      set_ui_settings_with(time_zone: '+31:30')
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Timezone offset is invalid (must be between -23:59 and +23:59)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Time Zone', with: '+00:00')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")
    end
    it "change timezone offset to an offset with hour offset too low" do
      set_ui_settings_with(time_zone: '-25:10')
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Timezone offset is invalid (must be between -23:59 and +23:59)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Time Zone', with: '+00:00')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")
    end
    it "change timezone offset to an offset with no +/- symbol" do
      set_ui_settings_with(time_zone: '05:20')
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Timezone offset is invalid (must be between -23:59 and +23:59)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Time Zone', with: '+00:00')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")
    end
    it "change timezone offset to an offset with minute offset to high" do
      set_ui_settings_with(time_zone: '-05:70')
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Timezone offset is invalid (must be between -23:59 and +23:59)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Time Zone', with: '+00:00')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")
    end
    it "change timezone offset to a blank offset" do
      set_ui_settings_with(time_zone: '')
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Timezone offset is invalid (must be between -23:59 and +23:59)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Time Zone', with: '+00:00')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")
    end
  end
end

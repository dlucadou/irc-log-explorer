require 'rails_helper'
include AuthenticationHelpers
include SettingsHelpers

RSpec.describe "TosAcceptances", type: :request do
  describe "A logged in user" do
    before :each do
      name = 'Test User 1'
      @email = 'testuser@domain.test'
      @password = 'CorrectHorseBatteryStaple'
      sign_up_with(name, @email, @password, @password)
      expect(page).to have_content("Welcome! You have signed up successfully.")
      expect(page).to have_content("Terms of Service")
      click_on "Continue"
      expect(page).to have_content("Thank you for accepting the terms of service!")
    end

    it "will remained logged in if they agree to the TOS" do
      visit policy_tos_path
      expect(page).to have_content("Terms of Service")
      expect(page).to have_button("Continue")
      within('.radio-buttons-tos') do
        choose "I agree to the terms and conditions above"
      end
      within('.actions') do
        click_on "Continue"
      end
      expect(page).to have_content("Thank you for accepting the terms of service!")
      expect(page).to have_content("Sign out") # Check if still signed in
    end
    it "will be logged out if they do not agree to the TOS but wish to keep their account" do
      visit policy_tos_path
      expect(page).to have_content("Terms of Service")
      expect(page).to have_button("Continue")
      within('.radio-buttons-tos') do
        choose "I do not agree to the terms and conditions above, but I wish to keep my account"
      end
      within('.actions') do
        click_on "Continue"
      end
      expect(page).to have_content("You have been signed out. To use the site, sign in and accept the terms of service.")
      expect(page).to have_content("Sign in") # Check if signed out
      expect(page).to have_content("Register")
      sign_in_with(@email, @password, accept_tos: false) # Make sure account hasn't been deleted
      expect(page).to have_content("You must accept the Terms of Service before continuing.")
      expect(page).to have_content("Terms of Service") # Accept TOS
      click_on "Continue"
      expect(page).to have_content("Thank you for accepting the terms of service!")
      expect(page).to have_content("Sign out") # Check if signed in
    end
    it "will have their account deleted if they do not agree to the TOS and do not wish to keep their account" do
      visit policy_tos_path
      expect(page).to have_content("Terms of Service")
      expect(page).to have_button("Continue")
      within('.radio-buttons-tos') do
        choose "I do not agree to the terms and conditions above, delete my account"
      end
      within('.actions') do
        click_on "Continue"
      end
      expect(current_path).to eq(root_path)
      expect(page).to have_content("Account deleted. Should you want to use the site again, you will have to create a new account.")
      expect(page).to have_content("Sign in")
      expect(page).to have_content("Register")
      sign_in_with(@email, @password, expecting_success: false) # Check if account was deleted
      expect(page).to have_content("Invalid Email or password.")
      visit policy_tos_path
      expect(page).to have_content("Terms of Service")
      expect(page).to_not have_button("Continue")
        # Make sure account is not partially signed in
    end
  end

  describe "A logged out user" do
    it "will not see a form" do
      visit policy_tos_path
      expect(page).to have_content("Terms of Service")
      expect(page).to_not have_button("Continue")
    end
  end
end

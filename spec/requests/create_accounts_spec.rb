require 'rails_helper'
include AuthenticationHelpers

RSpec.describe "CreateAccounts", type: :request do
  it "creates an account with correct input" do
    sign_up_with('Test user 1', 'testuser1@domain.test', 'CorrectHorseBatteryStaple', 'CorrectHorseBatteryStaple')
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    expect(page).to have_content("Sign out")
    expect(current_path).to eq(root_path)
    sign_out_with
  end

  it "fails to create account with no nickname" do
    sign_up_with('', 'testuser1@domain.test', 'CorrectHorseBatteryStaple', 'CorrectHorseBatteryStaple')
    expect(page).to have_content("1 error prohibited this user from being saved:")
    expect(page).to have_content("Name cannot be left blank")
  end

  it "fails to create account with no nickname or password" do
    sign_up_with('', 'testuser1@domain.test', '', '')
    expect(page).to have_content("2 errors prohibited this user from being saved:")
    expect(page).to have_content("Name cannot be left blank")
    expect(page).to have_content("Password is too short (minimum is 16 characters)")
  end

  it "fails to create account with no nickname, email address, or password" do
    sign_up_with('', '', '', '')
    expect(page).to have_content("3 errors prohibited this user from being saved:")
    expect(page).to have_content("Name cannot be left blank")
    expect(page).to have_content("Email cannot be left blank")
    expect(page).to have_content("Password is too short (minimum is 16 characters)")
  end

  it "fails to create account with no nickname and invalid email address" do
    sign_up_with('', 'a@a.a', 'CorrectHorseBatteryStaple', 'CorrectHorseBatteryStaple')
    expect(page).to have_content("2 errors prohibited this user from being saved:")
    expect(page).to have_content("Name cannot be left blank")
    expect(page).to have_content("Email must be a valid address")
  end

  it "fails to create account with no nickname, no password, and invalid email address" do
    sign_up_with('', 'a@a.a', '', '')
    expect(page).to have_content("3 errors prohibited this user from being saved:")
    expect(page).to have_content("Name cannot be left blank")
    expect(page).to have_content("Email must be a valid address")
    expect(page).to have_content("Password is too short (minimum is 16 characters)")
  end

  it "fails to create account with already registered email address" do
    sign_up_with('Test User 1', 'testuser1@domain.test', 'CorrectHorseBatteryStaple', 'CorrectHorseBatteryStaple')
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    expect(current_path).to eq(root_path)
    sign_out_with

    expect(page).to have_content("Register")
    click_on("navbar-signup-link")
    sign_up_with('Test User 2', 'testuser1@domain.test', 'CorrectHorseBatteryStaple', 'CorrectHorseBatteryStaple', navigate_to_page: false)
    expect(page).to have_content("1 error prohibited this user from being saved:")
    expect(page).to have_content("Email is already in use")
  end

  it "fails to create account with no email address" do
    sign_up_with('Test User', '', 'CorrectHorseBatteryStaple', 'CorrectHorseBatteryStaple')
    expect(page).to have_content("1 error prohibited this user from being saved:")
    expect(page).to have_content("Email cannot be left blank")
  end

  it "fails to create account with invalid email address" do
    sign_up_with('Test User', 'a@a.a', 'CorrectHorseBatteryStaple', 'CorrectHorseBatteryStaple')
    expect(page).to have_content("1 error prohibited this user from being saved:")
    expect(page).to have_content("Email must be a valid address")
  end

  it "fails to create account with no passwords" do
    sign_up_with('Test User', 'testuser1@domain.test', '', '')
    expect(page).to have_content("1 error prohibited this user from being saved:")
    expect(page).to have_content("Password is too short (minimum is 16 characters)")
  end

  it "fails to create account with long enough but non-matching passwords" do
    sign_up_with('Test User', 'testuser1@domain.test', 'CorrectHorseBatteryStaple', 'CorrectHorseBattery')
    expect(page).to have_content("1 error prohibited this user from being saved:")
    expect(page).to have_content("Password confirmation doesn't match Password")
  end

  it "fails to create account with one passwords that is long enough and one that is too short" do
    sign_up_with('Test User', 'testuser1@domain.test', 'CorrectHorseBatteryStaple', 'Correct')
    expect(page).to have_content("1 error prohibited this user from being saved:")
    expect(page).to have_content("Password confirmation doesn't match Password")
  end

  it "fails to create account with matching passwords that are too short" do
    sign_up_with('Test User', 'testuser1@domain.test', 'Correct', 'Correct')
    expect(page).to have_content("1 error prohibited this user from being saved:")
    expect(page).to have_content("Password is too short (minimum is 16 characters)")
  end
end

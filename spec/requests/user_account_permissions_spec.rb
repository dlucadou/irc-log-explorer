require 'rails_helper'
include AuthenticationHelpers

RSpec.describe "UserAccountPermissions", type: :request do
  before :each do
    name = 'Test User 1'
    @email = 'testuser@domain.test'
    @password = 'CorrectHorseBatteryStaple'
    sign_up_with(name, @email, @password, @password)
    sign_in_with(@email, @password)
    @user = User.where(email: @email).first
    visit account_security_path
    expect(page).to have_content("Enable TOTP")
  end

  describe "User can" do
    it "browse chat logs"
    it "view chat logs"
    it "search for chat logs"
    it "report chat logs"
  end

  describe "User cannot" do
    it "edit chat logs"
    it "delete chat logs"
    # note to self: make an admin permissions spec file as well
    # note: on the view message details page, i should not see edit/delete msg
  end
end

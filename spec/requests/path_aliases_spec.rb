require 'rails_helper'

RSpec.describe "PathAliases", type: :request do
  describe "sign in aliases" do
    # Each should redirect to /account/sign_in
    it "redirects /signin" do
      get '/signin'
      expect(response).to have_http_status(301)
      expect(response).to redirect_to new_user_session_path
    end
    it "redirects /sign_in" do
      get '/sign_in'
      expect(response).to have_http_status(301)
      expect(response).to redirect_to new_user_session_path
    end
    it "redirects /login" do
      get '/login'
      expect(response).to have_http_status(301)
      expect(response).to redirect_to new_user_session_path
    end
  end

  describe "sign up aliases" do
    # Each should redirect to /account/sign_up
    it "redirects /signup" do
      get '/signup'
      expect(response).to have_http_status(301)
      expect(response).to redirect_to new_user_registration_path
    end
    it "redirects /sign_up" do
      get '/sign_up'
      expect(response).to have_http_status(301)
      expect(response).to redirect_to new_user_registration_path
    end
    it "redirects /register" do
      get '/register'
      expect(response).to have_http_status(301)
      expect(response).to redirect_to new_user_registration_path
    end
  end

  describe "sign out aliases" do
    # Each should redirect to /account/sign_out
    it "redirects /signout" do
      get '/signout'
      expect(response).to have_http_status(301)
      expect(response).to redirect_to destroy_user_session_path
    end
    it "redirects /sign_out" do
      get '/sign_out'
      expect(response).to have_http_status(301)
      expect(response).to redirect_to destroy_user_session_path
    end
    it "redirects /logout" do
      get '/logout'
      expect(response).to have_http_status(301)
      expect(response).to redirect_to destroy_user_session_path
    end
  end
end

require 'rails_helper'
include AuthenticationHelpers
include SettingsHelpers

RSpec.describe "AppearanceSettingsChanges", type: :request do
  before :each do
    name = 'Test User 1'
    email = 'testuser@domain.test'
    password = 'CorrectHorseBatteryStaple'
    sign_up_with(name, email, password, password)
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    visit account_ui_path
  end

  describe "User can" do
    it "change default sort column" do
      # Test with Channel column
      set_ui_settings_with(sort_col: "Channel")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_default_sort_col', selected: 'Channel')
      visit chat_logs_path
      expect(page).to have_content("#stereotonetim")
      expect(page).to_not have_content("#monotonetim")
        # When sorted by channel descending, only messages from
        # #stereotonetim will be present

      # Test with Sender column
      set_ui_settings_with(sort_col: "Sender")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_default_sort_col', selected: 'Sender')
      visit chat_logs_path
      expect(page).to have_content("wcorbe")
      expect(page).to_not have_content("raynor_ex")
        # When sorted by sender descending, only messages from these 2
        # users will be present

      # Test with Time column
      set_ui_settings_with(sort_col: "Time")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_default_sort_col', selected: 'Time')
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")
        # First record when sorted by time descending
      expect(page).to_not have_content("2017-06-17 18:36:49")
        # Last record when sorted by time descending
    end
    it "change default sort order" do
      visit chat_logs_path
      expect(page).to have_content("2017-11-13 08:48:08")
        # First record when sorted by time descending
      expect(page).to_not have_content("2017-06-17 18:36:49")
        # Last record when sorted by time descending

      set_ui_settings_with(sort_order: "Ascending")
      expect(page).to have_content("Successfully updated settings")
      expect(find_field("user_preference_default_sort_order_asc")).to be_checked
      expect(find_field("user_preference_default_sort_order_desc")).not_to be_checked

      visit chat_logs_path
      expect(page).to have_content("2017-06-17 18:36:49")
        # First record when sorted by time ascending
      expect(page).to_not have_content("2017-11-13 06:22:38")
        # Last record when sorted by time ascending
    end
    it "change search results per page" do
      visit chat_logs_path
      expect(page).to have_selector('table tr', :count => 11)
        # 10 rows + 1 row for the column labels

      set_ui_settings_with(results_per_page: 20)
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_field('Search Results per Page', with: 20)
      visit chat_logs_path
      expect(page).to have_selector('table tr', :count => 21)
        # 20 rows + 1 row for the column labels

      set_ui_settings_with(results_per_page: 120)
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_field('Search Results per Page', with: 120)
      visit chat_logs_path
      expect(page).to have_selector('table tr', :count => 121)
        # 250 rows + 1 row for the column labels
    end
    it "change the theme" do
      # This doesn't test to ensure the actual CSS values are that of their
      # respective themes, but Nokogiri does not seem to be able to query
      # individual CSS attributes outside an element's inline CSS, e.g.
      # <p class="myclass" style="color: #000000;">

      # Test with Dark theme
      set_ui_settings_with(theme: "Dark")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_theme', selected: 'Dark')

      # Test with Midnight (Dark) theme
      set_ui_settings_with(theme: "Midnight (Dark)")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_theme', selected: 'Midnight (Dark)')

      # Test with Minty (Light) theme
      set_ui_settings_with(theme: "Minty (Light)")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_theme', selected: 'Minty (Light)')

      # Test with Solarized (Dark) theme
      set_ui_settings_with(theme: "Solarized (Dark)")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_theme', selected: 'Solarized (Dark)')

      # Test with Ubuntu (Light) theme
      set_ui_settings_with(theme: "Ubuntu (Light)")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_theme', selected: 'Ubuntu (Light)')

      # Test with Light (Default) theme
      set_ui_settings_with(theme: "Light (Default)")
      expect(page).to have_content("Successfully updated settings")
      expect(page).to have_select('user_preference_theme', selected: 'Light (Default)')
    end
  end

  describe "User cannot" do
    it "change search results per page to a negative value" do
      set_ui_settings_with(results_per_page: -5)
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Search results per page is invalid (must be a whole number between 10 and 1000)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Search Results per Page', with: 10)
    end
    it "change search results per page to 0" do
      set_ui_settings_with(results_per_page: 0)
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Search results per page is invalid (must be a whole number between 10 and 1000)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Search Results per Page', with: 10)
    end
    it "change search results per page to a value > 1000" do
      set_ui_settings_with(results_per_page: 10000)
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Search results per page is invalid (must be a whole number between 10 and 1000)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Search Results per Page', with: 10)
    end
    it "change search results per page to a decimal (non-integer) value" do
      set_ui_settings_with(results_per_page: 15.9)
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Search results per page is invalid (must be a whole number between 10 and 1000)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Search Results per Page', with: 10)
    end
    it "change search results per page to a string (non-integer) value" do
      set_ui_settings_with(results_per_page: "ten")
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Search results per page is invalid (must be a whole number between 10 and 1000)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Search Results per Page', with: 10)
    end
    it "change search results per page to a blank string" do
      set_ui_settings_with(results_per_page: '')
      expect(page).to have_content("Failed to update report, see error below.")
      expect(page).to have_content("1 error prohibited your settings from being saved:")
      expect(page).to have_content("Search results per page is invalid (must be a whole number between 10 and 1000)")
      expect(page).to have_content("Your settings have not been modified.")
      expect(page).to have_field('Search Results per Page', with: 10)
    end
  end
end

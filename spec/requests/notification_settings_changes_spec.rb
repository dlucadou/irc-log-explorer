require 'rails_helper'
include AuthenticationHelpers
include SettingsHelpers

RSpec.describe "NotificationSettingsChanges", type: :request do
  before :each do
    name = 'Test User 1'
    email = 'testuser@domain.test'
    password = 'CorrectHorseBatteryStaple'
    sign_up_with(name, email, password, password)
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    visit account_notifications_path
  end

  describe "Users with backup email" do
    before :each do
      name = 'Test User 1'
      email = 'testuser@domain.test'
      backup_email = 'testuser-backup@domain.test'
      password = 'CorrectHorseBatteryStaple'
      set_basic_settings_with(name, email, backup_email: backup_email, current_password: password)
      visit account_notifications_path
    end

    it "can disable password change notifications on primary email" do
      # set_notification_settings_with(pw_change_pri: true, pw_change_bkp: nil, pw_reset_pri: nil, pw_reset_bkp: nil, oauth_pri: true, oauth_bkp: nil, sec_2fa_pri: nil, sec_2fa_bkp: nil, admin_pri: nil, admin_bkp: nil)
      expect(page).to have_field('user_preference_notify_pwchange_pri', checked: true)
      set_notification_settings_with(pw_change_pri: false)
      expect(page).to have_field('user_preference_notify_pwchange_pri', checked: false)
    end
    it "can enable password change notifications on backup email" do
      expect(page).to have_field('user_preference_notify_pwchange_bkp', checked: false)
      set_notification_settings_with(pw_change_bkp: true)
      expect(page).to have_field('user_preference_notify_pwchange_bkp', checked: true)
    end
    it "can enable password reset notifications on backup email" do
      expect(page).to have_field('user_preference_notify_pwreset_bkp', checked: false)
      set_notification_settings_with(pw_reset_bkp: true)
      expect(page).to have_field('user_preference_notify_pwreset_bkp', checked: true)
    end
    it "can disable Oauth provider notifications on primary email" do
      expect(page).to have_field('user_preference_notify_sso_pri', checked: true)
      set_notification_settings_with(oauth_pri: false)
      expect(page).to have_field('user_preference_notify_sso_pri', checked: false)
    end
    it "can enable Oauth provider notifications on backup email" do
      expect(page).to have_field('user_preference_notify_sso_bkp', checked: false)
      set_notification_settings_with(oauth_bkp: true)
      expect(page).to have_field('user_preference_notify_sso_bkp', checked: true)
    end
    it "can enable 2FA method notifications on backup email" do
      expect(page).to have_field('user_preference_notify_2fa_bkp', checked: false)
      set_notification_settings_with(sec_2fa_bkp: true)
      expect(page).to have_field('user_preference_notify_2fa_bkp', checked: true)
    end
    it "can enable admin messages on backup email" do
      expect(page).to have_field('user_preference_notify_admin_bkp', checked: false)
      set_notification_settings_with(admin_bkp: true)
      expect(page).to have_field('user_preference_notify_admin_bkp', checked: true)
    end
  end

  describe "Users without backup email" do
    it "can disable password change notifications on primary email" do
      expect(page).to have_field('user_preference_notify_pwchange_pri', checked: true)
      set_notification_settings_with(pw_change_pri: false)
      expect(page).to have_field('user_preference_notify_pwchange_pri', checked: false)
    end
    it "can disable Oauth provider notifications on primary email" do
      expect(page).to have_field('user_preference_notify_sso_pri', checked: true)
      set_notification_settings_with(oauth_pri: false)
      expect(page).to have_field('user_preference_notify_sso_pri', checked: false)
    end
  end
end

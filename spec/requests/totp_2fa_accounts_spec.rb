require 'rails_helper'
include AuthenticationHelpers
include Account2FAHelpers

RSpec.describe "Totp2faAccounts", type: :request do
  before :each do
    name = 'Test User 1'
    @email = 'testuser@domain.test'
    @password = 'CorrectHorseBatteryStaple'
    sign_up_with(name, @email, @password, @password)
    sign_in_with(@email, @password)
    @user = User.where(email: @email).first
    visit account_security_path
    expect(page).to have_content("Enable TOTP")
  end

  describe "User can" do
    it "enable TOTP" do
      enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)
    end
    it "enable TOTP with some time drift" do
      # This test will ensure a slight time drift will not cause the TOTP code
      # to be invalid
      totp = enable_2fa(submit_form: false)
      current_code = totp.now

      # Wait for code to be invalidated
      while(totp.now == current_code)
        sleep(5)
      end

      # Submit form
      click_on "Enable TOTP"

      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)
    end
    it "login using TOTP" do
      # Enable TOTP
      totp = enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Wait 30 seconds to make sure the next code will be ready
      # Technically I could do <30 seconds, but then it's not a guarantee that
      # would work - it introduces a race condition
      sleep(30)

      # Sign back in
      switch_to_user(@email, @password, totp: totp)
      expect(page).to have_content("Signed in with One-Time Password. If you used a recovery code, make sure to generate new codes so you don't run out!")
    end
    it "disable TOTP using TOTP" do
      # Enable TOTP
      totp = enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Wait 30 seconds to make sure the next code will be ready
      sleep(30)

      # Disable TOTP
      disable_2fa(code: totp)
      expect(page).to have_content("TOTP disabled")
      expect(page).to have_content("Enable TOTP")
      expect(page).to have_content("Recovery codes not generated")
    end
    it "generate recovery codes after enabling TOTP" do
      # Enable TOTP
      enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Get recovery codes
      click_on "Get Recovery Codes"
      get_recovery_codes()

      # Verify recovery codes were generated
      click_on("Back to Account Security")
      expect(page).to have_content("Recovery codes generated on")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)
    end
    it "login using a recovery code" do
      # Enable TOTP
      enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Get recovery codes
      click_on "Get Recovery Codes"
      recovery_codes = get_recovery_codes()

      click_on("Back to Account Security")
      expect(page).to have_content("Recovery codes generated on")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Sign out and back in with a recovery code
      switch_to_user(@email, @password, totp: recovery_codes[0])
      expect(page).to have_content("Sign out")
    end
    it "disable TOTP using a recovery code" do
      # Enable TOTP
      enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Get recovery codes
      click_on "Get Recovery Codes"
      recovery_codes = get_recovery_codes()

      click_on("Back to Account Security")
      expect(page).to have_content("Recovery codes generated on")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Disable TOTP with a recovery code
      disable_2fa(code: recovery_codes[0])
      expect(page).to have_content("TOTP disabled")
      expect(page).to have_content("Because you have no 2FA methods enabled, your account recovery codes have been disabled. To generate and use recovery codes, please enable a 2FA method.")
      expect(page).to have_content("Enable TOTP")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_current_path(account_security_path)
    end
  end

  describe "User cannot" do
    it "enable TOTP with an expired TOTP code" do
      # Fill in form with accurate TOTP code but do not submit it
      code = enable_2fa(submit_form: false)

      # Wait for code to expire, then submit the form
      sleep(60) # 60 seconds to ensure it's not within the time drift range
      click_on "Enable TOTP"
      expect(page).to have_content("Invalid code, please try again")
      expect(page.find('input#totp-seed')[:placeholder]).to eq(code.secret)
        # have_content does not work for placeholder elements, so I have to
        # reference the element attribute directly
    end
    it "enable TOTP with an incorrect TOTP code" do
      # <6 digit TOTP code
      enable_2fa(code: 1111)
      expect(page).to have_content("Invalid code, please try again")
      expect(page).to have_current_path(account_security_2fa_totp_path)

      # >6 digit TOTP code
      enable_2fa(code: 8675309)
      expect(page).to have_content("Invalid code, please try again")
      expect(page).to have_current_path(account_security_2fa_totp_path)

      # Decimal TOTP code
      enable_2fa(code: 3.14159)
      expect(page).to have_content("Invalid code, please try again")
      expect(page).to have_current_path(account_security_2fa_totp_path)

      # ASCII TOTP code
      enable_2fa(code: 'e' * 512)
      expect(page).to have_content("Invalid code, please try again")
      expect(page).to have_current_path(account_security_2fa_totp_path)
    end
    it "enable TOTP with an expired TOTP seed" do
      # When you visit the enable TOTP page, if you do not submit a correct
      # code for the seed after 5 minutes, it is regenerated. This tests
      # to make sure it generates a new seed

      # Fill in form with accurate TOTP code but do not submit it
      code = enable_2fa(submit_form: false)

      # Wait for code to expire, then submit the form
      sleep(60)
      click_on "Enable TOTP"

      # Loop until seed changes
      iterations = 0
      while(page.find('input#totp-seed')[:placeholder] == code.secret)
        # Code has not yet changed
        expect(iterations).to be < 10 # Should only take 5 minutes, not >= 10
        expect(page).to have_content("Invalid code, please try again")
          # Code submitted was 1 minute old
        expect(page).to have_current_path(account_security_2fa_totp_path)

        fill_in "user_otp_attempt", with: code.now
        sleep(60)
          # Ensures submitted code will always be 1 minute behind
        click_on "Enable TOTP"
        iterations += 1
      end

      # A new TOTP seed has been generated
      expect(page).to have_content("Invalid code. Because you did not enter a correct code in time, the TOTP seed has been regenerated. Please enter a correct value for this new seed.")
      expect(page).to have_current_path(account_security_2fa_totp_path)
      expect(page.find('input#totp-seed')[:placeholder]).not_to eq(code.secret)
    end
    it "enable TOTP with a recovery code" do
      # Enable TOTP
      totp = enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Wait 30 seconds to make sure the next code will be ready
      sleep(30)

      # Get recovery codes
      recovery_codes = get_recovery_codes()

      click_on("Back to Account Security")
      expect(page).to have_content("Recovery codes generated on")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Disable TOTP
      disable_2fa(code: totp)
      expect(page).to have_content("TOTP disabled")
      expect(page).to have_content("Because you have no 2FA methods enabled, your account recovery codes have been disabled. To generate and use recovery codes, please enable a 2FA method.")
      expect(page).to have_content("Enable TOTP")
      expect(page).to have_content("Recovery codes not generated")

      # Try to enable TOTP with a recovery code
      enable_2fa(code: recovery_codes[0])
      expect(page).to have_content("Invalid code, please try again")
      expect(page).to have_current_path(account_security_2fa_totp_path)
    end
    it "generate recovery codes before enabling TOTP" do
      # Try to get to recovery code generation page
      visit account_security_2fa_recovery_codes_path
      expect(page).to have_content("Because you have no 2FA methods enabled, you cannot generate account recovery codes. To generate and use recovery codes, please enable a 2FA method.")
      expect(page).to have_current_path(account_security_path)
      expect(page).to have_content("Recovery codes not generated")
    end
    it "login using an expired TOTP code" do
      # Enable TOTP
      totp = enable_2fa()

      # Wait until the next TOTP code is available
      sleep(30)
      code = totp.now

      # Wait 60 more seconds for time delay to expire, then sign out and
      # back in using old code
      sleep(60)
      switch_to_user(@email, @password, totp: code, expecting_success: false)
      expect(page).to have_content("Invalid 2FA or recovery code, please try again")
      expect(page).to have_content("Authenticate via One-Time Password (TOTP) or Recovery Code")

      # Make sure user can still sign in with a valid TOTP code
      fill_in "user_otp_attempt", with: totp.now
      click_on "Submit"
      expect(page).to have_content("Signed in with One-Time Password. If you used a recovery code, make sure to generate new codes so you don't run out!")
    end
    it "login using an incorrect TOTP code" do
      # Generate TOTP code
      enable_2fa()

      # Log out
      sign_out_with()

      # Attempt to sign in with a <6 digit code
      sign_in_with(@email, @password, totp: 2222, expecting_success: false)

      # Attempt to sign in with a >6 digit code
      sign_in_with(@email, @password, totp: 8675309, expecting_success: false)

      # Attempt to sign in with a decimal code
      sign_in_with(@email, @password, totp: 3.14159, expecting_success: false)

      # Attempt to sign in with an ASCII code
      sign_in_with(@email, @password, totp: 'e' * 512, expecting_success: false)
    end
    it "login using an incorrect recovery code" do
      # Enable TOTP
      enable_2fa()

      # Generate recovery codes
      recovery_codes = get_recovery_codes()

      # Sign out and attempt to sign in with incorrect codes
      switch_to_user(@email, @password, totp: recovery_codes[0] + 'oh', expecting_success: false)
    end
    it "login using an already used recovery code" do
      # Enable TOTP
      enable_2fa()

      # Generate recovery codes
      recovery_codes = get_recovery_codes()

      # Sign out and sign back in with a recovery code
      switch_to_user(@email, @password, totp: recovery_codes[0])

      # Sign out and attempt to sign in with the same recovery code
      switch_to_user(@email, @password, totp: recovery_codes[0], expecting_success: false)
      expect(page).not_to have_content("Sign out")
    end
    it "disable TOTP using a TOTP code already used" do
      # Ensure the code will not change in the middle of this test, which
      # prevent a race condition.
      # I chose these ranges because this application uses the default 30
      # second TOTP code timer.
      if Time.now.sec > 15 && Time.now.sec < 30 # [16, 30)
        delay = 30 - Time.now.sec
      elsif Time.now.sec > 45 # [46, 60)
        delay = 60 - Time.now.sec
      else
        delay = 0
      end
      sleep(delay)

      # Enable TOTP
      totp = enable_2fa()

      # Disable 2FA without any delay
      disable_2fa(code: totp)
      expect(page).to have_content("Invalid code; TOTP is still enabled. If you used the same code recently, please wait for the next code to appear on your device.")
    end
    it "disable TOTP using an invalidated recovery code" do
      # Enable TOTP
      totp = enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Generate recovery codes
      recovery_codes = get_recovery_codes()
      click_on("Back to Account Security")
      expect(page).to have_content("Recovery codes generated on")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Disable 2FA after waiting for next code
      sleep(30)
      disable_2fa(code: totp)
      expect(page).to have_content("TOTP disabled")
      expect(page).to have_content("Because you have no 2FA methods enabled, your account recovery codes have been disabled. To generate and use recovery codes, please enable a 2FA method.")
      expect(page).to have_content("Enable TOTP")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_current_path(account_security_path)

      # Re-enable TOTP
      sleep(30) # Have to wait or it will cause an error
      enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Attempt to disable TOTP with an old recovery code
      disable_2fa(code: recovery_codes[0])
      expect(page).to have_content("Invalid code; TOTP is still enabled. If you used the same code recently, please wait for the next code to appear on your device.")
      expect(page).to have_current_path(account_security_2fa_totp_path)
    end
    it "disable TOTP using a regenerated recovery code" do
      # Enable TOTP
      enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Generate recovery codes
      recovery_codes = get_recovery_codes()

      # Regenerate recovery codes
      get_recovery_codes()
      click_on("Back to Account Security")
      expect(page).to have_content("Recovery codes generated on")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Attempt to disable TOTP with an invalidated recovery code
      disable_2fa(code: recovery_codes[0])
      expect(page).to have_content("Invalid code; TOTP is still enabled. If you used the same code recently, please wait for the next code to appear on your device.")
    end
    it "disable TOTP using an already used recovery code" do
      # Enable TOTP
      enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Generate recovery codes
      recovery_codes = get_recovery_codes()

      # Use a recovery code to log back in
      switch_to_user(@email, @password, totp: recovery_codes[0])

      # Attempt to disable TOTP with that same recovery code
      disable_2fa(code: recovery_codes[0])
      expect(page).to have_content("Invalid code; TOTP is still enabled. If you used the same code recently, please wait for the next code to appear on your device.")
    end
    it "disable TOTP using an expired TOTP code" do
      # Enable TOTP
      totp = enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Wait 30 seconds and generate current TOTP code
      sleep(30)
      code = totp.now

      # Wait until time shift delay expires, then attempt to disable TOTP with
      # that old code
      sleep(60)
      disable_2fa(code: code)
      expect(page).to have_content("Invalid code; TOTP is still enabled. If you used the same code recently, please wait for the next code to appear on your device.")
      expect(page).to have_current_path(account_security_2fa_totp_path)
    end
    it "disable TOTP using an incorrect TOTP code" do
      # Enable TOTP
      enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Attempt to disable TOTP with a <6 digit code
      disable_2fa(code: 2222)
      expect(page).to have_content("Invalid code; TOTP is still enabled. If you used the same code recently, please wait for the next code to appear on your device.")
      expect(page).to have_current_path(account_security_2fa_totp_path)

      # Attempt to disable TOTP with a >6 digit code
      disable_2fa(code: 8675309)
      expect(page).to have_content("Invalid code; TOTP is still enabled. If you used the same code recently, please wait for the next code to appear on your device.")
      expect(page).to have_current_path(account_security_2fa_totp_path)

      # Attempt to disable TOTP with a decimal code
      disable_2fa(code: 3.14159)
      expect(page).to have_content("Invalid code; TOTP is still enabled. If you used the same code recently, please wait for the next code to appear on your device.")
      expect(page).to have_current_path(account_security_2fa_totp_path)

      # Attempt to disable TOTP with an ASCII code
      disable_2fa(code: 'e' * 512)
      expect(page).to have_content("Invalid code; TOTP is still enabled. If you used the same code recently, please wait for the next code to appear on your device.")
      expect(page).to have_current_path(account_security_2fa_totp_path)
    end
    it "bypass TOS acceptance when TOTP is enabled" do
      # See issue #76 on GitLab for more info

      # Enable TOTP
      totp = enable_2fa()
      expect(page).to have_content("TOTP enabled. Make sure to generate recovery codes so you don't get locked out of your account!")
      expect(page).to have_content("Recovery codes not generated")
      expect(page).to have_content("Get Recovery Codes")
      expect(page).to have_content("Disable TOTP")
      expect(page).to have_current_path(account_security_path)

      # Wait 30 seconds, then un-accept TOS
      sleep(30) # Ensure same TOTP code will not be used twice
      visit policy_tos_path
      expect(page).to have_content("Terms of Service")
      expect(page).to have_button("Continue")
      within('.radio-buttons-tos') do
        choose "I do not agree to the terms and conditions above, but I wish to keep my account"
      end
      within('.actions') do
        click_on "Continue"
      end
      expect(page).to have_content("You have been signed out. To use the site, sign in and accept the terms of service.")
      expect(page).to have_content("Sign in") # Make sure user is signed out

      # Sign back in
      sign_in_with(@email, @password, totp: totp, accept_tos: false)
      expect(page).to have_content("You must accept the Terms of Service before continuing.")
      expect(page).to have_content("Terms of Service")
      expect(page).to have_content("Sign in") # User is not fully signed in
      click_on "Continue"
      expect(page).to have_content("Thank you for accepting the terms of service!")
      expect(page).to have_content("Sign out") # Make sure user is signed in
    end
  end
end

require 'rails_helper'
include AuthenticationHelpers
include ReportsHelpers
# need to:
# add some reports to the test_seeds file
# change to login with premade admin user account
# test functionality from the user's reports view

RSpec.describe "UserReportsSearch", type: :request do
  before :each do
    name = 'Test User 1'
    email = 'testuser@domain.test'
    password = 'CorrectHorseBatteryStaple'
    sign_up_with(name, email, password, password)
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    visit chat_logs_path

    create_report_with(msg_id: 126, reason: 'Report 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
    create_report_with(msg_id: 126, reason: 'RePoRt 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing', 'You have already reported this message. To view the status of your report, go to the Reports tab in the Settings menu'])
    @midpoint_time = Time.now # To be used when searching with report creation time
    create_report_with(msg_id: 110, reason: '/rEpOrT 2/', expecting_before: ['#stereotonetim', '"big pink balloon rabbit destroys wildlife"'])
    create_report_with(msg_id: 51, reason: '/report 3', expecting_before: ['#stereotonetim', 'timPlus'])
    create_report_with(msg_id: 48, reason: '(REPORT 4)', expecting_before: ['#stereotonetim', 'I can\'t believe I missed it! timMinus'])

    visit reports_path
  end

  describe "User can" do
    it "search their reports with no fields filled in" do
      expect(page).not_to have_content("Next ›")
      search_user_reports_with(expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Next ›")
    end
    it "search their reports with a message channel" do
      # Channel that does not exist
      search_user_reports_with(msg_channel: '#unknown_channel', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['No reports found'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")

      # Channel that does exist
      search_user_reports_with(msg_channel: '#stereotonetim', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['/rEpOrT 2/', '/report 3', '(REPORT 4)'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")

      search_user_reports_with(msg_channel: '#channel12345678901234567_', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")
    end
    it "search their reports with a message sender" do
      # Sender that does not exist
      search_user_reports_with(msg_sender: 'nonexistent_user', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['No reports found'])
      expect(page).not_to have_content("Failed to execute search, see errors below")

      # Sender that does exist
      search_user_reports_with(msg_sender: 'luna_moona', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")

      search_user_reports_with(msg_sender: 'shimmerfairy', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['/report 3', '(REPORT 4)'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
    end
    it "search their reports with a message ID" do
      # ID is positive but will return no results (message has not been
      # reported by the current user)
      search_user_reports_with(msg_id: 90, expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['No reports found'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")

      search_user_reports_with(msg_id: 1, expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['No reports found'])
        # 1 is the starting ID for chat logs
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")

      # ID is positive but will return no results (message does not exist)
      search_user_reports_with(msg_id: 999, expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['No reports found'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")

      search_user_reports_with(msg_id: 2147483647, expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['No reports found'])
        # 2147483647 is the current max ID
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")

      # ID is positive and will return results
      search_user_reports_with(msg_id: 126, expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")
    end
    it "search their reports with a report status" do
      # Resolve some of the reports
      last_report = Report.last.id
      resolve_report(last_report, resolving_action: 'Cats make about 100 different sounds, while dogs make only about 10') # '(REPORT 4)'
      resolve_report(last_report - 4, resolving_action: 'A cat usually has about 12 whiskers on each side of its face') # 'Report 1'

      # Status "All"
      # This is technically implied in all the tests before now, but I'm
      # explicitly testing it here
      search_user_reports_with(report_status: 'All', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)', 'Cats make about 100 different sounds, while dogs make only about 10', 'A cat usually has about 12 whiskers on each side of its face'])
      expect(page).not_to have_content("Failed to execute search, see errors below")

      # Status "Resolved"
      search_user_reports_with(report_status: 'Resolved', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', '(REPORT 4)', 'Cats make about 100 different sounds, while dogs make only about 10', 'A cat usually has about 12 whiskers on each side of its face'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")

      # Status "Unresolved"
      search_user_reports_with(report_status: 'Unresolved', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['RePoRt 1', '/rEpOrT 2/', '/report 3'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("(REPORT 4)")
    end
    it "search their reports with a case insensitive report reason without regex" do
      # Normal searches that will get results
      search_user_reports_with(report_reason: 'report 1', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")

      search_user_reports_with(report_reason: 'epor', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'])
      expect(page).not_to have_content("Failed to execute search, see errors below")

      # Normal search that will not get any results
      # ID is positive but will return no results (message does not exist)
      search_user_reports_with(report_reason: 'no', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['No reports found'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")

      # Excessively large search (test to make sure >255 character searches work)
      search_user_reports_with(report_reason: 'no' * 255, expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['No reports found']) # 512 character report reason
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")
    end
    it "search their reports with a case sensitive report reason with regex" do
      # Search that will have results
      search_user_reports_with(report_reason: '/\/report/', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['/report 3'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("(REPORT 4)")

      # Search that will not have any results
      search_user_reports_with(report_reason: '/\(report 4\)/', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['No reports found'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")
    end
    it "search their reports with a case insensitive report reason with escaped regex" do
      search_user_reports_with(report_reason: '\/report', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['/rEpOrT 2/', '/report 3'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("(REPORT 4)")
    end
    it "search their reports with a case insensitive message text without regex" do
      # Normal search
      search_user_reports_with(msg_text: 'Tim', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['/report 3', 'timPlus', '(REPORT 4)', 'I can\'t believe I missed it! timMinus'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("\"big pink balloon rabbit destroys wildlife\"")

      # Long search (>255 characters)
      search_user_reports_with(msg_text: 'gg' * 255, expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['No reports found']) # 512 character message text
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")
    end
    it "search their reports with a case sensitive message text with regex" do
      # Create another report for this specifically
      create_report_with(msg_id: 39, reason: 'report 5', expecting_before: ['#stereotonetim', 'timOh'])

      # Search that will have some results
      search_user_reports_with(msg_text: '/tim(Plus|Minus)/', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)', 'report 5'], expecting_after: ['/report 3', 'timPlus', '(REPORT 4)', 'I can\'t believe I missed it! timMinus'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("\"big pink balloon rabbit destroys wildlife\"")
      expect(page).not_to have_content("report 5")
      expect(page).not_to have_content("timOh")

      # Search that will have no results
      search_user_reports_with(msg_text: '/tim(plus|minus|oh)/', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)', 'report 5'], expecting_after: ['No reports found'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("\"big pink balloon rabbit destroys wildlife\"")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("timPlus")
      expect(page).not_to have_content("(REPORT 4)")
      expect(page).not_to have_content("I can't believe I missed it! timMinus")
      expect(page).not_to have_content("report 5")
      expect(page).not_to have_content("timOh")
    end
    it "search their reports with a case insensitive message text with escaped regex" do
      # Create some new reports specifically for this
      create_report_with(msg_id: 128, reason: 'Report 5', expecting_before: ['#luna_moona', '/Forward slash test'])
      create_report_with(msg_id: 129, reason: 'Report 6', expecting_before: ['#luna_moona', '/forward Slash test/'])
      create_report_with(msg_id: 130, reason: 'Report 7', expecting_before: ['#luna_moona', 'forward slash Test/'])

      # Escaped starting forward slash
      search_user_reports_with(msg_text: '\/forwArd', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)', '/Forward slash test', 'Report 5', '/forward Slash test/', 'Report 6', 'forward slash Test/', 'Report 7'], expecting_after: ['Report 5', '/Forward slash test', 'Report 6', '/forward Slash test/'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("\"big pink balloon rabbit destroys wildlife\"")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("timPlus")
      expect(page).not_to have_content("(REPORT 4)")
      expect(page).not_to have_content("I can't believe I missed it! timMinus")
      expect(page).not_to have_content("Report 7")
      expect(page).not_to have_content("forward slash Test/")

      # Escaped starting and ending forward slashes
      search_user_reports_with(msg_text: '\/forwarD slAsh tEst\/', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)', '/Forward slash test', 'Report 5', '/forward Slash test/', 'Report 6', 'forward slash Test/', 'Report 7'], expecting_after: ['Report 6', '/forward Slash test/'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("\"big pink balloon rabbit destroys wildlife\"")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("timPlus")
      expect(page).not_to have_content("(REPORT 4)")
      expect(page).not_to have_content("I can't believe I missed it! timMinus")
      expect(page).not_to have_content("Report 5")
      expect(page).not_to have_content("/Forward slash test")
      expect(page).not_to have_content("Report 7")
      expect(page).not_to have_content("forward slash Test/")

      # Escaped ending forward slash
      search_user_reports_with(msg_text: 'tEst\/', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)', '/Forward slash test', 'Report 5', '/forward Slash test/', 'Report 6', 'forward slash Test/', 'Report 7'], expecting_after: ['Report 6', '/forward Slash test/', 'Report 7', 'forward slash Test/'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("\"big pink balloon rabbit destroys wildlife\"")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("timPlus")
      expect(page).not_to have_content("(REPORT 4)")
      expect(page).not_to have_content("I can't believe I missed it! timMinus")
      expect(page).not_to have_content("Report 5")
      expect(page).not_to have_content("/Forward slash test")
    end
    it "search their reports with a starting reported time before the current system time" do
      # Will not return all reports
      first_report = Report.find(Report.last.id - 4)
      first_report.created_at = Time.now.utc - 5.minutes
        # Because all these reports are created one after the other (and will
        # occur in less than a minute, smaller than the 1 minute increments
        # allowed on the date reported field), I have to adjust the created_at
        # attribute on 1 of them to have anything to look for.
      first_report.save!
      search_user_reports_with(start_time: (Time.now.utc - 3.minutes).strftime('%Y-%m-%d %H:%M'), expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")

      # Will return all reports
      search_user_reports_with(start_time: (Time.now.utc - 6.minutes).strftime('%Y-%m-%d %H:%M'), expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
    end
    it "search their reports with a ending reported time before the current system time" do
      # Will return some reports
      first_report = Report.find(Report.last.id - 4)
      first_report.created_at = Time.now.utc - 5.minutes
      first_report.save!
      search_user_reports_with(end_time: (Time.now.utc - 3.minutes).strftime('%Y-%m-%d %H:%M'), expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")

      # Will return no reports
      search_user_reports_with(end_time: (Time.now.utc - 6.minutes).strftime('%Y-%m-%d %H:%M'), expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['No reports found'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")
    end
    it "search their reports with a starting and ending reported time before the current system time" do
      # Will return some reports
      # Modify creation dates for multiple reports
      report = Report.find(Report.last.id - 4) # 'Report 1'
      report.created_at = Time.now.utc - 5.minutes
      report.save!
      report = Report.find(Report.last.id - 3) # 'RePoRt 1'
      report.created_at = Time.now.utc - 4.minutes
      report.save!
      report = Report.find(Report.last.id - 2) # '/rEpOrT 2/'
      report.created_at = Time.now.utc - 3.minutes
      report.save!
      report = Report.find(Report.last.id - 1) # '/report 3'
      report.created_at = Time.now.utc - 2.minutes
      report.save!
      report = Report.find(Report.last.id) # '(REPORT 4)'
      report.created_at = Time.now.utc - 1.minutes
      report.save!
      search_user_reports_with(start_time: (Time.now.utc - 4.minutes).strftime('%Y-%m-%d %H:%M'), end_time: (Time.now.utc - 2.minutes).strftime('%Y-%m-%d %H:%M'), expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['RePoRt 1', '/rEpOrT 2/'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")

      # Will return all reports
      search_user_reports_with(start_time: (Time.now.utc - 6.minutes).strftime('%Y-%m-%d %H:%M'), end_time: (Time.now.utc).strftime('%Y-%m-%d %H:%M'), expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
    end
    it "search their reports with a custom results per page" do
      # Generate lots more reports
      report = 5
      (110...130).each do |n|
        create_report_with(msg_id: n, reason: "Report #{report}", expecting_before: [ChatLog.find(n).channel, ChatLog.find(n).message])
        report += 1
      end

      # Make sure these reports are visible - 11 per page
      search_user_reports_with(per_page: 11)
      expect(page).to have_content("Report 24")
      expect(page).to have_content("Report 20")
      expect(page).to have_content("Report 17")
      expect(page).to have_link("Next ›", href: "/account/reports?advanced_query=&channel=&end_time=&msg_id=&page=2&per_page=11&reason=&resolved=all&sender=&start_time=")
      expect(page).to have_link("Last »", href: "/account/reports?advanced_query=&channel=&end_time=&msg_id=&page=3&per_page=11&reason=&resolved=all&sender=&start_time=")
      click_on "Next ›"
      expect(page).to have_content("Report 13")
      expect(page).to have_content("Report 10")
      expect(page).to have_content("Report 6")
      click_on "Next ›"
      expect(page).to have_content("/rEpOrT 2/")
      expect(page).to have_content("RePoRt 1")
      expect(page).to have_content("Report 1")
      expect(page).to have_link("« First", href: "/account/reports?advanced_query=&channel=&end_time=&msg_id=&per_page=11&reason=&resolved=all&sender=&start_time=")
      expect(page).to have_link("‹ Prev", href: "/account/reports?advanced_query=&channel=&end_time=&msg_id=&page=2&per_page=11&reason=&resolved=all&sender=&start_time=")

      # 15 per page
      search_user_reports_with(per_page: 15)
      expect(page).to have_content("Report 24")
      expect(page).to have_content("Report 20")
      expect(page).to have_content("Report 17")
      expect(page).to have_content("Report 13")
      expect(page).to have_content("Report 10")
      expect(page).to have_link("Next ›", href: "/account/reports?advanced_query=&channel=&end_time=&msg_id=&page=2&per_page=15&reason=&resolved=all&sender=&start_time=")
      expect(page).to have_link("Last »", href: "/account/reports?advanced_query=&channel=&end_time=&msg_id=&page=2&per_page=15&reason=&resolved=all&sender=&start_time=")
      click_on "Next ›"
      expect(page).to have_content("Report 6")
      expect(page).to have_content("/rEpOrT 2/")
      expect(page).to have_content("RePoRt 1")
      expect(page).to have_content("Report 1")
      expect(page).to have_link("« First", href: "/account/reports?advanced_query=&channel=&end_time=&msg_id=&per_page=15&reason=&resolved=all&sender=&start_time=")
      expect(page).to have_link("‹ Prev", href: "/account/reports?advanced_query=&channel=&end_time=&msg_id=&per_page=15&reason=&resolved=all&sender=&start_time=")

      # 30 per page
      search_user_reports_with(per_page: 30)
      expect(page).to have_content("Report 24")
      expect(page).to have_content("Report 20")
      expect(page).to have_content("Report 17")
      expect(page).to have_content("Report 13")
      expect(page).to have_content("Report 10")
      expect(page).to have_content("Report 6")
      expect(page).to have_content("/rEpOrT 2/")
      expect(page).to have_content("RePoRt 1")
      expect(page).to have_content("Report 1")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("Last »")
    end
    it "search their reports with multiple fields filled in" do
      # Resolve a few
      last_report = Report.last.id
      resolve_report(last_report, resolving_action: 'Cats make about 100 different sounds, while dogs make only about 10') # '(REPORT 4)'
      resolve_report(last_report - 4, resolving_action: 'A cat usually has about 12 whiskers on each side of its face') # 'Report 1'

      # Status, reason (no regex)
      search_user_reports_with(report_status: 'Unresolved', report_reason: 'report', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['RePoRt 1', '/rEpOrT 2/', '/report 3'])
      expect(page).not_to have_content("Failed to execute search, see errors below")

      # Status, reason (regex)
      search_user_reports_with(report_status: 'Unresolved', report_reason: '/(Re|rE)/', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['RePoRt 1', '/rEpOrT 2/'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("(REPORT 4)")

      # Create another report
      create_report_with(msg_id: 39, reason: 'report 5', expecting_before: ['#stereotonetim', 'timOh'])

      # Status, message text (regex)
      search_user_reports_with(msg_text: '/tim(Plus|Minus|oh)/', expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)', 'report 5'], expecting_after: ['/report 3', 'timPlus', '(REPORT 4)', 'I can\'t believe I missed it! timMinus'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("RePoRt 1")
      expect(page).not_to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("/rEpOrT 2/")
      expect(page).not_to have_content("\"big pink balloon rabbit destroys wildlife\"")
      expect(page).not_to have_content("report 5")
      expect(page).not_to have_content("timOh")

      # Create more reports for the next test
      (6..15).each do |n|
        create_report_with(msg_id: 126, reason: "report #{n}", expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      end

      # Message ID
      search_user_reports_with(msg_id: 126)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("report 15")
      expect(page).to have_content("report 12")
      expect(page).to have_content("report 9")
      expect(page).to have_content("report 6")
      expect(page).to have_link("Next ›", href: "/account/reports?advanced_query=&channel=&end_time=&msg_id=126&page=2&per_page=&reason=&resolved=all&sender=&start_time=")
      expect(page).to have_link("Last »", href: "/account/reports?advanced_query=&channel=&end_time=&msg_id=126&page=2&per_page=&reason=&resolved=all&sender=&start_time=")

      # Generate a bunch more reports
      (16..30).each do |n|
        create_report_with(msg_id: 126, reason: "Report #{n}", expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      end

      # Modify all the creation dates so the oldest will be created 35 minutes
      # ago and increments by 1 minute intervals
      offset = 35
        # Why did I choose 35? Because I currently have 30 reports created,
        # so it gives me a little bit of leeway before I would end up with
        # reports being created in the future, which might break the tests.
        # Initially I was using the ID of the reports, but as you test over
        # and over, the IDs start getting into the thousands and mess
        # everything up. Kinda brittle, but not too difficult to adjust.
      User.where(email: 'testuser@domain.test').first.reports.each do |report|
        # I can't use current_user.reports or Current.user.reports in this
        # context.
        report.created_at = Time.now.utc - offset.minutes
        report.save!
        offset -= 1
        # I initially used reports.reverse_order.each, but modifying creation
        # date can end up reordering things so I ended up with some reports
        # not being touched and others being modified multiple times.
        # That is why I have to decrement offset each time.
      end

      # Status, date reported end, per page
      search_user_reports_with(report_status: 'Unresolved', end_time: (Time.now.utc - 10.minutes).strftime('%Y-%m-%d %H:%M'), expecting_before: ['Report 29', 'Report 26', 'Report 23'], expecting_after: ['Report 23', 'Report 19', 'report 15'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 30")
      expect(page).not_to have_content("Report 25")

      # Message channel, per page
      search_user_reports_with(msg_channel: '#channel12345678901234567_', per_page: 15, expecting_before: ['Report 30', 'Report 25', 'Report 21'], expecting_after: ['Report 30', 'Report 25', 'Report 21', 'Report 16'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_link("Next ›", href: "/account/reports?advanced_query=&channel=%23channel12345678901234567_&end_time=&msg_id=&page=2&per_page=15&reason=&resolved=all&sender=&start_time=")
      expect(page).to have_link("Last »", href: "/account/reports?advanced_query=&channel=%23channel12345678901234567_&end_time=&msg_id=&page=2&per_page=15&reason=&resolved=all&sender=&start_time=")

      # Resolve some reports
      last_report = Report.last.id
      resolve_report(last_report - 1) # 'Report 29'
      resolve_report(last_report - 3) # 'Report 27'
      resolve_report(last_report - 15) # 'Report 15'

      # Status, message sender
      search_user_reports_with(report_status: 'Resolved', msg_sender: 'luna_moona', expecting_before: ['Report 30', 'Report 25', 'Report 21'], expecting_after: ['Report 29', 'Report 27', 'report 15', 'Report 1'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("Report 30")
      expect(page).not_to have_content("Report 25")
      expect(page).not_to have_content("Report 21")
      expect(page).not_to have_content("Next ›")

      # Date reported start, message text (regex)
      start_time = User.where(email: 'testuser@domain.test').first.reports.where(msg_text: 'timOh').first.created_at.strftime('%Y-%m-%d %H:%M')
      search_user_reports_with(msg_text: '/tim(Plus|Minus|Oh)/', start_time: start_time, expecting_before: ['Report 30', 'Report 25', 'Report 21'], expecting_after: ['report 5', 'timOh'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("/report 3")
      expect(page).not_to have_content("timPlus")
      expect(page).not_to have_content("(REPORT 4)")
      expect(page).not_to have_content("I can't believe I missed it! timMinus")
    end
  end

  describe "User cannot" do
    it "search their reports for reports created by others" do
      # Create reports with admin account
      switch_to_user('admin@domain.example', 'passwordpassword1')
      create_report_with(msg_id: 126, reason: "Admin report", expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])

      # Switch back to normal account and verify I cannot see the report,
      # even if I search for it
      switch_to_user('testuser@domain.test', 'CorrectHorseBatteryStaple')
      visit reports_path
      expect(page).not_to have_content("Admin report")

      search_user_reports_with(report_reason: "Admin report", expecting_after: ['No reports found'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("A blank search has been executed")
    end
    it "search their reports with an invalid message channel format" do
      # No preceding # with >3 character channel name
      search_user_reports_with(msg_channel: 'luna_moona')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # No preceding # with 3 character channel name
      search_user_reports_with(msg_channel: 'tla')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # No preceding # with <3 character channel name
      search_user_reports_with(msg_channel: 'ez')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # Less than 3 character channel name after the #
      search_user_reports_with(msg_channel: '#ez')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # Channel name with a space
      search_user_reports_with(msg_channel: '#fake channel')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # Channel name with a dash
      search_user_reports_with(msg_channel: '#fake-channel')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # Channel name >25 characters
      search_user_reports_with(msg_channel: '#dddddddddddddddddddddddddd')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # Channel name with mixed case
      search_user_reports_with(msg_channel: '#fAkE_ChanneL')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # Channel name with all upper case letters
      search_user_reports_with(msg_channel: '#STEREOTONETIM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with an invalid message sender format" do
      # Sender name with <3 characters
      search_user_reports_with(msg_sender: 'ez')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # Sender name with a space
      search_user_reports_with(msg_sender: 'fake sender')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # Sender name with a dash
      search_user_reports_with(msg_sender: 'fake-sender')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # Sender name with >26 characters
      search_user_reports_with(msg_sender: 'dddddddddddddddddddddddddd')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # Sender name with mixed case
      search_user_reports_with(msg_sender: 'FaKe_sender')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")

      # Sender name with all upper case letters
      search_user_reports_with(msg_sender: 'FAKE_SENDER')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with a negative or zero message ID" do
      # Message ID 0
      search_user_reports_with(msg_id: 0)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reported message ID, must be a positive integer")
      expect(page).to have_content("A blank search has been executed")

      # Message ID -100
      search_user_reports_with(msg_id: -100)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reported message ID, must be a positive integer")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with a decimal message ID" do
      # Positive decimal message ID
      search_user_reports_with(msg_id: 3.14159)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reported message ID, must be a positive integer")
      expect(page).to have_content("A blank search has been executed")

      # Negative decimal message ID
      search_user_reports_with(msg_id: -3.14159)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reported message ID, must be a positive integer")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with a non-numeric message ID" do
      search_user_reports_with(msg_id: 'ggggggg')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reported message ID, must be a positive integer")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with a message ID greater than 2147483647" do
      # 2^31 = 2147483648, the current max for the message ID fields
      search_user_reports_with(msg_id: 2147483648)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid message ID, must be less than or equal to 2147483647")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with a message ID less than -2147483648" do
      # -2^31 = 2147483648, the current min for the message ID fields
      # (although it shouldn't let the user specify that to begin with since
      # message IDs start at 1)
      search_user_reports_with(msg_id: -2147483649)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reported message ID, must be a positive integer")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with an invalid message text regex" do
      # Unopened parenthesis
      search_user_reports_with(msg_text: '/\(closed parenthesis for regex testing)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in message text, please double check your query. Error details: unmatched close parenthesis")
      expect(page).to have_content("A blank search has been executed")

      # Unclosed parenthesis
      search_user_reports_with(msg_text: '/(closed parenthesis for regex testing\)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in message text, please double check your query. Error details: end pattern with unmatched parenthesis")
      expect(page).to have_content("A blank search has been executed")

      # Unclosed square brackets
      search_user_reports_with(msg_text: '/[closed parenthesis for regex testing\)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in message text, please double check your query. Error details: premature end of char-class")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with an invalid report status" do
      # There is no good way to force a new value into the dropdown menu
      # without using JS to modify the page, but since this is a simple GET,
      # I can just set a custom report status in the URL
      visit "/account/reports?utf8=#{URI.encode('✓')}&resolved=aaaAAAAAAaaaaaaaa"
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid report status, please select an option from the dropdown menu")
      expect(page).to have_content("A blank search has been executed")

      # Check to make sure changing capitalization of valid statuses results
      # in an error
      visit "/account/reports?utf8=#{URI.encode('✓')}&resolved=Resolved"
        # Without the URI.encode, I get a URI::InvalidURIError due to having
        # non-ASCII characters. This ocurs even if I do "utf8=\u2713", so
        # there is no way around using URI.encode.
        # https://stackoverflow.com/a/50158549
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid report status, please select an option from the dropdown menu")
      expect(page).to have_content("A blank search has been executed")

      # Check to make sure an absurdly long status displays an error message
      visit "/account/reports?utf8=#{URI.encode('✓')}&resolved=#{'e' * 256}"
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid report status, please select an option from the dropdown menu")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with an invalid report reason regex" do
      # Unopened parenthesis
      search_user_reports_with(report_reason: '/\(closed parenthesis for regex testing)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in reporting reason, please double check your query. Error details: unmatched close parenthesis")
      expect(page).to have_content("A blank search has been executed")

      # Unclosed parenthesis
      search_user_reports_with(report_reason: '/(closed parenthesis for regex testing\)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in reporting reason, please double check your query. Error details: end pattern with unmatched parenthesis")
      expect(page).to have_content("A blank search has been executed")

      # Unclosed square brackets
      search_user_reports_with(report_reason: '/[closed parenthesis for regex testing\)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in reporting reason, please double check your query. Error details: premature end of char-class")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with a starting reported time after the current system time" do
      year = Time.now.year + 5
      search_user_reports_with(start_time: "#{year}-01-01 09:00", expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'])
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Starting time cannot be greater than the current system time")
      expect(page).not_to have_content("Ending time cannot be greater than the current system time")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with an ending reported time after the current system time" do
      year = Time.now.year + 5
      search_user_reports_with(end_time: "#{year}-12-01 09:00", expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'])
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).not_to have_content("Starting time cannot be greater than the current system time")
      expect(page).to have_content("Ending time cannot be greater than the current system time")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with a starting and ending reported time after the current system time" do
      year = Time.now.year + 5
      search_user_reports_with(start_time: "#{year}-01-01 09:00", end_time: "#{year}-12-01 09:00", expecting_before: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'], expecting_after: ['Report 1', 'RePoRt 1', '/rEpOrT 2/', '/report 3', '(REPORT 4)'])
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Starting time cannot be greater than the current system time")
      expect(page).to have_content("Ending time cannot be greater than the current system time")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with an starting time after the ending time" do
      search_user_reports_with(start_time: '2017-11-13 06:10', end_time: '2017-11-13 06:00')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Ending time must be greater than the starting time")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with an invalid reported time start format" do
      # No date, valid time
      search_user_reports_with(start_time: '13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # No time, valid date
      search_user_reports_with(start_time: '2018-02-27')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid time, invalid date - user's account is YYYY-MM-DD (system default), entered as MM-DD-YYYY
      search_user_reports_with(start_time: '02-27-2018 13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid date, invalid time - 24 hour time, entered with AM/PM
      search_user_reports_with(start_time: '2018-02-27 01:40 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid date, invalid time - entered with seconds
      search_user_reports_with(start_time: '2018-02-27 13:40:51')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid date, invalid time - 24 hour time, entered as a negative number
      search_user_reports_with(start_time: '2018-02-27 -13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Invalid date and time
      search_user_reports_with(start_time: '02-27-2018 -01:40:51 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Invalid date and time - random text
      search_user_reports_with(start_time: 'war. war never changes.')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with an invalid reported time end format" do
      # No date, valid time
      search_user_reports_with(end_time: '13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # No time, valid date
      search_user_reports_with(end_time: '2018-02-27')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid time, invalid date - user's account is YYYY-MM-DD (system default), entered as MM-DD-YYYY
      search_user_reports_with(end_time: '02-27-2018 13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid date, invalid time - 24 hour time, entered with AM/PM
      search_user_reports_with(end_time: '2018-02-27 01:40 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid date, invalid time - entered with seconds
      search_user_reports_with(end_time: '2018-02-27 13:40:51')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid date, invalid time - 24 hour time, entered as a negative number
      search_user_reports_with(end_time: '2018-02-27 -13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Invalid date and time
      search_user_reports_with(end_time: '02-27-2018 -01:40:51 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Invalid date and time - random text
      search_user_reports_with(end_time: 'war. war never changes.')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with invalid reported time start and end formats" do
      # No date, valid time
      search_user_reports_with(start_time: '12:40', end_time: '13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # No time, valid date
      search_user_reports_with(start_time: '2018-02-21', end_time: '2018-02-27')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid time, invalid date - user's account is YYYY-MM-DD (system default), entered as MM-DD-YYYY
      search_user_reports_with(start_time: '02-21-2018 12:40', end_time: '02-27-2018 13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid date, invalid time - 24 hour time, entered with AM/PM
      search_user_reports_with(start_time: '2018-02-21, 12:40 PM', end_time: '2018-02-27 01:40 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid date, invalid time - entered with seconds
      search_user_reports_with(start_time: '2018-02-21 12:40:31', end_time: '2018-02-27 13:40:51')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid date, invalid time - 24 hour time, entered as a negative number
      search_user_reports_with(start_time: '2018-02-21 -12:40', end_time: '2018-02-27 -13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Invalid date and time
      search_user_reports_with(start_time: '02-21-2018 -00:40:51 AM', end_time: '02-27-2018 -01:40:51 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Invalid date and time - random text
      search_user_reports_with(start_time: '3.14159265359', end_time: 'war. war never changes.')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with an valid start time and invalid end time" do
      year = Time.now.year + 5

      # Valid start time, end time format is valid but after current system time
      search_user_reports_with(start_time: '2018-02-21 12:40', end_time: "#{year}-02-27 13:40")
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Ending time cannot be greater than the current system time")
      expect(page).to have_content("A blank search has been executed")

      # Valid start time, end time format is invalid but before current system time
      search_user_reports_with(start_time: '2018-02-21 12:40', end_time: '2018-02-27 -13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid start time, end time format is invalid and after current system time
      search_user_reports_with(start_time: '2018-02-21 12:40', end_time: "#{year}-02-27 -13:40")
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with an invalid start time and valid end time" do
      year = Time.now.year + 5

      # Valid end time, start time format is valid but after current system time
      search_user_reports_with(start_time: "#{year}-02-21 13:40", end_time: '2018-02-27 12:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Starting time cannot be greater than the current system time")
      expect(page).to have_content("Ending time must be greater than the starting time")
      expect(page).to have_content("A blank search has been executed")

      # Valid end time, start time format is invalid but before current system time
      search_user_reports_with(start_time: '2018-02-21 -13:40', end_time: '2018-02-27 12:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")

      # Valid end time, start time format is invalid and after current system time
      search_user_reports_with(start_time: "#{year}-02-21 -13:40", end_time: '2018-02-27 12:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with per page below 10" do
      # 9 per page
      search_user_reports_with(per_page: 9)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")

      # 1 per page
      search_user_reports_with(per_page: 1)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")

      # 0 per page
      search_user_reports_with(per_page: 0)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")

      # -1 per page
      search_user_reports_with(per_page: -1)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")

      # -30 per page
      search_user_reports_with(per_page: -30)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")

      # -2147483649 per page (test to ensure values below -2^31 do not mess
      # things up)
      search_user_reports_with(per_page: -2147483649)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with per page above 1000" do
      # 1001 per page
      search_user_reports_with(per_page: 1001)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")

      # 15000 per page
      search_user_reports_with(per_page: 15000)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")

      # 2147483648 per page (test to ensure values above 2^31-1 do not mess
      # things up)
      search_user_reports_with(per_page: 2147483648)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with a decimal results per page" do
      # 2147483648.999 per page
      search_user_reports_with(per_page: 2147483648.999)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")

      # 1000.7 per page
      search_user_reports_with(per_page: 1000.7)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")

      # 9.1 per page
      search_user_reports_with(per_page: 9.1)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")

      # -17.33 per page
      search_user_reports_with(per_page: -17.33)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")

      # -2147483649.95 per page
      search_user_reports_with(per_page: -2147483649.95)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with a non-numeric results per page" do
      search_user_reports_with(per_page: 'aaaAAAAAAaaaaaaaa99aaaaaa')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
    end
    it "search their reports with multiple fields filled in incorrectly" do
      year = Time.now.year + 5
      visit "/account/reports?utf8=#{URI.encode('✓')}&resolved=no&reason=%2F%5Bregex+is+fun%5C%29%2F&advanced_query=%2F%7Bregex+is+fun%7D%29%5C%29%2F&msg_id=2147483649.956&channel=ur+a+big+guy&sender=4u&start_time=#{year}-02-20+12%3A40&end_time=#{year}-01-27+12%3A40&per_page=9.5"
        # Let's break this down...
          # Report status: "no"
          # Reporting reason: "/[regex is fun\)/"
          # Message text: "/{regex is fun})\)/"
          # Message ID: 2147483649.956
          # Message channel: "ur a big guy"
          # Message sender: "4u"
          # Date reported start: "{5 years ahead}-02-20 12:40"
          # Date reported end: "{5 years ahead}-01-27 12:40"
          # Per page: 9.5
        # This all gives us a whopping 10 errors:
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("10 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid report status, please select an option from the dropdown menu")
      expect(page).to have_content("Invalid regex in reporting reason, please double check your query. Error details: premature end of char-class")
      expect(page).to have_content("Invalid regex in message text, please double check your query. Error details: unmatched close parenthesis")
      expect(page).to have_content("Invalid message ID, must be less than or equal to 2147483647")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("Starting time cannot be greater than the current system time")
      expect(page).to have_content("Ending time cannot be greater than the current system time")
      expect(page).to have_content("Ending time must be greater than the starting time")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
    end
  end
end

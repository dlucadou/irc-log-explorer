module EulaHelpers
  def create_eula_with(note, text, navigate_to_page: true, submit: true)
    if navigate_to_page
      visit new_admin_eula_path
    end

    expect(page).to have_content("New Terms of Service")
    fill_in "eula_note", with: note
    fill_in "eula_text", with: text

    if submit
      within(".actions") do
        click_button "Publish"
      end
    end
  end

  def search_eulas_with(note: '', text: '', publisher: '', start_time: '',
                        end_time: '', per_page: '', navigate_to_page: true,
                        submit: true)
    if navigate_to_page
      visit admin_eulas_path
    end

    expect(page).to have_content("Terms of Services")
    fill_in "note", with: note
    fill_in "eula_text", with: text
    fill_in "publisher", with: publisher
    fill_in "start_time", with: start_time
    fill_in "end_time", with: end_time
    fill_in "per_page", with: per_page

    if submit
      within(".actions") do
        click_button "Search"
      end
    end
  end
end
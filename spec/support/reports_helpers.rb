module ReportsHelpers
  def create_report_with(msg_id: '', reason: '', navigate_to_page: true, expecting_before: nil, expecting_after: nil, submit: true)
    if navigate_to_page
      visit new_report_path(msg_id: msg_id)
    end

    if expecting_before
      expecting_before.each do |item|
        expect(page).to have_content(item)
      end
    end

    fill_in "report[report_reason]", with: reason

    if submit
      within(".actions") do
        click_button "Submit Report"
      end
    end

    if expecting_after
      expecting_after.each do |item|
        expect(page).to have_content(item)
      end
    end
  end

  def delete_report(report_id: '', navigate_to_page: true, expecting_before: nil, expecting_after: nil)
    if navigate_to_page
      visit reports_path
    end

    if expecting_before
      expecting_before.each do |item|
        expect(page).to have_content(item)
      end
    end

    delete_button = page.find("a[data-report-id=\"#{report_id}\"]")
    delete_button.click

    if expecting_after
      expecting_after.each do |item|
        expect(page).to have_content(item)
      end
    end
  end

  def resolve_report(report_id, resolving_action: 'Generic Reason', switch_to_user: {username: 'admin@domain.example', password: 'passwordpassword1'}, switch_back_user: {username: 'testuser@domain.test', password: 'CorrectHorseBatteryStaple'}, navigate_to_page: true, expecting_before: nil, expecting_after: nil)
    switch_to_user(switch_to_user[:username], switch_to_user[:password]) if switch_to_user

    if navigate_to_page
      visit resolve_report_path(report_id, from_admin: true)
    end

    if expecting_before
      expecting_before.each do |item|
        expect(page).to have_content(item)
      end
    end

    # Resolve report
    fill_in 'report[resolving_action]', with: resolving_action
      # For some reason I can just fill_in 'resolving_action' (the ID of the
      # field) locally, but in CI it fails and I have to use the name itself.
      # The ID is present in CI, no idea why it fails there but not locally.
    click_on 'Mark as Resolved'
    expect(page).to have_content("Successfully resolved report")
    expect(page).to have_current_path(admin_reports_path(highlight: report_id))

    if expecting_after
      expecting_after.each do |item|
        expect(page).to have_content(item)
      end
    end

    switch_to_user(switch_back_user[:username], switch_back_user[:password]) if switch_back_user
  end

  def search_user_reports_with(report_status: '', report_reason: '', msg_text: '', msg_id: '', msg_channel: '', msg_sender: '', start_time: '', end_time: '', per_page: '', navigate_to_page: true, expecting_before: nil, expecting_after: nil)
    if navigate_to_page
      visit reports_path
    end

    if expecting_before
      expecting_before.each do |item|
        expect(page).to have_content(item)
      end
    end

    select report_status, from: "resolved" if report_status.to_s.length > 1
      # Note: if the value is not "All", "Resolved", or "Unresolved", this
      # will cause problems
    fill_in "reason", with: report_reason
    fill_in "advanced_query", with: msg_text
    fill_in "msg_id", with: msg_id
    fill_in "channel", with: msg_channel
    fill_in "sender", with: msg_sender
    fill_in "start_time", with: start_time
    fill_in "end_time", with: end_time
    fill_in "per_page", with: per_page

    click_button "Search"

    if expecting_after
      expecting_after.each do |item|
        expect(page).to have_content(item)
      end
    end
  end

  def search_admin_reports_with(report_status: '', report_reason: '', report_user_id: '', msg_channel: '', msg_text: '', msg_sender: '', report_resolver_id: '', start_time: '', end_time: '', per_page: '', navigate_to_page: true, expecting_before: nil, expecting_after: nil)
    if navigate_to_page
      visit admin_reports_path
    end

    if expecting_before
      expecting_before.each do |item|
        expect(page).to have_content(item)
      end
    end

    select report_status, from: "resolved" if report_status.to_s.length > 1
      # Note: if the value is not "All", "Resolved", or "Unresolved", this
      # will cause problems
    fill_in "reason", with: report_reason
    fill_in "user_id", with: report_user_id
    fill_in "channel", with: msg_channel
    fill_in "advanced_query", with: msg_text
    fill_in "sender", with: msg_sender
    fill_in "resolver_id", with: report_resolver_id
    fill_in "start_time", with: start_time
    fill_in "end_time", with: end_time
    fill_in "per_page", with: per_page

    click_button "Search"

    if expecting_after
      expecting_after.each do |item|
        expect(page).to have_content(item)
      end
    end
  end
end

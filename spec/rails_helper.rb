# This file is copied to spec/ when you run 'rails generate rspec:install'
require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
# Prevent database truncation if the environment is production
abort("The Rails environment is running in production mode!") if Rails.env.production?
require 'rspec/rails'
require 'database_cleaner/active_record'

# Add additional requires below this line. Rails is not loaded until this point!

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
Dir[Rails.root.join('spec', 'support', '**', '*.rb')].each { |f| require f }

# Checks for pending migrations and applies them before tests are run.
# If you are not using ActiveRecord, you can remove these lines.
begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end
RSpec.configure do |config|
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = false
  # Changed to false for DatabaseCleaner
  DatabaseCleaner.clean_with :truncation
  DatabaseCleaner.strategy = :transaction
  config.before(:suite) do
    Rails.application.load_seed
  end
  config.before(:each) do
    DatabaseCleaner.start
  end
  config.after(:each) do
    DatabaseCleaner.clean
  end
  config.around(:each, use_transactional_fixtures: false) do |test|
    # This block exists for a few tests which run into trouble with transaction
    # fixtures because they intentionally cause PG exceptions. More info:
    # https://stackoverflow.com/a/22383251
    # This DatabaseCleaner code from:
    # https://stackoverflow.com/a/10655518
    # This might be slower than deletion on CI systems
    # ( https://stackoverflow.com/a/11423886 )
    # but I like resetting the auto-increment counters, which deletion cannot
    # do automatically. Having a more predictable database state is more
    # important than a small drop in CI speed, in my opinion.
    DatabaseCleaner.strategy = :truncation
    test.run
    DatabaseCleaner.strategy = :transaction
  end

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")
  config.include Devise::Test::ControllerHelpers, type: :controller
    # Prevent MissingWarden errors https://stackoverflow.com/a/38422513
  
  # Clear & seed database before running tests
  config.before(:suite) do
    #load "#{Rails.root}/db/test_seeds.rb"
    # https://stackoverflow.com/a/19930700
    # I disabled re-seeding so I wouldn't have to worry about duplicate
    # chat_log entries messing up tests.
    # When I write tests to edit/delete chat_logs, the transactional fixtures
    # setting should undo edits and deletes after the test runs
  end
end

# IRC Log Explorer

This application connects to a Postgres database with IRC logs to allow for easy searching and message editing.

Current features include:
* Admin console with the ability to ban users, manage reports, and view user searches
* Ability to run on multiple domains with different branding and a unified account system
* Creating accounts and requiring sign-in to view logs
* Date picker when editing messages
* Editing and deleting messages
* Message reporting system
* Message system for communication between users and admins featuring Markdown support
* Oauth integration - sign in from multiple providers on one account, including Discord, Facebook, GitHub, GitLab, Google, Twitter, and Twitch
* Search result pagination
* TOTP based 2FA for increased account security
* User settings (date/time format, theme, time zone, search results per page, default sort, etc.)
* Uses Bootstrap so it looks nice
* U2F based 2FA using WebAuthn

Planned features include:
* Admin console with the ability to view message edit history
* Email sending (account confirmations, admin notices, etc.)
* Enable/disable creating new messages
* Permissions system (including an anonymous mode with search-only capabilities)
* Retain full message history
* The ability to require 2FA for certain permissions

Technical info:
* IRC logger used - [Bernardo Pires's Twitch chat logger](https://github.com/bernardopires/twitch-chat-logger)
* Ruby version - 2.5.1 (Ubuntu repo version)
* Rails version - currently using 5.2.2
* Dependency setup (for Ubuntu):
    ```bash
    sudo apt-get install build-essential patch ruby-dev zlib1g-dev liblzma-dev # for gem 'nokogiri'
    sudo apt-get install postgresql-12 postgresql-server-dev-12
    sudo apt-get install imagemagick # for gem 'mini_magick'
    sudo apt-get install libssl-dev # for running Puma with SSL
    sudo apt-get install xvfb # for gem 'headless'
    # For Selenium to use headless Chromium (assuming Chrome & Chrome WebDriver are not yet installed):
    sudo apt-get install chromium-browser chromium-chromedriver
    # Optional; if you want to install dependencies locally:
    bundler config set --local path 'vendor/bundle'
    # Required:
    bundler install
    ```
* Configuration:
    ```bash
    # If upgrading the application, make sure to git diff the example files
    # to make sure there are no changes to move over with the command:
    # git diff <old-tag-or-commit> <new-tag-or-commit> -- <file>
    # e.g.:
    # git diff 0.9.0 0.9.1 -- config/config.yml.example
    cp config/secrets.yml.example config/secrets.yml
    # Edit config/secrets.yml and insert the relevant API keys
    cp config/config.yml.example config/config.yml
    # Edit config/config.yml and change whatever settings you want
    cp config/database.yml.example config/database.yml
    # Edit config/database.yml and change whatever settings you want
    cp config/locales/domains.en.yml.example config/locales/domains.en.yml
    # Edit config/locales/domains.en.yml and setup your domain configurations
    cp config/environments/production.rb.example config/environments/production.rb
    # Edit config/environments/production.rb -- just go to the last few lines
    # and add in the domains you want the application to be accessible on.
    ```
* Database setup:  
  For the IRC logs database, the logger I linked above can setup the tables for you. For your development environment, however:
    ```bash
    rake db:create
    rake db:schema:load # or "rake db:migrate", but that takes longer
    rake db:seed
    # or...
    bundler exec rake db:drop && bundler exec rake db:create && bundler exec rake db:schema:load && bundler exec rake db:seed
    ```
* Running the application:
    ```bash
    rails server
    # Go to http://localhost:3000/ in your browser, or...
    bundler exec rails s -b 'ssl://127.0.0.1:3000?key=config/ssl/localhost.lucadou.sh.key&cert=config/ssl/localhost.lucadou.sh.crt' # for IPv4
    bundler exec rails s -b 'ssl://[::1]:3000?key=config/ssl/localhost.lucadou.sh.key&cert=config/ssl/localhost.lucadou.sh.crt' # for IPv6
    # U2F requires HTTPS, hence the certificate
    # Go to https://localhost.lucadou.sh:3000/ or https://localhost2.lucadou.sh:3000/ (both domains are in the cert) in your browser
    # This is not a valid certificate, but WebAuthn doesn't require a CA-signed cert to work, just a cert
    ```
* Running tests:
    ```bash
    RAILS_ENV=test xvfb-run -a bundle exec rspec # assuming a headless server, or...
    RAILS_ENV=test bundle exec rspec # if not running headless
    ```

## Notes/FAQ

Make sure you have Postgres and its corresponding server dev package installed before running `bundler install`.

When running tests, you cannot currently do headless Chromium tests with WSL, I recommend running them in a VM or doing something [listed here](https://gist.github.com/danwhitston/5cea26ae0861ce1520695cff3c2c3315).

------

If you get the following error message when running `rake db:create` or `rake db:setup`:

```
PG::InvalidParameterValue: ERROR:  new encoding (UTF8) is incompatible with the encoding of the template database (SQL_ASCII)
```

Follow [these instructions.](https://stackoverflow.com/questions/16736891/pgerror-error-new-encoding-utf8-is-incompatible)

------

If you get the following error message when running `rake db:create` or `rake db:setup`:

```
FATAL:  Peer authentication failed for user "ircsearch"
Couldn't create database for {"adapter"=>"postgresql", "encoding"=>"unicode", "pool"=>5, "username"=>"$USERNAME", "password"=>"$PASSWORD", "database"=>"irc-log-explorer_development"}
rake aborted!
PG::ConnectionBad: FATAL:  Peer authentication failed for user "$USERNAME"
```

Run the following from the Postgres console:

```sql
postgres=# create role $USERNAME with createdb login password '$PASSWORD';
```

[Source](https://www.digitalocean.com/community/tutorials/how-to-setup-ruby-on-rails-with-postgres#setting-up-postgres)

------

If you get the following error message when running `rspec` or `guard`:

```ruby
12:47:11 - INFO - Running all specs

An error occurred while loading ./spec/controllers/admin/dashboard_controller_spec.rb.
Failure/Error: while User.where(email: email_addr).exists?

ActiveRecord::StatementInvalid:
  PG::UndefinedTable: ERROR:  relation "users" does not exist
  LINE 8:                WHERE a.attrelid = '"users"'::regclass
                                            ^
...
```

Run this command:

```bash
RAKE_ENV=test bundler exec rake db:migrate:reset db:test:prepare
```

[Source](https://stackoverflow.com/a/10936745)

------

> How can I change the favicon?

First, make sure your icon is within the boundaries set by [Faceboook](https://developers.facebook.com/docs/sharing/webmasters/images) and [Twitter](https://developer.twitter.com/en/docs/tweets/optimize-with-cards/overview/summary) (scroll down to `twitter:image` in the Reference section). Generally speaking, make sure it is larger than 200x200 pixels, smaller than 4096x4096 pixels, and less than 5 MB.

1. Copy your image to `app/assets/images/favicon.png` (it is recommended you use favicon.png or it will break references to it in the asset pipeline, like generating Twitter card tags)
2. Run this command:
```bash
$ bundler exec rails g favicon
      create  app/assets/images/favicon/android-chrome-192x192.png
      create  app/assets/images/favicon/site.webmanifest.erb
      create  app/assets/images/favicon/android-chrome-384x384.png
      create  app/assets/images/favicon/favicon-16x16.png
      create  app/assets/images/favicon/browserconfig.xml.erb
      create  app/assets/images/favicon/safari-pinned-tab.svg
      create  app/assets/images/favicon/favicon-32x32.png
      create  app/assets/images/favicon/favicon.ico
      create  app/assets/images/favicon/mstile-150x150.png
      create  app/assets/images/favicon/apple-touch-icon.png
      create  app/views/application/_favicon.html.erb
      create  config/initializers/web_app_manifest.rb
```
3. Open `app/views/application/_favicon.html.erb` and remove the `<meta name="theme-color" content="#ffffff">` line (the application automatically sets the theme-color based on the user's theme, so this line is unnecessary)
4. If running in production mode, make sure to run `RAILS_ENV="production" bundler exec rake assets:precompile` and restart the application.
5. Open the application to make sure the icon looks right to you

------

> Is setting up multiple domains required?

No. If you just want 1 domain, it is quite simple. Once you have copied the config files according to the configuration instructions:

* In `config/locales/domains.en.yml`, fill in what you want under `en.domains.default` and delete the other domains
* In `config/secrets.yml`, rename the first domain, fill it in, and delete the others
* In `config/environments/production/rb`, just specify one domain for config.hosts to match

Then make sure you have your webserver setup to only allow connections on the domain you want.

------

> How do I add more than 2 domains?

In the config files mentioned above, just add more domain blocks and fill them in appropriately.

------

> Why is multi-domain setup this way?

Because the overhead of segregating an accounts system by domain is much higher, and just running multiple instances with a shared chat log database has the potential for some difficult to prevent conflicts and race conditions, and data integrity issues with regards to tracking edits and deletions.

Just responding to several domains without special customizations has its own set of problems: back in 2019, Twitch only supported having 1 Oauth redirect URI, meaning if you wanted to have multiple domains, you were out of luck unless your domain structure would allow you to make a cookie that is valid for all of them.

If you did not have subdomains that could create cookies for each other, you would have to do something like what I have setup.
And I know that there are still Oauth providers who only support 1 redirect URI (or 1 domain for redirect URIs), so this allows for adding more Oauth integrations without having to worry about redirect capabilities, patching to redirect a user to another domain with parameters in the URL for authentication, or the pitfalls of creating a shared cookie that could be read on other subdomains (e.g. creating a cookie for `Domain=.herokuapp.com` is a bad idea).

Plus, doing this allows you to have the appearance of separate systems.
It does not attempt to maintain the illusion, but the unified account system is very convenient, like a very small federated system.

------

> What Oauth redirect URIs did you use?

* Discord: https://subdomain.yourdomain.com:3000/account/auth/discord/callback
* Facebook: https://subdomain.yourdomain.com:3000/account/auth/facebook/callback
* GitHub: https://subdomain.yourdomain.com:3000/account/auth/github/callback
* GitLab: https://subdomain.yourdomain.com:3000/account/auth/gitlab/callback
* Google: https://subdomain.yourdomain.com:3000/account/auth/google_oauth2/callback
* Twitter: https://subdomain.yourdomain.com:3000/account/auth/twitter/callback
* Twitch: https://subdomain.yourdomain.com:3000/account/auth/twitch/callback

Previously, you could use localhost:3000. However, U2F requires HTTPS, so you have to have some domain name, and Google requires a real domain name (I can't use localhost.test since .test is not a real TLD), so you have to have an actual domain with A records pointing to 127.0.0.1 and AAAA records pointing to ::1. For example, I use localhost.lucadou.sh (and you can too, since it points to 127.0.0.1 and ::1!).

------

> I want to generate a new SSL certificate for my own domain, how can I generate it?

Modify `config/ssl/localhost-cert.conf` ([source for file](https://stackoverflow.com/a/23587047)), then run this command (changing the filename of the certificate):

```bash
$ openssl req -config config/ssl/localhost-cert.conf -new -x509 -newkey rsa:2048 -nodes -keyout config/ssl/localhost.lucadou.sh.key -days 3650 -out config/ssl/localhost.lucadou.sh.crt
Generating a 2048 bit RSA private key
.+++
...........+++
writing new private key to 'localhost.lucadou.sh.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [US]:
Common Name (e.g. server FQDN or YOUR name) [IRC Log Viewer Development Certificate]:
```

Then, you can verify it has the correct domains by running this command (again changing the filename of the certificate):

```bash
$ openssl x509 -in config/ssl/localhost.lucadou.sh.crt -text -noout
Certificate:
    Data:
        Version: 3 (0x2)
        ...
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = US, CN = IRC Log Viewer Development Certificate
        ...
        X509v3 extensions:
            ...
            X509v3 Extended Key Usage:
                TLS Web Server Authentication
            X509v3 Subject Alternative Name:
                DNS:localhost.lucadou.sh, DNS:localhost2.lucadou.sh
            Netscape Comment:
                OpenSSL Generated Certificate - Not for Production Use
    Signature Algorithm: sha256WithRSAEncryption
         ...

$
```

------

If you get a message similar to this when attempting to run Rails server with custom SSL certificates:

```ruby
=> Booting Puma
=> Rails 5.2.2 application starting in development
=> Run `rails server -h` for more startup options
Puma starting in single mode...
* Version 3.12.0 (ruby 2.5.1-p57), codename: Llamas in Pajamas
* Min threads: 5, max threads: 5
* Environment: development
Exiting
Traceback (most recent call last):
        20: from bin/rails:4:in `<main>'
        19: from bin/rails:4:in `require'
        ...
         2: from /var/lib/gems/2.5.0/gems/puma-3.12.0/lib/puma/binder.rb:88:in `each'
         1: from /var/lib/gems/2.5.0/gems/puma-3.12.0/lib/puma/binder.rb:149:in `block in parse'
/var/lib/gems/2.5.0/gems/puma-3.12.0/lib/puma/binder.rb:149:in `check': SSL not available in this build (StandardError)
```

Run these commands:

```bash
    sudo apt-get install libssl-dev # for running Puma with SSL
    sudo gem uninstall puma
    bundler install
```

------

If you are using Unix domain sockets for your database connection and get an error message similar to this when attempting to run rake commands:

```bash
$ bundler exec rake db:schema:load
rake aborted!
PG::ConnectionBad: could not connect to server: Connection refused
        Is the server running on host "localhost" (127.0.0.1) and accepting
        TCP/IP connections on port 5432?
/var/lib/gems/2.5.0/gems/pg-1.1.4/lib/pg.rb:56:in `initialize'
/var/lib/gems/2.5.0/gems/pg-1.1.4/lib/pg.rb:56:in `new'
/var/lib/gems/2.5.0/gems/pg-1.1.4/lib/pg.rb:56:in `connect'
...
```

Comment out the `host` and `port` lines in `config/database.yml`, then Rails will use Unix sockets to connect to Postgres. The `default` block should now look like this:

```yaml
default: &default
  adapter: postgresql
  encoding: unicode
  #host: <%= ENV.fetch("POSTGRES_HOST") { "localhost" } %>
  #port: <%= ENV.fetch("POSTGRES_PORT") { 5432 } %>
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  username: <%= ENV.fetch("POSTGRES_USER") { "ircsearch" } %>
  password: <%= ENV.fetch("POSTGRES_PASSWORD") { "rubypass" } %>
```

-----

If you run into an error similar to this when trying to run the tests:


```ruby
  1) UserReports User cannot report a message with no reason (a reason 0 characters long)
     Failure/Error: visit new_user_registration_path

     NoMethodError:
       undefined method `strip' for nil:NilClass
     # /var/lib/gems/2.5.0/gems/webdrivers-3.7.2/lib/webdrivers/chromedriver.rb:116:in `chrome_on_linux'
     # /var/lib/gems/2.5.0/gems/webdrivers-3.7.2/lib/webdrivers/chromedriver.rb:71:in `chrome_version'
     # /var/lib/gems/2.5.0/gems/webdrivers-3.7.2/lib/webdrivers/chromedriver.rb:62:in `release_version'
     ...
```

Chances are, the Chrome/Chromium binary is not installed or it is installed to a non-standard location that isn't included in your `$PATH`. If Chrome/Chromium is installed to a non-standard location, you should add the installation to your `$PATH` manually. Alternatively, you can try symlinking it to a more common location with one of these commands:

```bash
sudo ln -s /path/to/custom/binary /usr/bin/chromium-browser # Only run 1 of these!
sudo ln -s /path/to/custom/binary /usr/bin/google-chrome
```

------

If visiting the homepage or other pages results in a segfault from sassc-ruby:

```ruby
/var/lib/gems/2.5.0/gems/sassc-2.3.0/lib/sassc/engine.rb:43: [BUG] Segmentation fault at 0x0000000000000000
ruby 2.5.1p57 (2018-03-29 revision 63029) [x86_64-linux-gnu]

-- Control frame information -----------------------------------------------
c:0059 p:---- s:0449 e:000448 CFUNC  :compile_data_context
c:0058 p:0318 s:0444 E:0007d0 METHOD /var/lib/gems/2.5.0/gems/sassc-2.3.0/lib/sassc/engine.rb:43
c:0057 p:0006 s:0432 E:002170 BLOCK  /var/lib/gems/2.5.0/gems/sassc-rails-2.1.2/lib/sassc/rails/template.rb:40
c:0056 p:0026 s:0429 E:001058 METHOD 
...
7f48facbd000-7f48facbe000 rw-p 00028000 fd:00 660977                     /lib/x86_64-linux-gnu/ld-2.27.so
7f48facbe000-7f48facbf000 rw-p 00000000 00:00 0 
7ffdf589d000-7ffdf6099000 rwxp 00000000 00:00 0                          [stack]
7ffdf6099000-7ffdf609c000 rw-p 00000000 00:00 0 
7ffdf6148000-7ffdf614b000 r--p 00000000 00:00 0                          [vvar]
7ffdf614b000-7ffdf614d000 r-xp 00000000 00:00 0                          [vdso]
ffffffffff600000-ffffffffff601000 r-xp 00000000 00:00 0                  [vsyscall]


[NOTE]
You may have encountered a bug in the Ruby interpreter or extension libraries.
Bug reports are welcome.
For details: http://www.ruby-lang.org/bugreport.html

Aborted (core dumped)
```

Try deleting the cookie issued by the server in your browser. I have no idea how a browser cookie could cause sassc-ruby to segfault the Ruby interpreter, but it does. I ran into this after upgrading to Rails 6 and this fixed it.

-----

If Selenium-enabled tests fail with this error message:

```ruby
  3) UserReports User cannot report a message that does not exist
     Failure/Error: visit new_user_registration_path

     Selenium::WebDriver::Error::UnknownError:
       unknown error: Chrome failed to start: crashed
         (unknown error: DevToolsActivePort file doesn't exist)
         (The process started from chrome location /usr/bin/google-chrome is no longer running, so ChromeDriver is assuming that Chrome has crashed.)
         (Driver info: chromedriver=73.0.3683.68 (47787ec04b6e38e22703e856e101e840b65afe72),platform=Linux 4.4.0-17763-Microsoft x86_64)
     # /var/lib/gems/2.5.0/gems/selenium-webdriver-3.141.0/lib/selenium/webdriver/remote/response.rb:69:in `assert_ok'
     # /var/lib/gems/2.5.0/gems/selenium-webdriver-3.141.0/lib/selenium/webdriver/remote/response.rb:32:in `initialize'
     # /var/lib/gems/2.5.0/gems/selenium-webdriver-3.141.0/lib/selenium/webdriver/remote/http/common.rb:84:in `new'
     # /var/lib/gems/2.5.0/gems/selenium-webdriver-3.141.0/lib/selenium/webdriver/remote/http/common.rb:84:in `create_response'
     ...
```

There seem to be a variety of potential causes of this, from a mismatch between Chrome/Chromium and WebDriver versions to needing to symlink the Chrome/Chromium binary (see the section above about `NoMethodError: undefined method 'strip' for nil:NilClass`).

However, there is another cause of this which took me awhile to figure out. If you are using Windows Subsystem for Linux (WSL), Selenium tests are not currently possible without doing a bunch of X stuff or using your Windows Chrome/Chromium and WebDriver install ([source](https://gist.github.com/danwhitston/5cea26ae0861ce1520695cff3c2c3315)). Personally, I just found it easier to run it in a VM.

------

> The application seems to have problems with ImageMagick on TOTP setup pages

I used to think this was a Heroku limitation, but I have seen it occur on a fresh install of Linux Mint, so I have no idea what causes it. I've done a lot of investigation and at first it seemed like an ImageMagick version bug but at this point I still have no idea. These are the logs I got on Heroku when trying to generate PNG QR codes (the SVG QR codes work just fine):

```ruby
app[web.1]: I, [2019-01-26T14:34:13.946280 #4]  INFO -- : [17e3dbf7-59d2-45dd-ab73-248c982c9a20] Started GET "/account/security/2fa/totp" for 198.86.77.9 at 2019-01-26 14:34:13 +0000
app[web.1]: I, [2019-01-26T14:34:13.949193 #4]  INFO -- : [17e3dbf7-59d2-45dd-ab73-248c982c9a20] Processing by RegistrationsController#account_manage_totp as HTML
app[web.1]: D, [2019-01-26T14:34:13.953086 #4] DEBUG -- : [17e3dbf7-59d2-45dd-ab73-248c982c9a20]   User Load (1.5ms)  SELECT  "users".* FROM "users" WHERE "users"."id" = $1 ORDER BY "users"."id" ASC LIMIT $2  [["id", 1], ["LIMIT", 1]]
app[web.1]: I, [2019-01-26T14:34:14.122555 #4]  INFO -- : [17e3dbf7-59d2-45dd-ab73-248c982c9a20] Completed 500 Internal Server Error in 173ms (ActiveRecord: 1.5ms)
app[web.1]: F, [2019-01-26T14:34:14.123224 #4] FATAL -- : [17e3dbf7-59d2-45dd-ab73-248c982c9a20]   
app[web.1]: F, [2019-01-26T14:34:14.123298 #4] FATAL -- : [17e3dbf7-59d2-45dd-ab73-248c982c9a20] MiniMagick::Invalid (`identify /tmp/mini_magick20190126-4-w3sjan` failed with error:
app[web.1]: identify-im6.q16: delegate failed `'rsvg-convert' -o '%o' '%i'' @ error/delegate.c/InvokeDelegate/1919.
app[web.1]: identify-im6.q16: unable to open file `/tmp/magick-3112g59DHKDfmY': No such file or directory @ error/constitute.c/ReadImage/544.
app[web.1]: ):
app[web.1]: F, [2019-01-26T14:34:14.123360 #4] FATAL -- : [17e3dbf7-59d2-45dd-ab73-248c982c9a20]   
app[web.1]: F, [2019-01-26T14:34:14.123423 #4] FATAL -- : [17e3dbf7-59d2-45dd-ab73-248c982c9a20] app/controllers/registrations_controller.rb:535:in `build_qr_code'
app[web.1]: [17e3dbf7-59d2-45dd-ab73-248c982c9a20] app/controllers/registrations_controller.rb:260:in `account_manage_totp'

```

To work around this, you can disable PNG generation by changing `users.totp.generate_png_qrcodes` to `false` in `config/config.yml`. The TOTP page will only display SVGs and tell the user they will have to manually enter the code if their browser does not support SVGs.

If anyone knows what causes this, please let me know, it might help track down the root cause.

------

> How do I set this up on my server in production?

I am using Puma with Nginx as a reverse proxy, as it allows for good performance and easy HTTPS setup. I'm basing these instructions off of [this guide](https://igotablog.com/using-puma-with-nginx/), but have changed it a fair amount since it was written in 2013, and is somewhat out of date.

These instructions you have already installed Nginx, opened the appropriate ports, and confirmed you can access the Nginx default webpage. These instructions also assume you have run the relevant `rake db` commands and `rake assets:precompile` (in production mode - set the `RAILS_ENV` environment variable beforehand), and that you have confirmed the system runs in development mode.

1. Open the file `/etc/nginx/sites-available/[your.domain.here]` (you'll probably need to open it with sudo to write to it) and add the following:

    ```nginx
    upstream app {
      # Path to Puma SOCK file
      server unix:///path/to/application/shared/sockets/puma.sock;
    }

    server {
      server_name [your.domain.here];
      root /path/to/application/public;
      listen 80;
      listen [::]:80;

      location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_set_header X-Forwarded-Proto https; # Needed to avoid 'WARNING: Can't verify CSRF token authenticity' with HTTPS, source: https://stackoverflow.com/a/48632245
        proxy_pass http://app;
      }

      error_page 500 502 503 504 /500.html;
      client_max_body_size 4G;
      keepalive_timeout 10;
    }
    ```

2. Symlink the file into the sites-enabled directory by running this command:

    ```bash
    sudo ln -s /etc/nginx/sites-available/[your.domain.here] /etc/nginx/sites-enabled/[your.domain.here]
    ```

3. Make sure we haven't broken anything by running this command:

    ```bash
    sudo nginx -t
    ```

    You should get output similar to this:

    ```bash
    nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
    nginx: configuration file /etc/nginx/nginx.conf test is successful
    ```

4. If so, restart Nginx (if your output did not match the above, you should figure out what went wrong or Nginx will not start):

    ```bash
    sudo systemctl restart nginx
    # Or, if your system doesn't have System D:
    sudo service nginx restart
    ```

5. (Optional) Setup HTTPS using Certbot (if you don't have certificates from another source):

    ```bash
    sudo certbot --nginx
    # After you finish running Certbot, restart Nginx
    sudo systemctl restart nginx
    # Or, if your system doesn't have System D:
    sudo service nginx restart
    ```

6. From the application directory, launch the server:

    ```bash
    RAILS_ENV=production puma
    # Alternatively:
    export RAILS_ENV=production # You can put this in your .bashrc, .bash_profile, etc.
    puma
    ```

7. When you point your browser to `http(s)://[your.domain.here]/`, it should be up and running.

    Another guide you might look into is [this guide](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-rails-app-with-puma-and-nginx-on-ubuntu-14-04) from DigitalOcean (however, when I tried to follow it, I could not get it to work right because they used an @ in the upstream name, which it seems is unsupported in Nginx 1.14.0, the current version for Ubuntu 18.04).

------

> I've gotten the application running with Nginx, how do I get it to auto-start when I reboot my server?

To configure auto-starting, we will need to set this up as a SystemD service (this guide assumes you are using SystemD). These instructions are based off of [this guide](https://github.com/puma/puma/blob/master/docs/systemd.md):

1. Create a new file at `/etc/systemd/system/[service-name].service`

2. Add this to the file:

    ```
    [Unit]
    Description=<Service Name>
    After=network.target

    # Uncomment for socket activation (see below)
    # Requires=puma.socket

    [Service]
    # Foreground process (do not use --daemon in ExecStart or config.rb)
    Type=simple

    # Preferably configure a non-privileged user
    User=<username>

    # The path to the your application code root directory.
    WorkingDirectory=</full/path/to/git/repo>

    # SystemD will not run puma even if it is in your path. You must specify
    # an absolute URL to puma. For example /usr/local/bin/puma
    # Reason for /bin/bash: https://stackoverflow.com/a/45778018
    ExecStart=/bin/bash -lc </full/path/to/puma/binary> -C </full/path/to/git/repo>/config/puma.rb </full/path/to/git/repo>/config.ru

    Restart=always

    [Install]
    WantedBy=multi-user.target
    ```

    The `Restart` policy can be freely modified; to view the other possible options, the documentation can be found [here](https://www.freedesktop.org/software/systemd/man/systemd.service.html#Restart=).

3. You will need to replace anything surrounded by <angle brackets> - `Description`, `User`, `WorkingDirectory`, and `ExecPath`. To get the path of your Puma binary, run the command `which puma`.

4. Load the new file by running this command:
    ```bash
    sudo systemctl daemon-reload
    ```

5. Start the service by running this command:

    ```bash
    sudo systemctl start [service-name].service
    ```

6. Check the status of the service by running this command:

    ```bash
    systemctl status [service-name].service
    ```

7. If the status is failed and you see something like this:

    ```bash
    Mar 26 03:14:27 ip-172-31-23-77 systemd[1]: Started <Service Name>.
    Mar 26 03:14:27 ip-172-31-23-77 puma[4562]: /usr/bin/env: ‘ruby_executable_hooks’: No such file or directory
    Mar 26 03:14:27 ip-172-31-23-77 systemd[1]: [service-name].service: Main process exited, code=exited, status=127/n/a
    Mar 26 03:14:27 ip-172-31-23-77 systemd[1]: [service-name].service: Failed with result 'exit-code'.
    ```

    You will need to run this command:

    ```bash
    gem install --user-install executable-hooks
    ```
    [Source](https://stackoverflow.com/a/29519580)

8. Once the status is `active (running)`, we can set it to auto-start with the system by running this command:

    ```bash
    sudo systemctl enable [service-name].service
    ```

9. (Optional, but recommended.) Finally, reboot your server (the physical server, not Rails or Puma) to make sure everything comes up.

10. You should not see Nginx's 502 Bad Gateway error message when your server reboots now, as Puma should auto-start!

------

> When I access the application, it gives a 502 Bad Gateway issue.

This is typically caused by a bad `ExecStart` command. Double check the formatting of the command. I noticed when I was trying to start Puma with Bundler, if I was quick to check the status of the service after boot:

```bash
May 20 04:43:35 ip-172-31-23-77 systemd[1]: Started [service-description].
May 20 04:43:35 ip-172-31-23-77 bash[29444]: Unknown ruby interpreter version (do not know how to handle): ~>2.6.0.
```

And when I re-checked the status immediately afterwards, I got this:

```bash
May 20 04:43:36 ip-172-31-23-77 systemd[1]: [service-name].service: Service hold-off time over, scheduling restart.
May 20 04:43:36 ip-172-31-23-77 systemd[1]: [service-name].service: Scheduled restart job, restart counter is at 5.
May 20 04:43:36 ip-172-31-23-77 systemd[1]: Stopped [service-description].
May 20 04:43:36 ip-172-31-23-77 systemd[1]: [service-name].service: Start request repeated too quickly.
May 20 04:43:36 ip-172-31-23-77 systemd[1]: [service-name].service: Failed with result 'start-limit-hit'.
May 20 04:43:36 ip-172-31-23-77 systemd[1]: Failed to start [service-description].
```

Turns out trying to use bundler exec to start it was a bit of a red herring, you can read more about the problem I faced immediately below (504 Gateway Timeout Errors).

------

> When I try to access the application, it stalls for awhile before Nginx displays a 504 Gateway Timeout Error. What does this mean?

You'll need to look at the file `shared/log/puma.stderr.log` (I recommend using `less`, as it can get quite large and overwhelm most GUI-based text editors). If you see something like this:

```ruby
=== puma startup: 2019-05-20 04:51:54 +0000 ===
/home/ubuntu/.rvm/rubies/ruby-2.6.0/lib/ruby/2.6.0/bundler/lockfile_parser.rb:108:in `warn_for_outdated_bundler_version': You must use Bundler 2 or greater with this lockfile. (Bundler::LockfileError)
        from /home/ubuntu/.rvm/rubies/ruby-2.6.0/lib/ruby/2.6.0/bundler/lockfile_parser.rb:95:in `initialize'
        from /home/ubuntu/.rvm/rubies/ruby-2.6.0/lib/ruby/2.6.0/bundler/definition.rb:83:in `new'
        from /home/ubuntu/.rvm/rubies/ruby-2.6.0/lib/ruby/2.6.0/bundler/definition.rb:83:in `initialize'
        ...
```

This error usually indicates you need to check the Ruby version in use. If you cd into the git directory and rvm outputs this:

```bash
$ cd -
/home/ubuntu/git/irc-log-explorer-prod
RVM used your Gemfile for selecting Ruby, it is all fine - Heroku does that too,
you can ignore these warnings with 'rvm rvmrc warning ignore /home/ubuntu/git/irc-log-explorer-prod/Gemfile'.
To ignore the warning for all files run 'rvm rvmrc warning ignore allGemfiles'.

Unknown ruby interpreter version (do not know how to handle): ~>2.5.0.
```

Check what version of Ruby is the system default. For example, when I upgraded my Ubuntu version, it added Ruby 2.6.0. Previously, it was on 2.5.2 (you'll note the Gemfile specifies `~>2.5.0`, which is equivalent to `2.5.0 <= ruby -v < 2.6.0`.

There are several strategies you can take at this point. One would be to modify the Gemfile to use your system's default Ruby version, re-run `bundler install` and `bundler update`, and hope for the best. I tried this and it did not work. What did work for me, however, is creating a file, `.ruby-version`, and setting it to the last 2.5.x release I had:

```bash
$ ls ~/.rvm/gems
cache  default  ruby-2.5.5  ruby-2.5.5@global  ruby-2.6.0  ruby-2.6.0@global
$ echo '2.5.5' > .ruby-version

$ cat .ruby-version # I specifically avoid committing this file to the repo because different distros will have different non-major versions available. For example, Linux Mint 19.1 has 2.5.1 available, while Ubuntu 18.04 has 2.5.5.
2.5.5

$ cd ..
/home/ubuntu/git/
$ cd -
/home/ubuntu/git/irc-log-explorer-prod
$ # Notice how RVM didn't complain this time.
```

Then, try restarting the server:

```bash
$ sudo systemctl restart [service-name].service
```

Check the status, make sure it hasn't died, and try accessing the application. It should be up and running now! If it still is not working, however, re-open the SystemD service file and re-check the `ExecStart` command. Make sure it is using the correct Ruby version:

```
ExecStart=/bin/bash -lc /home/ubuntu/.rvm/gems/ruby-2.5.5/bin/puma -C /home/ubuntu/git/irc-log-explorer-prod/config/puma.rb /home/ubuntu/git/irc-log-explorer-prod/config.ru
```

If there is a Ruby version mispatch (e.g. it's using `/home/ubuntu/.rvm/gems/ruby-2.6.0/bin/puma`), you will need to correct it.

Past this, I have not encountered any other causes of 504 errors. `less`-ing the `shared/log/puma.stderr.log` is the best way to debug errors since they typically will not show up in the SystemD status unless they are fatal (like the Ruby/Bundler version problem).

------

Another potential cause for 504 gateway timeouts is an invalid YAML config. From my `shared/log/puma.stderr.log`:

```ruby
/home/ubuntu/.rvm/gems/ruby-2.5.5/gems/railties-5.2.3/lib/rails/application.rb:2
40:in `rescue in config_for': YAML syntax error occurred while parsing /home/ubu
ntu/git/irc-log-explorer-prod/config/config.yml. Please note that YAML must be c
onsistently indented using spaces. Tabs are not allowed. Error: (<unknown>): did
 not find expected key while parsing a block mapping at line 3 column 5 (Runtime
Error)
        from /home/ubuntu/.rvm/gems/ruby-2.5.5/gems/railties-5.2.3/lib/rails/app
lication.rb:226:in `config_for'
        from /home/ubuntu/.rvm/gems/ruby-2.5.5/gems/railties-5.2.3/lib/rails/rai
ltie.rb:190:in `public_send'
        from /home/ubuntu/.rvm/gems/ruby-2.5.5/gems/railties-5.2.3/lib/rails/railtie.rb:190:in `method_missing'
```

The message about indents is a bit misleading, as it implies the error on line 3 column 5 (or wherever in your file) is due to indentation. What you should do is install `yamllint` and run it against the file.

```bash
$ yamllint config/config.yml
  4:81      error    line too long (141 > 80 characters)  (line-length)
  4:130     error    syntax error: expected <block end>, but found '<scalar>'
```

The line in question:

```yaml
---
default: &default
  footer:
    append: 'Version 0.8.8 Alpha <a href="https://gitlab.com/lucadou/irc-log-explorer/blob/development/CHANGELOG.md">(See what\'s new!)</a>'
```

The line too long error can be safely ignored; what matters is that I messed up escaping a single quote - I should have done `''`, not `\'`. After correcting that error, rerunning it returned just the line too long error:

```bash
$ yamllint config/config.yml
  4:81      error    line too long (141 > 80 characters)  (line-length)
```

I then restarted the SystemD service and the application ran as expected.

------

> When in production mode, I get errors like this showing up in `log/production.log`:

```ruby
I, [2020-01-03T05:00:49.491856 #5493]  INFO -- : [7953bda5-6f2f-48e5-8e8f-681ebb859186] Started GET "/" for 2600:6c5e:2180:600:1508:64b1:bab2:372b at 2020-01-03 05:00:4
9 +0000
I, [2020-01-03T05:00:49.496727 #5493]  INFO -- : [7953bda5-6f2f-48e5-8e8f-681ebb859186] Processing by PagesController#index as HTML
I, [2020-01-03T05:00:49.500511 #5493]  INFO -- : [7953bda5-6f2f-48e5-8e8f-681ebb859186]   Rendering pages/index.html.erb within layouts/application
I, [2020-01-03T05:00:49.501298 #5493]  INFO -- : [7953bda5-6f2f-48e5-8e8f-681ebb859186]   Rendered pages/index.html.erb within layouts/application (0.7ms)
I, [2020-01-03T05:00:49.513227 #5493]  INFO -- : [7953bda5-6f2f-48e5-8e8f-681ebb859186] Completed 500 Internal Server Error in 16ms
F, [2020-01-03T05:00:49.513977 #5493] FATAL -- : [7953bda5-6f2f-48e5-8e8f-681ebb859186]
F, [2020-01-03T05:00:49.514005 #5493] FATAL -- : [7953bda5-6f2f-48e5-8e8f-681ebb859186] ActionView::Template::Error (The asset "favicon.png" is not present in the asset
pipeline.):
F, [2020-01-03T05:00:49.514081 #5493] FATAL -- : [7953bda5-6f2f-48e5-8e8f-681ebb859186]     1: <!DOCTYPE html>
[7953bda5-6f2f-48e5-8e8f-681ebb859186]     2: <html>
[7953bda5-6f2f-48e5-8e8f-681ebb859186]     3:   <head>
[7953bda5-6f2f-48e5-8e8f-681ebb859186]     4:     <%= generate_header_tags.html_safe %>
[7953bda5-6f2f-48e5-8e8f-681ebb859186]     5:     <%= csrf_meta_tags %>
[7953bda5-6f2f-48e5-8e8f-681ebb859186]     6:     <meta name="viewport" content="width=device-width, initial-scale=1" />
[7953bda5-6f2f-48e5-8e8f-681ebb859186]     7:     <meta charset="utf-8" />
F, [2020-01-03T05:00:49.514097 #5493] FATAL -- : [7953bda5-6f2f-48e5-8e8f-681ebb859186]
F, [2020-01-03T05:00:49.514116 #5493] FATAL -- : [7953bda5-6f2f-48e5-8e8f-681ebb859186] app/controllers/application_controller.rb:39:in `block (3 levels) in generate_he
ader_tags'
[7953bda5-6f2f-48e5-8e8f-681ebb859186] app/controllers/application_controller.rb:33:in `each'
[7953bda5-6f2f-48e5-8e8f-681ebb859186] app/controllers/application_controller.rb:33:in `block (2 levels) in generate_header_tags'
[7953bda5-6f2f-48e5-8e8f-681ebb859186] app/controllers/application_controller.rb:31:in `each'
[7953bda5-6f2f-48e5-8e8f-681ebb859186] app/controllers/application_controller.rb:31:in `block in generate_header_tags'
[7953bda5-6f2f-48e5-8e8f-681ebb859186] app/controllers/application_controller.rb:27:in `each'
[7953bda5-6f2f-48e5-8e8f-681ebb859186] app/controllers/application_controller.rb:27:in `generate_header_tags'
[7953bda5-6f2f-48e5-8e8f-681ebb859186] app/views/layouts/application.html.erb:4:in `_app_views_layouts_application_html_erb__3139627725913624106_37001680'
```

This is usually caused by 1 of the following 4 issues:

* You forgot to copy the asset into the appropriate folder
* You named the asset incorrectly (or misspelled the asset's name in the configuration file)
* You forgot to precompile your newly-added assets
* Assets have changed in newer versions and you need to recompile

If your issue was one of the first 2, fix it.  
Then (for all 4 causes), run `RAILS_ENV="production" bundler exec rake assets:precompile` and restart the application.

------

> I am getting `ActionController::InvalidAuthenticityToken` errors like so:

```ruby
Can't verify CSRF token authenticity.
Completed 500  in 48ms (ActiveRecord: 4.7ms)


  
ActionController::InvalidAuthenticityToken (ActionController::InvalidAuthenticityToken):
  
actionpack (5.2.3) lib/action_controller/metal/request_forgery_protection.rb:211:in `handle_unverified_request'
actionpack (5.2.3) lib/action_controller/metal/request_forgery_protection.rb:243:in `handle_unverified_request'
devise (4.6.2) lib/devise/controllers/helpers.rb:255:in `handle_unverified_request'
actionpack (5.2.3) lib/action_controller/metal/request_forgery_protection.rb:238:in `verify_authenticity_token'
...
```

I have only seen this happen when you refuse to supply an email via Facebook, enter an email that is already in use when prompted, click the link to delete the account, then try to sign in with an existing account.

To fix this, just delete the cookie for the domain and refresh the page.

------

> There are weird CSS and JS errors that I can't reproduce in development mode.

This is usually caused by compiling assets without the `RAILS_ENV` environment variable being set. There are 2 ways to do this:

1. Set the environment varaible and re-compile your assets:

    ```bash
    export RAILS_ENV="production"
    bundler exec rake assets:precompile
    ```

2. Or preface the command with the variable:

    ```bash
    RAILS_ENV="production" bundler exec rake assets:precompile
    ```

When it finishes, make sure to restart the application for the changes to take effect. The problems should be fixed. (I recommend setting the environment variable in your .bashrc or other RC file so you do not have to worry about this in the future.)

------

> Why did you make this?

I wanted an IRC log viewer that worked with a remote database because pgAdmin 4 is buggy and slow (and updating my IP in my AWS VPC security groups every time it changes is a bit annoying). I went looking around, and the few viewers I found that worked with remote databases were all old (and looked old) or they used some weird schemas I didn't feel like adapting. The rest of the viewers I found were programs for viewing your local mIRC, IRSSI, etc. logs.

With this disappointing selection, I realized I could make a log viewer and use it as a way to improve my knowledge from a Ruby/Rails course I had taken. The scope very quickly expanded as I thought about what all I needed (authentication, administrative capabilities, etc.), and it turns out Rails is an amazing framework to do all this.

------

> Do you have a live instance I can check out?

Yep! I don't deploy to production too often (since I haven't finished streamlining the process yet), but my deployments [will be available here](https://logs.lucadou.sh/), with the version listed in the footer.

## Versioning

We use Semantic Versioning, aka [SemVer](https://semver.org/spec/v2.0.0.html).

## Authors

* Luna Lucadou - Wrote the application in her free time

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgements

* My coworkers at UNC Charlotte Student Affairs IT for encouraging me to keep pursuing this project
* The denizens of MonotoneTim's offline Twitch chat for providing plenty of test data
* The fine folks maintaining Ruby, Ruby-Doc.org, RubyGems, and Rails

## Bugs, feature ideas, etc?

Feel free to fill out an issue or submit a merge request!


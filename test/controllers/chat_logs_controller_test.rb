require 'test_helper'

class ChatLogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @chat_log = chat_logs(:one)
  end

  test "should get index" do
    get chat_logs_url
    assert_response :success
  end

  test "should get new" do
    get new_chat_log_url
    assert_response :success
  end

  test "should create chat_log" do
    assert_difference('ChatLog.count') do
      post chat_logs_url, params: { chat_log: { channel: @chat_log.channel, date: @chat_log.date, message: @chat_log.message, sender: @chat_log.sender } }
    end

    assert_redirected_to chat_log_url(ChatLog.last)
  end

  test "should show chat_log" do
    get chat_log_url(@chat_log)
    assert_response :success
  end

  test "should get edit" do
    get edit_chat_log_url(@chat_log)
    assert_response :success
  end

  test "should update chat_log" do
    patch chat_log_url(@chat_log), params: { chat_log: { channel: @chat_log.channel, date: @chat_log.date, message: @chat_log.message, sender: @chat_log.sender } }
    assert_redirected_to chat_log_url(@chat_log)
  end

  test "should destroy chat_log" do
    assert_difference('ChatLog.count', -1) do
      delete chat_log_url(@chat_log)
    end

    assert_redirected_to chat_logs_url
  end
end
